/**
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configStore from './store/configStore.js';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  StatusBar,
  View,
  Text,
  ActivityIndicator,
  Dimensions,
  Platform,
  NetInfo
} from 'react-native';
import { FitlyFirebase } from './library/firebaseHelpers.js';
import NewNavApp from './navigator/_GlobalNavigator';
import { FitlyBlue, loginStyles } from './styles/styles.js';
const store = configStore();
const { width, height } = Dimensions.get('window');
function setup() {
  class Root extends React.Component {
    state = {
      isConnected: true
    };
    componentWillMount() {
      StatusBar.setBarStyle('light-content');
      if (Platform.OS === 'android') {
        StatusBar.setTranslucent(true);
        StatusBar.setBackgroundColor('#000045');
      }
      NetInfo.isConnected.fetch().then(isConnected => {
        this.setState({ isConnected: true });
        isConnected
          ? this.setState({ isConnected: true })
          : this.setState({ isConnected: false });
      });
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this.handleConnectivityChange.bind(this)
      );
    }

    handleConnectivityChange(isConnected) {
      if (isConnected) {
        this.setState({ isConnected: true });
      } else this.setState({ isConnected: false });
    }
    renderNoConnection() {
      return (
        <View
          style={{
            width,
            height,
            backgroundColor: 'rgba(0, 0, 0, 0.3)',
            position: 'absolute',
            left: 0,
            top: 0
          }}
        >
          <View
            style={{
              width: width,
              height: 80,
              backgroundColor: '#000',
              position: 'absolute',
              shadowColor: '#000',
              shadowOpacity: 0.2,
              alignItems: 'center',
              shadowOffset: { width: 0, height: 3 },
              shadowRadius: 2,
              paddingTop: 20,
              elevation: 9,
              flexDirection: 'row',
              paddingHorizontal: 16,
              justifyContent: 'space-between'
            }}
          >
            <Icon name="perm-scan-wifi" color="#fff" size={32} />
            <Text
              style={{
                width: width - 80,
                backgroundColor: 'transparent',
                color: '#fff',
                fontWeight: 'bold',
                fontSize: 12,
                textAlign: 'center'
              }}
            >
              Oops! No internet connection available. Please wait for the
              connection or try again later.
            </Text>
          </View>
        </View>
      );
    }

    render() {
      return (
        <View style={{ flex: 1 }}>
          <Provider store={store}>
            <NewNavApp FitlyFirebase={FitlyFirebase} />
          </Provider>
          {!this.state.isConnected && this.renderNoConnection()}
        </View>
      );
    }
  }
  return Root;
}

export default setup;
