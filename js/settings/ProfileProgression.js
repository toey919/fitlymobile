import React from 'react';
import { Text, View, ScrollView } from 'react-native';
import HeaderInView from '../header/HeaderInView.js';
import Icon from 'react-native-vector-icons/Ionicons';
let data = [
  { title: 'Upload a profile picture', value: false },
  { title: 'Complete Tutorial', value: false },
  { title: 'Host an event', value: false },
  { title: 'Attend an event', value: false },
  { title: 'Join a workout now session', value: false },
  { title: 'Follow 4 accounts', value: false },
  { title: 'Get 10 followers', value: false },
  { title: 'Invite 3 friends', value: false },
  { title: 'Leave a review on the app store', value: false },
  { title: 'Send Feedback', value: false },
  { title: 'Share to social media', value: false },
  { title: 'Post a photo on Fitly', value: false }
];
class ProfileProgress extends React.Component {
  componentWillMount() {
    const { profileProgress } = this.props.navigation.state.params.user.public;
    if (profileProgress) {
      data[0].value = profileProgress.uploadPhoto;
      data[1].value = profileProgress.completeTutorial;
      data[2].value = profileProgress.hostAnEvent;
      data[3].value = profileProgress.attendAnEvent;
      data[4].value = profileProgress.joinAWorkoutNowSession;
      data[5].value = profileProgress.follow4Accounts;
      data[6].value = profileProgress.get10Followers;
      data[7].value = profileProgress.invite3Friends;
      data[8].value = profileProgress.leaveAReviewOnTheAppStore;
      data[9].value = profileProgress.sendFeedBack;
      data[10].value = profileProgress.shareToSocialMedia;
      data[11].value = profileProgress.shareAPhoto;
    }
  }
  _renderHeader() {
    return (
      <HeaderInView
        leftElement={{ icon: 'ios-close' }}
        title="Profile Progress"
        _onPressLeft={() => {
          this.props.navigation.goBack();
        }}
      />
    );
  }
  _renderContent() {
    return (
      <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
        {data.map((item, index) => (
          <View
            style={{
              flexDirection: 'row',
              height: 40,
              borderBottomColor: '#ccc',
              borderBottomWidth: 0.5,
              paddingLeft: 20,
              paddingRight: 10,
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
            key={index}
          >
            <Text
              style={{
                fontSize: 16,
                color: item.value ? '#000' : '#ccc'
              }}
            >
              {item.title}
            </Text>
            <View
              style={{
                width: 30,
                height: 30,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                borderColor: '#ccc'
              }}
            >
              {item.value ? (
                <Icon
                  name="md-checkmark"
                  style={{ color: 'green', fontSize: 26 }}
                />
              ) : (
                false
              )}
            </View>
          </View>
        ))}
      </ScrollView>
    );
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {this._renderHeader()}
        {this._renderContent()}
      </View>
    );
  }
}
export default ProfileProgress;
