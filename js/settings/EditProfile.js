import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
  Slider,
  Dimensions,
  TextInput
} from 'react-native';
import Firebase from 'firebase';
const { height, width } = Dimensions.get('window');
import { GoogleSignin } from 'react-native-google-signin';
import Icon from 'react-native-vector-icons/Ionicons';
import AutoExpandingTextInput from '../common/AutoExpandingTextInput.js';
import {
  composeStyle,
  optionStyle,
  feedEntryStyle,
  FitlyBlue,
  commonStyle
} from '../styles/styles.js';

import { firebaseGetCurrentUser } from '../library/firebaseHelpers.js';
import {
  asyncFBLoginWithPermission,
  fetchFBProfile
} from '../library/asyncFBLogin.js';

const Settings = StyleSheet.create({
  btn: {
    backgroundColor: FitlyBlue,
    width,
    height: 44,
    justifyContent: 'center'
  }
});

export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    const { first_name, last_name, summary } = this.props.user.public;
    const { height, weight, activeLevel } = this.props.user.private;
    this.state = {
      editFName: false,
      editLName: false,
      editSummary: false,
      summary,
      fName: first_name,
      lName: last_name,
      success: false,
      error: false
    };
    this.userDataRef = this.props.FitlyFirebase.database().ref(
      'users/' + this.props.uID
    );
  }
  componentDidMount() {
    if (this.props.focusOnSummary) {
      this.setState({ editSummary: true }, () => this.refs.summary.focus());
    }
  }
  componentWillMount() {
    this._setupGoogleSignin();
  }
  _clear() {
    this.setState({
      error: false,
      success: false
    });
  }
  async _setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true });
      await GoogleSignin.configure({
        iosClientId:
          '356069342993-megogq783oeuc0598kl34dehvh2551h5.apps.googleusercontent.com',
        webClientId:
          '356069342993-1lflsl8sle1t4o94367ahmklcvk1adp0.apps.googleusercontent.com'
      });
    } catch (err) {
      console.log('Google signin error', err.code, err.message);
    }
  }
  _updateData() {
    this._clear();
    if (this.state.fName.length === 0 || this.state.lName.length === 0) {
      this.setState({
        error: "Can't have an empty name field"
      });
      return;
    }
    const updatePublic = {
      ...this.props.user.public,
      first_name: this.state.fName,
      last_name: this.state.lName,
      summary: this.state.summary
    };

    const updateObj = {};
    updateObj['/public'] = updatePublic;

    let user = this.props.FitlyFirebase.auth().currentUser;
    user
      .updateProfile({
        displayName: this.state.fName + ' ' + this.state.lName
      })
      .then(
        () => {
          this.userDataRef
            .update(updateObj)
            .then(
              this.setState({ success: 'Success' }, () =>
                firebaseGetCurrentUser()
              )
            );
        },
        error => {
          this.setState({ error: 'An error has happened: ' + error });
        }
      );
  }
  _renderSummary() {
    return (
      <View style={[optionStyle.entry, {}]}>
        {this.state.editSummary ? (
          <TextInput
            ref="summary"
            clearButtonMode="always"
            autoFocus={true}
            onChangeText={text => this.setState({ summary: text })}
            value={this.state.summary}
            style={{
              marginLeft: 20,
              width,
              fontSize: 16,
              marginTop: 15,
              color: '#000'
            }}
            multiline={true}
            returnKeyType={'done'}
            onSubmitEditing={() => this.setState({ editSummary: false })}
            onEndEditing={() => this.setState({ editSummary: false })}
            placeholder="Summary"
            placeholderTextColor="grey"
            maxLength={60}
          />
        ) : (
          <TouchableOpacity
            style={{ width, height: 65, justifyContent: 'center' }}
            onPress={() => this.setState({ editSummary: true })}
          >
            <View>
              <Text
                style={{
                  marginLeft: 20,
                  fontSize: 12,
                  letterSpacing: 2,
                  color: '#ccc'
                }}
              >
                PROFILE SUMMARY:
              </Text>
              <Text style={{ marginLeft: 20, fontSize: 16 }}>
                {this.state.summary && this.state.summary.length
                  ? this.state.summary
                  : `I haven't filled this yet!`}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  }
  _renderFirstName() {
    return (
      <View style={[optionStyle.entry, {}]}>
        {this.state.editFName ? (
          <TextInput
            clearButtonMode="always"
            autoFocus={true}
            onChangeText={text => this.setState({ fName: text })}
            value={this.state.fName}
            style={{
              marginLeft: 20,
              width,
              fontSize: 16,
              marginTop: 15,
              color: '#000'
            }}
            multiline={true}
            returnKeyType={'done'}
            onSubmitEditing={() => this.setState({ editFName: false })}
            onEndEditing={() => this.setState({ editFName: false })}
            placeholder="First Name"
            placeholderTextColor="grey"
          />
        ) : (
          <TouchableOpacity
            style={{ width, height: 65, justifyContent: 'center' }}
            onPress={() => this.setState({ editFName: true })}
          >
            <View>
              <Text
                style={{
                  marginLeft: 20,
                  fontSize: 12,
                  letterSpacing: 2,
                  color: '#ccc'
                }}
              >
                FIRST NAME:
              </Text>
              <Text style={{ marginLeft: 20, fontSize: 16 }}>
                {this.state.fName.length ? this.state.fName : ''}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  }

  _renderLastName() {
    return (
      <View style={[optionStyle.entry, { justifyContent: 'center' }]}>
        {this.state.editLName ? (
          <TextInput
            clearButtonMode="always"
            autoFocus={true}
            onChangeText={text => this.setState({ lName: text })}
            value={this.state.lName}
            style={{
              marginLeft: 40,
              width,
              fontSize: 16,
              marginTop: 15,
              color: '#000'
            }}
            multiline={true}
            onSubmitEditing={() => this.setState({ editLName: false })}
            onEndEditing={() => this.setState({ editLName: false })}
            placeholder="Last Name"
            placeholderTextColor="grey"
          />
        ) : (
          <TouchableOpacity
            style={{ width, height: 65, justifyContent: 'center' }}
            onPress={() => this.setState({ editLName: true })}
          >
            <View>
              <Text
                style={{
                  marginLeft: 20,
                  fontSize: 12,
                  letterSpacing: 2,
                  color: '#ccc'
                }}
              >
                LAST NAME:
              </Text>
              <Text style={{ marginLeft: 20, fontSize: 16 }}>
                {this.state.lName.length ? this.state.lName : ''}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  }

  _renderStats() {
    return (
      <View
        style={[
          optionStyle.entry,
          { alignItems: 'center', borderTopWidth: 0.5, borderTopColor: '#ccc' }
        ]}
      >
        <TouchableOpacity
          style={{ width, height: 65, justifyContent: 'center' }}
          onPress={() => {
            this.props.navigation.navigate('SetupStatsView', { edit: true });
          }}
        >
          <Text
            style={{
              marginLeft: 20,
              fontSize: 12,
              letterSpacing: 2,
              color: '#ccc'
            }}
          >
            UPDATE STATS
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  _handleFBLogin() {
    const { FitlyFirebase, action } = this.props;
    (async () => {
      try {
        action.setLoadingState(true);
        const data = await asyncFBLoginWithPermission([
          'public_profile',
          'email',
          'user_friends',
          'user_location',
          'user_birthday'
        ]);
        const userFBprofile = await fetchFBProfile(data.credentials.token);
        const { id } = userFBprofile;
        await FitlyFirebase.database()
          .ref('users/' + this.props.uID + '/public')
          .update({
            FacebookID: id,
            userFBprofile
          });
        await firebaseGetCurrentUser();
        action.setLoadingState(false);
      } catch (error) {
        action.setLoadingState(false);
        console.log(error);
      }
    })();
  }
  _renderConnectYourFacebookAccount() {
    return (
      <View
        style={[
          optionStyle.entry,
          { alignItems: 'center', borderTopWidth: 0.5, borderTopColor: '#ccc' }
        ]}
      >
        <TouchableOpacity
          style={{
            width,
            height: 65,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingRight: 20
          }}
          disabled={!!this.props.user.public.FacebookID}
          onPress={() => this._handleFBLogin()}
        >
          <Text
            style={{
              marginLeft: 20,
              fontSize: 12,
              letterSpacing: 2,
              color: this.props.user.public.FacebookID ? '#000' : '#ccc'
            }}
          >
            CONNECT YOUR FACEBOOK ACCOUNT
          </Text>
          {this.props.user.public.FacebookID && (
            <Icon name="md-checkmark" size={30} color="green" />
          )}
        </TouchableOpacity>
      </View>
    );
  }
  _renderConnectYourLinkedInAccount() {
    return (
      <View
        style={[
          optionStyle.entry,
          { alignItems: 'center', borderTopWidth: 0.5, borderTopColor: '#ccc' }
        ]}
      >
        <TouchableOpacity
          style={{ width, height: 65, justifyContent: 'center' }}
          disabled={!!this.props.user.public.LinkedInID}
          // onPress={() => {
          //   this.props.navigation.navigate('SetupStatsView', { edit: true });
          // }}
        >
          <Text
            style={{
              marginLeft: 20,
              fontSize: 12,
              letterSpacing: 2,
              color: this.props.user.public.LinkedInID ? '#000' : '#ccc'
            }}
          >
            CONNECT YOUR LINKEDIN ACCOUNT
          </Text>
          {this.props.user.public.LinkedInID && (
            <Icon name="md-checkmark" size={30} color="green" />
          )}
        </TouchableOpacity>
      </View>
    );
  }
  _renderConnectYourGoogleAccount() {
    return (
      <View
        style={[
          optionStyle.entry,
          { alignItems: 'center', borderTopWidth: 0.5, borderTopColor: '#ccc' }
        ]}
      >
        <TouchableOpacity
          style={{
            width,
            height: 65,
            justifyContent: 'space-between',
            paddingRight: 20,
            alignItems: 'center',
            flexDirection: 'row'
          }}
          disabled={!!this.props.user.public.GoogleID}
          onPress={() => {
            GoogleSignin.signIn()
              .then(async data => {
                try {
                  this.props.action.setLoadingState(true);
                  const userGoogleprofile = await GoogleSignin.currentUserAsync();
                  const { id } = userGoogleprofile;
                  await this.props.FitlyFirebase.database()
                    .ref('users/' + this.props.uID + '/public')
                    .update({
                      GoogleID: id,
                      userGoogleprofile
                    });
                  await firebaseGetCurrentUser();
                  this.props.action.setLoadingState(false);
                } catch (error) {
                  this.props.action.setLoadingState(false);
                  console.log(error);
                }
              })
              .catch(err => {
                this.props.action.setLoadingState(false);
                console.log('WRONG SIGNIN', err);
              })
              .done();
          }}
        >
          <Text
            style={{
              marginLeft: 20,
              fontSize: 12,
              letterSpacing: 2,
              color: this.props.user.public.GoogleID ? '#000' : '#ccc'
            }}
          >
            CONNECT YOUR GOOGLE ACCOUNT
          </Text>
          {this.props.user.public.GoogleID && (
            <Icon name="md-checkmark" size={30} color="green" />
          )}
        </TouchableOpacity>
      </View>
    );
  }

  _renderWeight() {
    return (
      <View style={[optionStyle.entry, { paddingTop: 15, paddingBottom: 15 }]}>
        {this.state.editWeight ? (
          <AutoExpandingTextInput
            clearButtonMode="always"
            autoFocus={true}
            onChangeText={text => this.setState({ weight: text })}
            style={{ marginLeft: 20, width: 300, fontSize: 16, color: '#000' }}
            multiline={true}
            onSubmitEditing={() => this.setState({ editWeight: false })}
            onEndEditing={() => this.setState({ editWeight: false })}
            placeholder="in lbs"
            placeholderTextColor="grey"
          />
        ) : (
          <View>
            <Text style={{ marginLeft: 20 }}>Weight: </Text>
            <Text style={{ marginLeft: 20, width: 300 }}>
              {this.state.weight ? this.state.weight : ''}
            </Text>
          </View>
        )}
        <TouchableOpacity
          style={optionStyle.icon}
          onPress={() => this.setState({ editWeight: !this.state.editWeight })}
        >
          <Icon name="ios-create-outline" size={30} color="#bbb" />
        </TouchableOpacity>
      </View>
    );
  }

  _renderActiveLevel() {
    return (
      <View
        style={[
          optionStyle.entry,
          { flexDirection: 'column', paddingTop: 15, paddingBottom: 15 }
        ]}
      >
        <Text>Activity Level</Text>
        <Slider
          style={{ width: 260, alignSelf: 'center' }}
          value={this.state.activeLevel}
          minimumValue={0}
          maximumValue={10}
          step={0.5}
          onValueChange={value => this.setState({ activeLevel: value })}
        />
        <Text style={{ fontSize: 20 }}>{this.state.activeLevel}</Text>
      </View>
    );
  }
  _renderSegmentHeader(headerText) {
    return (
      <View
        style={{
          width,
          height: 30,
          justifyContent: 'center',
          padding: 5,
          paddingLeft: 20,
          backgroundColor: 'rgba(251,251,251,1)'
        }}
      >
        <Text style={{ fontWeight: 'bold', fontSize: 12, opacity: 0.8 }}>
          {headerText}
        </Text>
      </View>
    );
  }
  render() {
    return (
      <View style={{ flex: 1, height: height - 90 }}>
        {this._renderSegmentHeader('EDIT PROFILE INFO')}
        {this._renderFirstName()}
        {this._renderLastName()}
        {this._renderSummary()}
        {this._renderSegmentHeader('BODY STATS')}
        {this._renderStats()}
        {this._renderSegmentHeader('CONNECT SOCIAL ACCOUNTS')}
        {this._renderConnectYourFacebookAccount()}
        {this._renderConnectYourLinkedInAccount()}
        {this._renderConnectYourGoogleAccount()}
        <View style={{ marginTop: 40 }}>
          {this.state.error ? (
            <Text
              style={{
                height: 40,
                fontWeight: '100',
                textAlign: 'center',
                color: '#FF0000'
              }}
            >
              {this.state.error}
            </Text>
          ) : null}
          {this.state.success ? (
            <Text style={commonStyle.success}>{this.state.success}</Text>
          ) : null}
        </View>
      </View>
    );
  }
}
