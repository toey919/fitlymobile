import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from 'react-native';
import firebase from 'firebase';
const { width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import AutoExpandingTextInput from '../common/AutoExpandingTextInput.js';
import {
  composeStyle,
  optionStyle,
  feedEntryStyle,
  FitlyBlue,
  commonStyle
} from '../styles/styles.js';

const Settings = StyleSheet.create({
  btn: {
    borderRadius: 2,
    backgroundColor: FitlyBlue,
    width: 150,
    height: 40,
    justifyContent: 'center',
    margin: 10,
    shadowColor: 'black',
    shadowOpacity: 0.6,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 2
  }
});

export default class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      success: false,
      currentPassword: '',
      newPassword: '',
      confirmPassword: ''
    };
  }

  _clear() {
    this.setState({
      error: false,
      success: false
    });
  }

  _updatePassword() {
    this._clear();
    if (
      !this.state.newPassword ||
      this.state.newPassword.length == 0 ||
      !this.state.confirmPassword ||
      this.state.confirmPassword.length == 0 ||
      !this.state.currentPassword ||
      this.state.currentPassword.length == 0
    ) {
      this.setState({ error: 'Fields cannot be empty.' });
      return;
    } else if (this.state.newPassword.length < 6) {
      this.setState({ error: 'Password should be at least 6 character long.' });
      return;
    } else if (this.state.newPassword !== this.state.confirmPassword) {
      this.setState({ error: 'New and Confirm New Password do not Match.' });
      return;
    } else {
      const emailCred = firebase.auth.EmailAuthProvider.credential(
        this.props.FitlyFirebase.auth().currentUser.email,
        this.state.currentPassword
      );
      console.log(firebase.auth().currentUser);
      firebase
        .auth()
        .currentUser.reauthenticateWithCredential(emailCred)
        .then(() => {
          this.props.FitlyFirebase.auth()
            .currentUser.updatePassword(this.state.newPassword)
            .then(() => {
              this.setState({ success: 'Success' });
            })
            .catch(e => {
              this.setState({ error: e.message.toUpperCase() });
            });
        })
        .catch(e => {
          this.setState({ error: e.message.toUpperCase() });
        });
    }
  }

  render() {
    return (
      <View style={{ flex: 1, paddingTop: 30 }}>
        <View
          style={{
            height: 51,
            width,
            borderBottomWidth: 0.5,
            marginVertical: 10,
            borderBottomColor: '#ccc'
          }}
        >
          <Text style={{ letterSpacing: 2, fontSize: 10 }}>
            CURRENT PASSWORD
          </Text>
          <TextInput
            style={{ height: 35, width, padding: 8 }}
            placeholder={'New Password'}
            onChangeText={text => this.setState({ currentPassword: text })}
            secureTextEntry={!this.state.showPassword}
          />
        </View>
        <View
          style={{
            height: 51,
            width,
            borderBottomWidth: 0.5,
            marginVertical: 10,
            borderBottomColor: '#ccc'
          }}
        >
          <Text style={{ letterSpacing: 2, fontSize: 10 }}>NEW PASSWORD</Text>
          <TextInput
            style={{ height: 35, width, padding: 8 }}
            placeholder={'New Password'}
            onChangeText={text => this.setState({ newPassword: text })}
            secureTextEntry={!this.state.showPassword}
          />
        </View>
        <View
          style={{
            height: 51,
            width,
            borderBottomWidth: 0.5,
            borderBottomColor: '#ccc'
          }}
        >
          <Text style={{ letterSpacing: 2, fontSize: 10 }}>
            CONFIRM NEW PASSWORD
          </Text>
          <TextInput
            style={{ height: 35, width, padding: 8 }}
            placeholder={'Confirm New Password'}
            onChangeText={text => this.setState({ confirmPassword: text })}
            secureTextEntry={!this.state.showPassword}
          />
        </View>
        <TouchableOpacity
          style={{ alignSelf: 'flex-end', marginRight: 10 }}
          onPress={() =>
            this.setState({ showPassword: !this.state.showPassword })
          }
        >
          <Text style={{ letterSpacing: 2, fontSize: 10 }}>SHOW PASSWORD</Text>
        </TouchableOpacity>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            paddingTop: 20
          }}
        >
          <TouchableOpacity
            onPress={this._updatePassword.bind(this)}
            style={Settings.btn}
          >
            <Text style={{ color: 'white', textAlign: 'center' }}>
              Change Password
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          {this.state.error ? (
            <Text
              style={{
                height: 40,
                fontWeight: '100',
                textAlign: 'center',
                color: '#FF0000'
              }}
            >
              {this.state.error}
            </Text>
          ) : null}
          {this.state.success ? (
            <Text style={commonStyle.success}>{this.state.success}</Text>
          ) : null}
        </View>
      </View>
    );
  }
}
