import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableHighlight,
  Linking,
  Animated,
  Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';
import EditProfile from './EditProfile';
import ChangePassword from './ChangePassword';
import LogoutBtn from '../common/LogoutBtn.js';
import HeaderInView from '../header/HeaderInView.js';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { resetTo, pop } from '../actions/navigation.js';
import { setLoadingState } from '../actions/app.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ComingSoon from '../common/commingSoon';
import { FitlyBlue } from '../styles/styles';
let count = 0;
class SettingsMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      view: 'all',
      headerText: 'Settings'
    };
    count = 0;
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.view !== prevState.view) {
      this._returnHeaderText();
    }
  }
  componentWillMount() {
    const { profileProgress = {} } = this.props.user.public;
    const profileProgressVal = Object.values(profileProgress);
    for (let i = 0; i < profileProgressVal.length; i++) {
      if (profileProgressVal[i]) {
        count++;
      }
    }
  }
  componentDidMount() {
    if (this.props.navigation.state.params.editSummary) {
      this.setState({ view: 'EditProfile' });
    }
  }
  _renderHeader() {
    return (
      <HeaderInView
        leftElement={{ icon: 'ios-close' }}
        rightElement={
          this.state.view === 'EditProfile' ? { icon: 'md-checkmark' } : null
        }
        title={this.state.headerText}
        _onPressRight={() => this.refs.EditProfile._updateData()}
        _onPressLeft={() => {
          if (this.props.navigation.state.params.editSummary) {
            this.props.navigation.goBack();
          }
          if (this.state.view !== 'all') this._goBack();
          else this.props.navigation.goBack();
        }}
      />
    );
  }

  _renderSettings(entries, removeHeaderAndLogout) {
    return (
      <View>
        {!removeHeaderAndLogout && (
          <TouchableHighlight
            underlayColor={'#eee'}
            onPress={() => {
              this.props.navigation.navigate('ProfileProgress', {
                user: this.props.user
              });
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                borderBottomColor: '#ccc',
                height: 40,
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingLeft: 32,
                paddingRight: 10
              }}
            >
              <Text style={{ fontSize: 16 }}>Profile Progress</Text>
              {this._renderProfileProgress()}
            </View>
          </TouchableHighlight>
        )}
        {entries.map(e => {
          return (
            <TouchableHighlight
              underlayColor={'#eee'}
              onPress={this._changeView.bind(this, e)}
              key={e}
            >
              <View
                style={{
                  flexDirection: 'row',
                  borderBottomWidth: 1,
                  borderBottomColor: '#ccc',
                  height: 40,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 32,
                  paddingRight: 20
                }}
                key={JSON.stringify(e)}
              >
                <Text style={{ fontSize: 16 }}>{e}</Text>
                <Icon name="ios-arrow-forward" />
              </View>
            </TouchableHighlight>
          );
        })}
        {!removeHeaderAndLogout && (
          <LogoutBtn navigation={this.props.navigation} />
        )}
      </View>
    );
  }
  _renderProfileProgress() {
    return (
      <View style={{ transform: [{ rotate: '270deg' }] }}>
        <AnimatedCircularProgress
          size={30}
          width={3}
          fill={count / 12 * 100}
          tintColor={FitlyBlue}
          backgroundColor="#eee"
        >
          {fill => (
            <Ionicons
              name={fill == 100 ? 'ios-star' : 'ios-star-outline'}
              size={28}
              style={{
                color: 'yellow',
                left: 3,
                position: 'absolute',
                backgroundColor: 'transparent',
                transform: [{ rotate: '20deg' }]
              }}
            />
          )}
        </AnimatedCircularProgress>
      </View>
    );
  }
  _changeView(e) {
    switch (e) {
      case 'Edit Profile':
        this.setState({
          view: 'EditProfile'
        });
        break;
      case 'Change Password':
        this.setState({
          view: 'ChangePassword'
        });
        break;
      case 'Find a Trainer':
        this.setState({
          view: 'FindTrainer'
        });
        break;
      case 'Change Password':
        this.setState({
          view: 'ChangePassword'
        });
        break;
      case 'Upgrade Account':
        this.setState({
          view: 'UpgradeAccount'
        });
        break;
      case 'Leave Feedback':
        this.setState({
          view: 'LeaveFeedback'
        });
        break;
      case 'Rate the App':
        Platform.OS == 'ios'
          ? Linking.openURL('https://itunes.apple.com/app/id1215863337')
          : Linking.openURL(
              'https://play.google.com/store/apps/details?id=com.thefitlyapp'
            );
        break;
      case 'Like us on Twitter':
        Linking.openURL('https://twitter.com/The_Fitly_App');
        break;
      case 'Like us on Instagram':
        Linking.openURL('https://www.instagram.com/fitly_app/');
        break;
      case 'Find us on Facebook':
        Linking.openURL('https://www.facebook.com/fitlyapp/');
        break;
      case 'App Walkthrough Mode':
        this.props.navigation.dispatch(
          NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'TabNavigator',
                params: { newUser: true }
              })
            ]
          })
        );
        break;
      case 'Report an Issue':
        this.setState({
          view: 'ReportAnIssue'
        });
        break;
      case 'Report Spam & Abuse':
        this._handleEmail(`issues@thefitlyapp.com`, `Report Spam & Abuse`);
        break;
      case 'Suggest Improvement':
        this._handleEmail(`issues@thefitlyapp.com`, `Improvement Suggestion`);
        break;
      case 'Report a Problem':
        this._handleEmail(`issues@thefitlyapp.com`, `User Problem`);
        break;
    }
  }

  _renderComingSoon() {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <ComingSoon />
      </View>
    );
  }
  _handleEmail(email, subject) {
    Linking.openURL(`mailto:${email}?subject=${subject}`);
  }

  _renderContactSupport() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Text style={{ textAlign: 'center', lineHeight: 22 }}>
          Having some issues?{'\n'}We will be more than happy to get them fixed.
          {'\n'}Feel free to contact us on the link below
          {'\n © 2018 The Fitly App,Inc\nALL RIGHTS RESERVED'}
        </Text>
        <Text
          onPress={() =>
            this._handleEmail(`feedback@thefitlyapp.com`, `Feedback from user`)
          }
          style={{
            textAlign: 'center',
            color: FitlyBlue,
            fontSize: 18,
            lineHeight: 50,
            fontWeight: 'bold'
          }}
        >
          support@thefitlyapp.com
        </Text>
      </View>
    );
  }
  _renderReportAnIssue() {
    return (
      <View style={{ flex: 1 }}>
        {this._renderSettings(
          ['Report Spam & Abuse', 'Suggest Improvement', 'Report a Problem'],
          true
        )}
      </View>
    );
  }
  _returnHeaderText() {
    switch (this.state.view) {
      case 'all':
        this.setState({ headerText: 'Settings' });
        break;
      case 'EditProfile':
        this.setState({ headerText: 'Edit Profile' });
        break;
      case 'ChangePassword':
        this.setState({ headerText: 'Change Password' });
        break;
      case 'FindTrainer':
        this.setState({ headerText: 'Find Trainer' });
        break;
      case 'UpgradeAccount':
        this.setState({ headerText: 'Upgrade Account' });
        break;
      case 'LeaveFeedback':
        this.setState({ headerText: 'Leave Feedback' });
        break;
      case 'ReportAnIssue':
        this.setState({ headerText: 'Report An Issue' });
        break;
    }
  }
  _renderSettingView() {
    switch (this.state.view) {
      case 'EditProfile':
        return (
          <EditProfile
            {...this.props}
            ref={'EditProfile'}
            focusOnSummary={this.props.navigation.state.params.editSummary}
            goBack={this._goBack.bind(this)}
          />
        );
        break;
      case 'ChangePassword':
        return (
          <ChangePassword {...this.props} goBack={this._goBack.bind(this)} />
        );
        break;
      case 'FindTrainer':
        return this._renderComingSoon();
        break;
      case 'UpgradeAccount':
        return this._renderComingSoon();
        break;
      case 'ContactSupport':
        //Support implementation to be performed
        return this._renderContactSupport();
        break;
      case 'LeaveFeedback':
        return this._renderContactSupport();
        break;
      case 'ReportAnIssue':
        return this._renderReportAnIssue();
        break;
    }
  }

  _goBack() {
    this.setState({
      view: 'all'
    });
  }

  render() {
    let options = [
      'Find a Trainer',
      'Upgrade Account',
      'Edit Profile',
      'Leave Feedback',
      'Rate the App',
      'Find us on Facebook',
      'Like us on Instagram',
      'Like us on Twitter',
      'Report an Issue',
      'App Walkthrough Mode'
    ];
    if (this.props.user.public.provider === 'Firebase')
      options.push('Change Password');
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {this._renderHeader()}
        <ScrollView contentContainerStyle={{ flex: 1 }}>
          {this.state.view === 'all' ? (
            <View style={{ flex: 1 }}>{this._renderSettings(options)}</View>
          ) : (
            this._renderSettingView()
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    uID: state.auth.uID,
    user: state.user.user,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    exnavigation: bindActionCreators({ resetTo, pop }, dispatch),
    action: bindActionCreators(
      {
        setLoadingState
      },
      dispatch
    )
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsMenu);
