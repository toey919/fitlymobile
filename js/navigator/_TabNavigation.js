import React from 'react';
import { TabNavigator, TabBarBottom } from 'react-navigation';
import Profile from '../tabs/profile/_Profile.js';
import Activity from '../tabs/activity/_Activity';
import Notification from '../tabs/notification/_Notification';
import Search from '../tabs/search/_Search';
import Connect from '../tabs/connect/_Connect';
import { View, Dimensions, Animated, Image } from 'react-native';
const { width } = Dimensions.get('window');
import FAIcon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IonIcons from 'react-native-vector-icons/Ionicons';
import Walkthough from './WalkthroughModal';
import { FitlyBlue } from '../styles/styles.js';
const returnIcon = routeName => {
  if (routeName === 'Activity')
    return <FAIcon name="calendar" color="white" size={24} />;
  if (routeName === 'Search')
    return <FAIcon name="search" size={24} color="white" />;
  if (routeName === 'Profile')
    return <FAIcon name="user" size={24} color="white" />;
  if (routeName === 'Notification')
    return <FAIcon name="bell-o" size={24} color="white" />;
  // if (routeName === 'Connect')
  return (
    <Image
      source={require('../../img/activity-tab-Icon.png')}
      style={{ height: 35, width: 35, tintColor: '#fff' }}
      resizeMethod={'auto'}
      resizeMode={'cover'}
    />
  );
};
const TabBarNavigation = TabNavigator(
  {
    Activity: { screen: Activity },
    Search: { screen: Search },
    Profile: { screen: Profile },
    Notification: { screen: Notification },
    Connect: { screen: Connect }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        return (
          <Animated.View
            style={{
              width: width / 5,
              height: 49,
              alignItems: 'center',
              justifyContent: 'center',
              opacity: 1,
              flexDirection: 'row'
            }}
          >
            {returnIcon(routeName)}
          </Animated.View>
        );
      }
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: true,
    initialRouteName: 'Profile',
    lazy: true,
    tabBarOptions: {
      tabStyle: { overflow: 'visible' },
      showLabel: false,
      showIcon: true,
      activeBackgroundColor: 'rgba(29, 47, 123, 1)',
      inactiveBackgroundColor: 'rgba(61,61,61,0.9)',
      indicatorStyle: null
    }
  }
);

class TabNavigationWrapper extends React.Component {
  state = { currentIndex: 2, startProfTutAnim: false };
  render() {
    const { navigation } = this.props;
    const { newUser } = navigation.state.params;
    return (
      <View style={{ flex: 1 }}>
        <TabBarNavigation
          screenProps={{
            rootNavigation: navigation,
            startProfTutAnim: this.state.startProfTutAnim,
            currentActiveIndex: this.state.currentIndex
          }}
          onNavigationStateChange={(prevState, nextState) => {
            // console.log(prevState, nextState, 'xxxx');
            this.setState({ currentIndex: nextState.index });
          }}
        />
        <Walkthough
          newUser={newUser}
          routeIndex={this.state.currentIndex}
          runAnim={() => {
            this.setState({ startProfTutAnim: true });
          }}
        />
      </View>
    );
  }
}

export default TabNavigationWrapper;
