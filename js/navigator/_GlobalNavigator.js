/* @flow */
import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import WelcomeView from '../login/WelcomeView.js';
import SignUpView from '../login/SignUpView.js';
import SignInView from '../login/SignInView.js';
import VerifyEmailView from '../login/VerifyEmailView.js';
import {
  SetupProfileView,
  SetupStatsView,
  SetupLocationView
} from '../login/SetupView.js';
import OnBoardingSlides from '../login/OnBoardingSlides.js';
import SettingsMenu from '../settings/SettingsMenu.js';
import MakePost from '../common/post/MakePost.js';
import ComposePost from '../common/post/ComposePost.js';
import CreateActivityScene from '../common/activity/CreateActivityScene.js';
import SelectDateScene from '../common/activity/SelectDateScene.js';
import SelectLocationScene from '../common/activity/SelectLocationScene.js';
import SelectInvitesScene from '../common/activity/SelectInvitesScene.js';
import SelectAdminsScene from '../common/activity/SelectAdminsScene.js';
import SelectContactScene from '../common/activity/SelectContactScene.js';
import EventOptionsMenu from '../common/activity/EventOptionsMenu.js';
import LocationPicker from '../common/LocationPicker.js';
import ChatSearch from '../messaging/ChatSearch.js';
import MessagingScene from '../messaging/MessagingScene.js';
import Chat from '../messaging/Chat.js';
import ActivityLevel from '../tabs/connect/ActivityLevel.js';
import MatchingView from '../tabs/connect/MatchingView.js';
import Connect from '../tabs/connect/Connect.js';
import SessionView from '../common/workoutSession/SessionView.js';
import UserListView from '../common/UserListView.js';
import InterestsView from '../common/InterestsView';
import TrainerListView from '../common/TrainerListView';
import ReportScene from '../common/ReportScene';
import EventScene from '../common/activity/EventScene';
import SessionListView from '../common/workoutSession/SessionView';
import ProfileProgress from '../settings/ProfileProgression';
import NewTabNavigation from './_TabNavigation';
import ProfileEntry from '../common/ProfileEntry';
import { crossFade } from '../common/crossFadeTransitionConfig.js';
import EditAdminScene from '../common/activity/EditAdminScene';
const ROUTES = {
  WelcomeView: { screen: WelcomeView },
  SignUpView: { screen: SignUpView },
  SignInView: { screen: SignInView },
  EditAdminScene: { screen: EditAdminScene },
  SetupProfileView: { screen: SetupProfileView },
  SetupStatsView: { screen: SetupStatsView },
  SetupLocationView: { screen: SetupLocationView },
  TabNavigator: { screen: NewTabNavigation },
  SettingsMenu: { screen: SettingsMenu },
  ProfileEntry: { screen: ProfileEntry },
  ProfileProgress: { screen: ProfileProgress },
  MakePost: { screen: MakePost },
  ComposePost: { screen: ComposePost },
  OnBoardingSlides: { screen: OnBoardingSlides },
  CreateActivityScene: { screen: CreateActivityScene },
  SelectDateScene: { screen: SelectDateScene },
  SelectLocationScene: { screen: SelectLocationScene },
  SelectAdminsScene: { screen: SelectAdminsScene },
  SelectInvitesScene: { screen: SelectInvitesScene },
  SelectContactScene: { screen: SelectContactScene },
  VerifyEmailView: { screen: VerifyEmailView },
  LocationPicker: { screen: LocationPicker },
  MessagingScene: { screen: MessagingScene },
  Chat: { screen: Chat },
  ChatSearch: { screen: ChatSearch },
  ActivityLevel: { screen: ActivityLevel },
  MatchingView: { screen: MatchingView },
  Connect: { screen: Connect },
  SessionView: { screen: SessionView },
  UserListView: { screen: UserListView },
  InterestsView: { screen: InterestsView },
  TrainerListView: { screen: TrainerListView },
  ReportScene: { screen: ReportScene },
  EventScene: { screen: EventScene },
  EventOptionsMenu: { screen: EventOptionsMenu },
  SessionListView: { screen: SessionListView }
};

const AppLoginScreen = StackNavigator(ROUTES, {
  headerMode: 'none',
  initialRouteName: 'WelcomeView',
  transitionConfig: crossFade
});

export default AppLoginScreen;
