import * as React from 'react';
import {
  Modal,
  View,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Text
} from 'react-native';
import { storeUserProfile } from '../actions/user';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import Events from '../walkthrough/event';
import WorkoutNow from '../walkthrough/workoutNow';
import Welcome from '../walkthrough/welcome';
import Congrats from '../walkthrough/congrats';
import MoreTabs from '../walkthrough/moreTabs';
import FeedsSearch from '../walkthrough/feedsSearch';
import FindATrainer from '../walkthrough/findAtrainer';
import { FitlyBlue } from '../styles/styles';
import { bindActionCreators } from 'redux';
const { width, height } = Dimensions.get('window');
class Walkthrough extends React.Component {
  state = {
    modalVisible: false,
    activityTut: false,
    searchTut: false,
    profileTut: false,
    connectTut: false,
    dots: new Array(3).fill(false),
    currentWalkthroughPage: 0
  };
  componentDidUpdate() {
    if (
      this.state.profileTut &&
      this.state.activityTut &&
      this.state.searchTut &&
      this.state.connectTut
    ) {
      if (
        !this.props.user.public.profileProgress ||
        !this.props.user.public.profileProgress.completeTutorial
      ) {
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}/public/profileProgress`)
          .update({ completeTutorial: true });
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}`)
          .on('value', snap => this.props.action.storeUserProfile(snap.val()));
      }
    }
  }
  componentDidMount() {
    if (this.props.newUser) {
      this.setState({ modalVisible: true });
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.routeName === 3) return false;
    if (this.state.modalVisible) return true;
    if (
      nextState.activityTut &&
      nextState.connectTut &&
      nextState.profileTut &&
      nextState.searchTut
    ) {
      return false;
    }
    return true;
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.newUser) {
      if (this.props.routeIndex !== nextProps.routeIndex) {
        if (nextProps.routeIndex === 0 && !this.state.activityTut)
          this.setState({ modalVisible: true });
        if (nextProps.routeIndex === 1 && !this.state.searchTut) {
          this.setState(
            {
              dots: new Array(2).fill(false),
              currentWalkthroughPage: 0
            },
            () => {
              this.setState({ modalVisible: true });
            }
          );
        }
        if (nextProps.routeIndex === 2 && !this.state.profileTut) {
          this.setState({
            modalVisible: true,
            currentWalkthroughPage: 0,
            dots: new Array(3).fill(false)
          });
        }
        if (nextProps.routeIndex === 4 && !this.state.connectTut)
          this.setState({ modalVisible: true });
      }
    }
  }
  _renderActivityContent() {
    return (
      <Events
        onDonePress={() => {
          this.setState({ modalVisible: false, activityTut: true });
        }}
      />
    );
  }
  _renderConnectContent() {
    return (
      <WorkoutNow
        onDonePress={() => {
          this.setState({ modalVisible: false, connectTut: true });
        }}
      />
    );
  }
  _renderProfileContent() {
    return (
      <ScrollView
        horizontal
        ref={'slides'}
        scrollEventThrottle={16}
        onScroll={({ nativeEvent }) => {
          this.setState({
            currentWalkthroughPage: Math.round(
              nativeEvent.contentOffset.x / 276
            )
          });
        }}
        bounces={false}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      >
        <Welcome />
        <Congrats />
        <MoreTabs />
      </ScrollView>
    );
  }
  _renderSearch() {
    return (
      <ScrollView
        horizontal
        ref="slides"
        scrollEventThrottle={16}
        onScroll={({ nativeEvent }) => {
          this.setState({
            currentWalkthroughPage: Math.round(
              nativeEvent.contentOffset.x / (width - 100)
            )
          });
        }}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      >
        <FeedsSearch />
        <FindATrainer
          onDonePress={() => {
            this.setState({ modalVisible: false });
          }}
        />
      </ScrollView>
    );
  }
  _renderNavButtons() {
    return (
      <View
        style={{
          width: 250,
          height: 40,
          flexDirection: 'row',
          justifyContent: 'space-between',
          position: 'absolute',
          bottom: (height - 476.5) / 2 + 40,
          left: (width - 250) / 2
        }}
        onTouchStart={event => event.stopPropagation()}
      >
        {this.state.currentWalkthroughPage ? (
          <TouchableOpacity
            style={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: '#fff',
              borderColor: FitlyBlue,
              alignItems: 'center',
              justifyContent: 'center'
            }}
            onPress={() => {
              this.refs.slides.scrollTo({
                x: (this.state.currentWalkthroughPage - 1) * 276,
                animated: true
              });
            }}
          >
            <Icon
              name="md-arrow-back"
              size={30}
              color={FitlyBlue}
              style={{ backgroundColor: 'transparent' }}
            />
          </TouchableOpacity>
        ) : (
          <View
            style={{
              width: 40,
              height: 40,
              justifyContent: 'center'
            }}
          />
        )}
        <TouchableOpacity
          style={{
            width:
              this.state.currentWalkthroughPage ==
              (this.props.routeIndex === 2 ? 2 : 1)
                ? 70
                : 40,
            height: 40,
            borderRadius: 20,
            backgroundColor: '#fff',
            borderColor: FitlyBlue,
            alignItems: 'center',
            justifyContent: 'center'
          }}
          onPress={() => {
            if (
              this.state.currentWalkthroughPage !==
              (this.props.routeIndex === 2 ? 2 : 1)
            )
              this.refs.slides.scrollTo({
                x: (this.state.currentWalkthroughPage + 1) * 276,
                animated: true
              });
            else
              this.setState(
                {
                  modalVisible: false
                },
                () => {
                  if (this.props.routeIndex === 2) {
                    this.setState({ profileTut: true });
                    this.props.runAnim();
                  } else {
                    this.setState({ searchTut: true });
                  }
                }
              );
          }}
        >
          {this.state.currentWalkthroughPage <
          (this.props.routeIndex === 2 ? 2 : 1) ? (
            <Icon
              name="md-arrow-forward"
              size={30}
              color={FitlyBlue}
              style={{ backgroundColor: 'transparent' }}
            />
          ) : (
            <Text
              style={{
                fontWeight: 'bold',
                color: FitlyBlue,
                backgroundColor: 'transparent'
              }}
            >
              Done
            </Text>
          )}
        </TouchableOpacity>
      </View>
    );
  }
  _renderDots() {
    return (
      <View
        style={{
          height: 20,
          width: this.props.routeIndex == 1 ? 40 : 80,
          position: 'absolute',
          bottom: (height - 476.5) / 2,
          left: width / 2 - (this.props.routeIndex == 1 ? 20 : 40),
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}
        onTouchStart={event => event.stopPropagation()}
      >
        {this.state.dots.map((item, index) => (
          <View
            style={{
              height: 8,
              width: 8,
              backgroundColor:
                this.state.currentWalkthroughPage === index
                  ? FitlyBlue
                  : 'rgba(0,0,0,0.4)',
              borderRadius: 4,
              transform: [
                {
                  scale: this.state.currentWalkthroughPage === index ? 1.5 : 1
                }
              ]
            }}
            key={index}
          />
        ))}
      </View>
    );
  }
  _renderScene(routeIndex) {
    switch (routeIndex) {
      case 0:
        return this._renderActivityContent();
      case 1:
        return this._renderSearch();
      case 2:
        return this._renderProfileContent();
      case 4:
        return this._renderConnectContent();
    }
  }
  render() {
    return (
      <Modal
        onRequestClose={() => {
          this.setState({ modalVisible: false });
        }}
        visible={this.state.modalVisible}
        animationType={'fade'}
        transparent
      >
        <View
          style={{
            height,
            width,
            backgroundColor: 'rgba(0,0,0,0.5)',
            alignItems: 'center',
            justifyContent: 'center'
          }}
          onTouchStart={() => {
            this.setState({ modalVisible: false });
            switch (this.props.routeIndex) {
              case 0:
                this.setState({ activityTut: true });
                break;
              case 1:
                this.setState({ searchTut: true });
                break;
              case 2:
                this.setState({ profileTut: true });
                break;
              case 4:
                this.setState({ connectTut: true });
                break;
            }
          }}
        >
          <View
            style={{
              height: 476.5,
              width: 276,
              borderRadius: 10,
              backgroundColor: '#eee'
            }}
            onTouchStart={event => {
              event.stopPropagation();
            }}
          >
            {this._renderScene(this.props.routeIndex)}
          </View>
          {(this.props.routeIndex === 2 || this.props.routeIndex === 1) &&
            this._renderDots()}
          {(this.props.routeIndex === 1 || this.props.routeIndex === 2) &&
            this._renderNavButtons()}
        </View>
      </Modal>
    );
  }
}
const mapStateToProps = function(state) {
  return {
    user: state.user.user,
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        storeUserProfile
      },
      dispatch
    )
  };
};
const ConnectedWalkthrough = connect(mapStateToProps, mapDispatchToProps)(
  Walkthrough
);

export default ConnectedWalkthrough;
