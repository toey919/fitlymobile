// import ImagePicker from 'react-native-image-picker';
import ImageCropPicker from 'react-native-image-crop-picker';
import { Platform } from 'react-native';

const isAndroid = Platform.OS === 'android';

export const selectPictureCropper = isProfilePicture => {
  const options = {
    width: 320,
    height: isProfilePicture ? 320 : 150,
    cropperCircleOverlay: isProfilePicture ? true : false,
    cropping: true,
    compressImageQuality: 0.6
  };
  return new Promise((resolve, reject) => {
    ImageCropPicker.openPicker(options)
      .then(image => {
        image.uri = image.path;
        resolve(image);
      })
      .catch(err => {
        console.log('image cropper', err);
        reject(err);
      });
  });
};
export const selectPictureCropperCam = isProfilePicture => {
  const options = {
    width: 320,
    height: isProfilePicture ? 320 : 150,
    cropperCircleOverlay: isProfilePicture ? true : false,
    cropping: true,
    compressImageQuality: 0.6
  };
  return new Promise((resolve, reject) => {
    ImageCropPicker.openCamera(options)
      .then(image => {
        image.uri = image.path;
        resolve(image);
      })
      .catch(err => {
        console.log('image cropper', err);
        reject(err);
      });
  });
};
export const getImageFromCam = callback => {
  ImageCropPicker.openCamera({
    compressImageQuality: 0.6
  })
    .then(images => {
      callback(images);
    })
    .catch(error => {
      console.log('image from cam', error);
    });
};

export const getImageFromLib = callback => {
  ImageCropPicker.openPicker({
    cropping: true,
    multiple: true,
    compressImageQuality: 0.6
  })
    .then(images => {
      callback(images);
    })
    .catch(error => {
      console.log('image from lib', error);
    });
};
