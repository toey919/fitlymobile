import Geocoder from 'react-native-geocoder';
import { Platform, PermissionsAndroid } from 'react-native';

// fall back to google in case something happens, need google api key
import { GEOCODING_API_KEY } from '../../credentials/GOOGLE_PLACE_API_KEY';

Geocoder.fallbackToGoogle(GEOCODING_API_KEY);

const isAndroid = Platform.OS === 'android';

const getCurrentPositionOptions = {
  enableHighAccuracy: false,
  timeout: 20000
};

if (!isAndroid) {
  getCurrentPositionOptions.maximumAge = 1000;
}

export const getCurrentPosition = () =>
  new Promise((resolve, reject) => {
    let locationPermission = false;
    if (Platform.OS === 'android')
      locationPermission = PermissionsAndroid.checkPermission(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );
    if (locationPermission || Platform.OS === 'ios')
      navigator.geolocation.getCurrentPosition(
        location => {
          resolve(location);
        },
        error => reject(error),
        getCurrentPositionOptions
      );
    else reject('Permission denied');
  });

export const getCurrentPlace = () =>
  getCurrentPosition()
    .then(({ coords }) => {
      return Geocoder.geocodePosition({
        lat: coords.latitude,
        lng: coords.longitude
      });
    })
    .then(geocoding => geocoding[0])
    .catch(error => {
      console.log('geocoding getCurrentPlace error', error);
    });

export const getPlaceByName = userInput =>
  Geocoder.geocodeAddress(userInput)
    .then(geocoding => geocoding[0])
    .catch(error => {
      console.log('geocoding getPlaceByName error', error);
    });

export const getPlaceByCoord = coord =>
  Geocoder.geocodePosition(coord)
    .then(geocoding => geocoding[0])
    .catch(error => {
      console.log('geocoding getPlaceByCoord error', error);
    });

export const getPlaceByCoordGoogle = coord =>
  fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?latlng=${coord.lat},${
      coord.lng
    }&key=${GEOCODING_API_KEY}`
  )
    .then(data => data.json())
    .catch(error => console.log(error));
