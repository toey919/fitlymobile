/**
 * @flow
 */

import React, { Component } from 'react';
import _ from 'lodash';
import {
  StatusBar,
  PermissionsAndroid,
  TextInput,
  TouchableHighlight,
  Text,
  View,
  Dimensions,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Slider,
  ActivityIndicator,
  ScrollView,
  Platform,
  Modal,
  Picker,
  TouchableOpacity,
  Switch,
  Image
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';
import PlaceSearchBox from '../common/PlaceSearchBox';
import { storeUserProfile } from '../actions/user.js';
import PickerAndroid from '../common/PickerAndro';
import {
  printError,
  setLoadingState,
  clearError,
  setSearchLocation
} from '../actions/app.js';
import { push, resetTo } from '../actions/navigation.js';
import FMPicker from 'react-native-fm-picker';
import DatePicker from 'react-native-datepicker';
const heightFeet = _.range(1, 9, 1);
const heightInch = _.range(1, 12, 1);
const heightCms = _.range(1, 276, 1);
const weightLbs = _.range(100, 501, 1);
const weightKgs = _.range(50, 201, 1);
const dismissKeyboard = require('dismissKeyboard');
import {
  loginStylesInverse,
  loadingStyle,
  commonStyle,
  FitlyBlue,
  optionStyle
} from '../styles/styles.js';
import {
  getCurrentPlace,
  getPlaceByName
} from '../library/asyncGeolocation.js';
import { createUpdateObj } from '../library/firebaseHelpers.js';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SegmentedControlTab from 'react-native-segmented-control-tab';
const { width } = Dimensions.get('window');
const isAndroid = Platform.OS === 'android';
const FinalPicker = isAndroid ? PickerAndroid : Picker;
class SetupProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: 'Male',
      birthday: null,
      genderModal: false
    };
    this.props.action.setLoadingState(false);
  }

  formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  async _handlePress() {
    if (this.state.gender !== null && this.state.birthday !== null) {
      this.setState({ loading: true });
      this.props.action.clearError();
      let privateDataUpdates = createUpdateObj(
        '/users/' + this.props.uID + '/private',
        {
          gender: this.state.gender,
          birthday: this.state.birthday
        }
      );
      await this.props.FitlyFirebase.database()
        .ref()
        .update({ ...privateDataUpdates });
      await this.props.FitlyFirebase.database()
        .ref('users/' + this.props.uID + '/public')
        .update({ profileComplete: true });
      const userData = (await this.props.FitlyFirebase.database()
        .ref('users/' + this.props.uID)
        .once('value')).val();
      this.props.action.storeUserProfile(userData);
      this.setState({ loading: false });
      this.props.navigation.dispatch(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: 'TabNavigator',
              params: { newUser: true }
            })
          ]
        })
      );
      // save this.state in dB
    } else {
      //TODO: print proper errors
      this.setState({ loading: false });
      this.props.action.printError('Fields cannot be empty');
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPress={() => dismissKeyboard()}
      >
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <ScrollView contentContainerStyle={loginStylesInverse.container}>
            <View style={loginStylesInverse.container}>
              <StatusBar barStyle="default" />
              <Text
                style={[
                  loginStylesInverse.header,
                  { paddingBottom: Dimensions.get('window').height * 0.03 }
                ]}
              >
                YOUR PROFILE
              </Text>
              <Text adjustsFontSizeToFit style={loginStylesInverse.textMid}>
                Your information help us find and suggest friends and events
                suitable for you. {'\n'}This information will never be made
                public.
              </Text>

              <View style={[loginStylesInverse.form, {}]}>
                <Text
                  adjustsFontSizeToFit
                  style={[
                    loginStylesInverse.input,
                    {
                      letterSpacing: 2,
                      lineHeight: 21,
                      fontSize: 12,
                      fontWeight: '400',
                      color: '#ccc',
                      paddingHorizontal: 0
                    }
                  ]}
                >
                  PLEASE SELECT YOUR GENDER
                </Text>

                <SegmentedControlTab
                  tabsContainerStyle={{
                    alignSelf: 'stretch',
                    height: 25,
                    paddingHorizontal: 5
                  }}
                  tabStyle={{ borderColor: FitlyBlue, borderWidth: 0.5 }}
                  activeTabStyle={{ backgroundColor: FitlyBlue }}
                  values={['Male', 'Female']}
                  selectedIndex={this.state.gender === 'Male' ? 0 : 1}
                  onTabPress={index => {
                    this.setState({ gender: index === 0 ? 'Male' : 'Female' });
                  }}
                />
              </View>
              <View style={loginStylesInverse.form}>
                <Text
                  adjustsFontSizeToFit
                  style={[
                    loginStylesInverse.input,
                    {
                      letterSpacing: 2,
                      lineHeight: 21,
                      fontWeight: '400',
                      color: '#ccc',
                      fontSize: 12,
                      paddingHorizontal: 0
                    }
                  ]}
                >
                  I WAS BORN ON...
                </Text>
                <IonIcon
                  name="md-calendar"
                  size={32}
                  color={FitlyBlue}
                  style={{
                    width: 32,
                    height: 32,
                    position: 'absolute',
                    top: 48,
                    left: 15
                  }}
                />

                <DatePicker
                  style={{
                    width: width - 70,
                    borderColor: FitlyBlue,
                    borderWidth: 1,
                    borderRadius: 5
                  }}
                  date={this.state.birthday}
                  mode="date"
                  placeholder="Tap here to select your D.O.B"
                  format="YYYY-MM-DD"
                  minDate="1800-01-01"
                  maxDate={this.formatDate(new Date())}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  iconComponent={() => {
                    return null;
                  }}
                  onDateChange={date => {
                    this.setState({ birthday: date });
                  }}
                  customStyles={{
                    datePickerCon: { backgroundColor: FitlyBlue },
                    datePicker: { backgroundColor: '#fff' },
                    dateInput: {
                      marginLeft: 36,
                      borderWidth: 0
                    },
                    placeholderText: { color: FitlyBlue },
                    dateText: {
                      color: FitlyBlue
                    },
                    btnTextCancel: {
                      color: '#bbb',
                      fontWeight: 'bold'
                    },
                    btnTextConfirm: {
                      color: '#FFF',
                      fontWeight: 'bold'
                    }
                  }}
                />
              </View>
              {this.props.error ? (
                <Text style={commonStyle.error}> {this.props.error} </Text>
              ) : (
                <Text style={commonStyle.hidden}> </Text>
              )}
            </View>
          </ScrollView>
          <TouchableHighlight
            style={loginStylesInverse.swipeBtn}
            onPress={() => this._handlePress()}
          >
            {this.state.loading ? (
              <View style={{ height: 40, width: 40, alignSelf: 'center' }}>
                <ActivityIndicator
                  size="large"
                  color="#fff"
                  animating
                  style={{ height: 40 }}
                />
              </View>
            ) : (
              <Text style={loginStylesInverse.btnText}>FINISH</Text>
            )}
          </TouchableHighlight>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

class SetupStats extends Component {
  constructor(props) {
    super(props);
    const { height, weight, unit, inches } = this.props.user.private;
    this.state = {
      height,
      weight,
      unit: unit ? unit : 'STANDARD',
      unitVal: unit === 'METRICS' ? true : false,
      inches,
      loading: false,
      showWeightModal: false,
      showHeightModal: false
    };
    this.nextSteps = this.props.navigation.state.params.nextSteps;
  }
  async _handlePress(skip) {
    if (skip) {
      if (this.nextSteps) {
        let privateDataUpdates = createUpdateObj(
          '/users/' + this.props.uID + '/private',
          {
            skippedStats: true
          }
        );
        await this.props.FitlyFirebase.database()
          .ref()
          .update({ ...privateDataUpdates });
        this.nextSteps();
      } else this.props.navigation.goBack();
    }
    if (this.state.height && this.state.weight && !skip) {
      this.setState({ loading: true });
      this.props.action.clearError();
      console.log();
      let privateDataUpdates = createUpdateObj(
        '/users/' + this.props.uID + '/private',
        {
          height: this.state.height,
          ...(this.state.unit === 'METRICS'
            ? { inches: this.state.inches }
            : {}),
          weight: this.state.weight,
          unit: this.state.unit
        }
      );
      await this.props.FitlyFirebase.database()
        .ref()
        .update({ ...privateDataUpdates });
      const userData = (await this.props.FitlyFirebase.database()
        .ref('users/' + this.props.uID)
        .once('value')).val();
      this.props.action.storeUserProfile(userData);
      this.setState({ loading: false });
      if (this.nextSteps) {
        this.nextSteps();
      } else this.props.navigation.goBack();
      // save this.state in dB
    } else {
      //TODO: print proper errors
      this.setState({ loading: false });
      this.props.action.printError('Fields cannot be empty');
    }
  }
  focusNextField = nextField => {
    this.refs[nextField].focus();
  };

  render() {
    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPress={() => dismissKeyboard()}
      >
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <View
            style={{
              height: 80,
              width,
              backgroundColor: FitlyBlue,
              flexDirection: 'row',
              alignItems: 'center',
              paddingTop: 12
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                paddingTop: 8
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <IonIcon
                  name={'ios-close'}
                  color={'white'}
                  size={48}
                  style={{ left: -2 }}
                />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 4, alignItems: 'center' }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 20,
                  fontWeight: 'bold'
                }}
              >
                {this.props.navigation.state.params.edit
                  ? `Settings`
                  : `Find A Partner`}
              </Text>
            </View>
            <View style={{ flex: 1 }} />
          </View>
          <View style={loginStylesInverse.container}>
            <StatusBar barStyle="light-content" />
            <Text
              style={[
                loginStylesInverse.header,
                { paddingBottom: Dimensions.get('window').height * 0.03 }
              ]}
            >
              YOUR STATS
            </Text>
            <Text adjustsFontSizeToFit style={loginStylesInverse.textMid}>
              Your dimensions help us improve your experience. This information
              will never be made public.
            </Text>
            <View
              style={[
                loginStylesInverse.form,
                {
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: width - 70
                }
              ]}
            >
              <Text
                adjustsFontSizeToFit
                style={{ color: '#ccc', letterSpacing: 2, lineHeight: 21 }}
              >
                UNIT OF MEASURE
              </Text>
              <Switch
                value={this.state.unitVal}
                onValueChange={() => {
                  this.setState({ unitVal: !this.state.unitVal }, () => {
                    this.setState({
                      unit: this.state.unitVal ? 'METRICS' : 'STANDARD'
                    });
                  });
                }}
                onTintColor={'#eee'}
              />
              <Text
                style={{ marginLeft: 8, fontSize: 12, textAlign: 'center' }}
              >
                {this.state.unitVal ? 'METRICS\n(U.S.)' : 'STANDARD\n(Europe)'}
              </Text>
            </View>
            <View style={loginStylesInverse.form}>
              <Text
                adjustsFontSizeToFit
                style={{ color: '#ccc', letterSpacing: 2, lineHeight: 21 }}
              >
                HEIGHT
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showHeightModal: true });
                }}
                style={{ width: width - 70, marginTop: 15 }}
              >
                <Text style={{ letterSpacing: 2, fontSize: 18 }}>
                  {this.state.height || '__'}
                  {this.state.unitVal
                    ? ` ft ${this.state.inches || '__'} in`
                    : ' cms'}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={loginStylesInverse.form}>
              <Text
                adjustsFontSizeToFit
                style={{ color: '#ccc', letterSpacing: 2, lineHeight: 21 }}
              >
                WEIGHT
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showWeightModal: true });
                }}
                style={{ width: width - 70, marginTop: 15 }}
              >
                <Text style={{ letterSpacing: 2, fontSize: 18 }}>
                  {this.state.weight || '__'}
                  {this.state.unitVal ? ' lbs' : ' kgs'}
                </Text>
              </TouchableOpacity>
            </View>
            {this.props.error ? (
              <Text style={commonStyle.error}> {this.props.error} </Text>
            ) : (
              <Text style={commonStyle.hidden}> </Text>
            )}
          </View>
          <View
            style={{ flexDirection: 'row', bottom: 0, position: 'absolute' }}
          >
            {!this.props.navigation.state.params.edit && (
              <TouchableHighlight
                style={{
                  height: 50,
                  width: width / 2,
                  backgroundColor: FitlyBlue,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                onPress={() => this._handlePress(true)}
              >
                <Text style={loginStylesInverse.btnText}>SKIP</Text>
              </TouchableHighlight>
            )}
            <TouchableHighlight
              style={{
                height: 50,
                width: this.props.navigation.state.params.edit
                  ? width
                  : width / 2,
                backgroundColor: FitlyBlue,
                justifyContent: 'center',
                alignItems: 'center'
              }}
              onPress={() => this._handlePress(false)}
            >
              {this.state.loading ? (
                <View style={{ height: 40, width: 40, alignSelf: 'center' }}>
                  <ActivityIndicator
                    size="large"
                    color="#fff"
                    animating
                    style={{ height: 40 }}
                  />
                </View>
              ) : (
                <Text style={loginStylesInverse.btnText}>SAVE</Text>
              )}
            </TouchableHighlight>
          </View>
          <Modal
            visible={this.state.showHeightModal || this.state.showWeightModal}
            transparent
            animationType={'fade'}
            onRequestClose={() => {
              this.setState({
                showHeightModal: false,
                showWeightModal: false
              });
            }}
          >
            <View
              style={{
                width,
                height: Dimensions.get('window').height,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'flex-end'
              }}
              onTouchStart={() => {
                this.setState({
                  showHeightModal: false,
                  showWeightModal: false
                });
              }}
            >
              <View
                onTouchStart={event => event.stopPropagation()}
                style={{ width, height: 300, backgroundColor: '#fff' }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    width,
                    height: 50,
                    backgroundColor: FitlyBlue
                  }}
                >
                  <View style={{ flex: 1 }} />
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 2
                    }}
                  >
                    <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                      Choose {this.state.showHeightModal ? 'Height' : 'Weight'}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1
                    }}
                    onPress={() => {
                      this.setState({
                        showHeightModal: false,
                        showWeightModal: false
                      });
                    }}
                  >
                    <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                      Done
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 0, flexDirection: 'row', height: 20 }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      width:
                        this.state.showHeightModal && this.state.unitVal
                          ? width / 2
                          : width
                    }}
                  >
                    {this.state.showHeightModal
                      ? this.state.unitVal ? 'Feet' : 'Centimeters'
                      : this.state.unitVal ? 'Pounds' : 'Kilograms'}
                  </Text>
                  {this.state.showHeightModal &&
                    this.state.unitVal && (
                      <Text style={{ width: width / 2, textAlign: 'center' }}>
                        Inches
                      </Text>
                    )}
                </View>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    flexDirection: 'row'
                  }}
                >
                  <FinalPicker
                    style={{
                      width:
                        this.state.showHeightModal && this.state.unitVal
                          ? width / 2
                          : width,
                      height: 300,
                      marginTop: 50
                    }}
                    selectedValue={
                      this.state.showHeightModal
                        ? this.state.height !== '__' ? this.state.height : 0
                        : this.state.weight !== '__' ? this.state.weight : 0
                    }
                    itemStyle={{
                      color: 'black',
                      fontSize: 26,
                      width:
                        this.state.showHeightModal && this.state.unitVal
                          ? width / 2
                          : width,
                      textAlign: 'center'
                    }}
                    onValueChange={index => {
                      this.setState(
                        this.state.showHeightModal
                          ? { height: index }
                          : { weight: index }
                      );
                    }}
                  >
                    {this.state.showHeightModal && this.state.unitVal
                      ? heightFeet.map((value, i) => (
                          <Picker.Item
                            label={value + ''}
                            value={value + ''}
                            key={i}
                          />
                        ))
                      : this.state.showHeightModal && !this.state.unitVal
                        ? heightCms.map((value, i) => (
                            <Picker.Item
                              label={value + ''}
                              value={value + ''}
                              key={i}
                            />
                          ))
                        : this.state.showWeightModal && this.state.unitVal
                          ? weightLbs.map((value, i) => {
                              return (
                                <Picker.Item
                                  label={value + ''}
                                  value={value + ''}
                                  key={i}
                                />
                              );
                            })
                          : weightKgs.map((value, i) => {
                              return (
                                <Picker.Item
                                  label={value + ''}
                                  value={value + ''}
                                  key={i}
                                />
                              );
                            })}
                  </FinalPicker>
                  {this.state.showHeightModal &&
                    this.state.unitVal && (
                      <FinalPicker
                        style={{
                          width: width / 2,
                          height: 300,
                          marginTop: 50
                        }}
                        selectedValue={
                          this.state.inches !== '__' ? this.state.inches : 0
                        }
                        itemStyle={{ color: 'black', fontSize: 26 }}
                        onValueChange={index => {
                          this.setState({ inches: index });
                        }}
                      >
                        {heightInch.map((value, i) => (
                          <Picker.Item
                            label={value + ''}
                            value={value + ''}
                            key={i}
                          />
                        ))}
                      </FinalPicker>
                    )}
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
class SetupLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      location: null,
      locationInput: null,
      showModal: false
    };
  }
  componentDidMount() {
    this._getLocation(null);
  }
  _handlePress() {
    const { FitlyFirebase } = this.props;
    //TODO: if picture not created yet, direct to picture upload scene
    this.props.action.clearError();
    if (this.state.location) {
      (async () => {
        try {
          this.setState({ showModal: false });
          this.props.navigation.navigate('InterestsView', {
            facebook:
              this.props.navigation.state.params &&
              this.props.navigation.state.params.facebook
          });
          let publicDataUpdates = createUpdateObj(
            '/users/' + this.props.uID + '/public',
            {
              userLocation: this.state.location,
              userCurrentLocation: this.state.location
            }
          );
          await this.props.FitlyFirebase.database()
            .ref()
            .update({ ...publicDataUpdates });
          const userData = (await FitlyFirebase.database()
            .ref('users/' + this.props.uID)
            .once('value')).val();
          this.props.action.storeUserProfile(userData);
          const coordinate = {
            coordinate: userData.public.userCurrentLocation.coordinate
          };
          this.props.action.setSearchLocation(coordinate);
        } catch (error) {
          this.props.action.printError(error.message);
        }
      })();
    } else {
      //TODO: show proper error
      this.props.action.printError(
        'Cannot set the location. Please grant the location permission first.'
      );
    }
  }

  async _getLocation(input) {
    // this.setState({ showModal: true });
    let getLocationFunc = input
      ? getPlaceByName.bind(null, input)
      : getCurrentPlace;
    let granted;
    if (Platform.OS === 'android') {
      granted = await PermissionsAndroid.requestPermission(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          message: 'Fitly needs to access your location to serve you better',
          title: 'Fitly Location Access'
        }
      );
    }
    this.props.action.clearError();
    (async () => {
      if (granted || Platform.OS === 'ios') {
        try {
          this.setState({ loading: true });
          const place = await getLocationFunc();
          this.setState({
            loading: false,
            location: {
              place: `${place.locality}, ${place.adminArea}`,
              coordinate: {
                lat: place.position.lat,
                lon: place.position.lng
              },
              zip: place.postalCode
            }
          });
        } catch (error) {
          this.props.action.printError(error);
          console.log('geolocation error', error);
        }
      } else {
        this.props.action.printError('Sorry, I cannot access your location.');
      }
    })();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <Modal
          visible={!this.state.loading && this.state.showModal}
          animationType={'slide'}
          transparent
          onRequestClose={() => this.setState({ showModal: false })}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: '#fff',
              overflow: 'hidden'
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                marginTop: 70,
                paddingHorizontal: 16,
                marginBottom: 20
              }}
            >
              <Icon
                name={'gps-fixed'}
                size={32}
                color={FitlyBlue}
                style={{ backgroundColor: 'transparent', marginRight: 20 }}
              />
              <Text
                style={[
                  {
                    fontSize: 14,
                    textAlign: 'left',
                    fontWeight: '200',
                    letterSpacing: 2
                  }
                ]}
              >
                {this.state.location && this.state.location.place
                  ? `Current Selection\n${this.state.location.place}`
                  : 'Getting your location...'}
              </Text>
            </View>
            <PlaceSearchBox
              placeholder={'Search for your city here'}
              onPress={(_data, place) => {
                this.setState({
                  location: {
                    place: place.formatted_address,
                    coordinate: {
                      lat: place.geometry.location.lat,
                      lon: place.geometry.location.lng
                    }
                  }
                });
              }}
            />

            <TouchableHighlight
              style={loginStylesInverse.swipeBtn}
              onPress={() => this._handlePress()}
            >
              <Text style={loginStylesInverse.btnText}>NEXT ></Text>
            </TouchableHighlight>
          </View>
        </Modal>
        <ScrollView contentContainerStyle={loginStylesInverse.container}>
          <View style={loginStylesInverse.container}>
            <StatusBar barStyle="default" />
            <Text style={[loginStylesInverse.header, { fontSize: 32 }]}>
              CURRENT LOCATION
            </Text>
            <View
              style={{
                width: 350,
                height: 350,
                borderRadius: 175,
                borderWidth: 2,
                borderColor: 'rgba(251,251,251,1)',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image
                source={require('../../img/map.png')}
                style={{
                  width: 350,
                  height: 300,
                  borderRadius: 140,
                  opacity: 0.1
                }}
              />
              <View
                style={{
                  width: 250,
                  height: 250,
                  borderRadius: 125,
                  borderWidth: 4,
                  borderColor: 'rgba(251,251,251,1)',
                  position: 'absolute',
                  top: 50,
                  left: 50
                }}
              />
            </View>
            <TouchableOpacity
              style={[
                loginStylesInverse.FBbtn,
                {
                  marginTop: 50,
                  borderRadius: 40,
                  flexDirection: 'row',
                  alignItems: 'center',
                  width: 280
                }
              ]}
              onPress={() =>
                !this.state.location
                  ? this._getLocation(null)
                  : this._handlePress()
              }
            >
              {!this.state.location && (
                <Icon
                  name={'gps-fixed'}
                  size={32}
                  color={'#fff'}
                  style={{ backgroundColor: 'transparent' }}
                />
              )}
              <Text style={[loginStylesInverse.btnText, { marginLeft: 8 }]}>
                {!this.state.location ? 'ALLOW LOCATION ACCESS' : 'CONTINUE'}
              </Text>
            </TouchableOpacity>
            {this.state.location && (
              <Text
                style={{
                  textAlign: 'center',
                  color: '#bbb',
                  fontWeight: 'bold',
                  lineHeight: 21,
                  marginTop: 5
                }}
              >
                I M NOT IN{'\n'}
                <Text
                  onPress={() => {
                    this.setState({ showModal: true });
                  }}
                  style={{ color: FitlyBlue, fontSize: 16 }}
                >
                  {this.state.location.place.toUpperCase()}
                </Text>
              </Text>
            )}
            {this.props.error ? (
              <Text style={commonStyle.error}> {this.props.error} </Text>
            ) : (
              <Text style={commonStyle.hidden}> </Text>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = function(state) {
  return {
    uID: state.auth.uID,
    user: state.user.user,
    error: state.app.error,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        printError,
        clearError,
        storeUserProfile,
        setLoadingState,
        setSearchLocation
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ push, resetTo }, dispatch)
  };
};

export const SetupProfileView = connect(mapStateToProps, mapDispatchToProps)(
  SetupProfile
);
export const SetupStatsView = connect(mapStateToProps, mapDispatchToProps)(
  SetupStats
);
export const SetupLocationView = connect(mapStateToProps, mapDispatchToProps)(
  SetupLocation
);
