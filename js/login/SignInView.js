/**
 * @flow
 */

import React, { Component } from 'react';
import Btn from '../common/Btn';
import {
  StatusBar,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  Text,
  View,
  ActivityIndicator,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
  Modal
} from 'react-native';
import {
  loginStyles,
  loadingStyle,
  commonStyle,
  FitlyBlue
} from '../styles/styles.js';
import FBloginBtn from '../common/FBloginBtn';
import GoogleloginBtn from '../common/GoogleloginBtn';
// import LinkedInloginBtn from '../common/LinkeInloginBtn';
import {
  setFirebaseUID,
  setSignInMethod,
  printAuthError,
  clearAuthError
} from '../actions/auth.js';
import { storeUserProfile } from '../actions/user.js';
import { setBlocks } from '../actions/blocks.js';
import { setLoadingState, setSearchLocation } from '../actions/app.js';
import { resetTo } from '../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateCurrentLocationInDB } from '../library/firebaseHelpers.js';
import Icon from 'react-native-vector-icons/Ionicons';
import FIcon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { Terms, PrivacyText } from '../constants/privacyTermsNCookies.js';
const isAndroid = Platform.OS === 'android';

class SignInView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '', //q948066@mvrht.net
      password: '', //qweqwe
      loading: false,
      agreement: false
    };
  }

  componentWillMount() {
    this.props.action.clearAuthError();
    this.props.action.setLoadingState(false);
  }
  _terms() {
    return <Text>{Terms}</Text>;
  }

  _privacy() {
    return <Text>{PrivacyText}</Text>;
  }
  _renderAgreement(view) {
    return (
      <View style={{ flex: 1, margin: 20, marginTop: 80, marginBottom: 80 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#fff',
            padding: 10,
            borderRadius: 5,
            shadowColor: 'black',
            shadowOpacity: 0.6,
            elevation: 2,
            shadowOffset: { width: 0, height: 0 },
            shadowRadius: 2,
            marginBottom: 20
          }}
        >
          <ScrollView>
            {view === 'privacy' ? this._privacy() : this._terms()}
          </ScrollView>
        </View>
        <Btn
          style={{ backgroundColor: 'white', alignSelf: 'center' }}
          textStyle={{ color: '#1D2F7B' }}
          text={'Done'}
          onPress={() => this.setState({ agreement: false })}
        />
      </View>
    );
  }
  _handleEmailSignin() {
    this.setState({ loading: true });
    //TODO: validate the email, password and names before sending it out
    const { navigation, action } = this.props;
    const { FitlyFirebase } = this.props.navigation.state.params;
    (async () => {
      try {
        action.clearAuthError();
        action.setLoadingState(true);
        const authData = await FitlyFirebase.auth().signInWithEmailAndPassword(
          this.state.email,
          this.state.password
        );
        action.setSignInMethod('Email');
        action.setFirebaseUID(authData.uid);
        await updateCurrentLocationInDB(authData.uid);
        //TODO abstract away check for profile completion, write a isProfileComplete function
        const userRef = FitlyFirebase.database().ref('users/' + authData.uid);
        const firebaseUserData = (await userRef.once('value')).val();
        const blocks = (await FitlyFirebase.database()
          .ref('blocks/' + authData.uid)
          .once('value')).val();
        if (authData.emailVerified === false) {
          navigation.navigate('VerifyEmailView', {
            authData: authData,
            email: this.state.email,
            password: this.state.password
          });
          action.setLoadingState(false);
        } else if (firebaseUserData.public.profileComplete === false) {
          navigation.navigate('SetupProfileView');
          action.setLoadingState(false);
        } else {
          action.storeUserProfile(firebaseUserData);
          if (firebaseUserData.public.userCurrentLocation) {
            const coordinate = {
              coordinate: firebaseUserData.public.userCurrentLocation.coordinate
            };
            action.setSearchLocation(coordinate);
          }
          action.setBlocks(blocks);
          navigation.dispatch(
            NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'TabNavigator',
                  params: { newUser: false }
                })
              ]
            })
          );
          action.setLoadingState(false);
          this.setState({ loading: false });
        }
      } catch (error) {
        action.setLoadingState(false);
        action.printAuthError(error.message);
        this.setState({ loading: false });
      }
    })();
  }

  focusNextField = nextField => {
    this.props.action.clearAuthError();
    this.refs[nextField].focus();
  };
  _renderLoginForm() {
    return (
      <View
        style={{
          marginTop: 50,
          width: Dimensions.get('window').width,
          alignItems: 'center'
        }}
      >
        <View style={loginStyles.form}>
          <TextInput
            underlineColorAndroid={'transparent'}
            returnKeyType="next"
            maxLength={30}
            clearButtonMode="always"
            ref="1"
            onSubmitEditing={() => this.focusNextField('2')}
            style={loginStyles.input}
            onChangeText={text => this.setState({ email: text })}
            value={this.state.email}
            keyboardType="email-address"
            placeholder="Email"
            placeholderTextColor={'#aaa'}
            autoCapitalize="none"
            autoCorrect={false}
          />
          {this.state.email.length > 0 ? (
            <Text
              style={{
                color: '#aaa',
                fontSize: 10,
                backgroundColor: 'transparent',
                letterSpacing: 2,
                position: 'absolute',
                top: 2,
                left: 22
              }}
            >
              EMAIL
            </Text>
          ) : (
            false
          )}
        </View>

        <View style={loginStyles.form}>
          <TextInput
            underlineColorAndroid={'transparent'}
            returnKeyType="done"
            maxLength={30}
            clearButtonMode="always"
            ref="2"
            style={loginStyles.input}
            onSubmitEditing={() => this._handleEmailSignin()}
            onChangeText={text => this.setState({ password: text })}
            value={this.state.password}
            keyboardType="default"
            secureTextEntry={true}
            placeholder="Password"
            placeholderTextColor={'#aaa'}
          />
          {this.state.password.length > 0 ? (
            <Text
              style={{
                color: '#aaa',
                fontSize: 10,
                backgroundColor: 'transparent',
                letterSpacing: 2,
                position: 'absolute',
                top: 2,
                left: 22
              }}
            >
              PASSWORD
            </Text>
          ) : (
            false
          )}
        </View>
        {/* TODO: need to implement the forget password and reset password feature */}
        <TouchableHighlight onPress={() => this._forgotPassword()}>
          <Text style={[loginStyles.textSmall, { marginTop: 0 }]}>
            Forgot your password?
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
  _forgotPassword() {
    const { email } = this.state.email;
    let auth = this.props.FitlyFirebase.auth();
    if (!email) {
      this.setState({
        error: 'there is no email in the email field'
      });
      setTimeout(() => {
        this.setState({ error: false });
      }, 2000);
      return;
    }

    auth.sendPasswordResetEmail(email).then(
      () => {
        this.setState({
          success: 'Email sent to reset password'
        });
      },
      error => {
        console.log(error);
      }
    );
  }

  render() {
    const { FitlyFirebase } = this.props.navigation.state.params;
    //console.log('FIREBASSS', FitlyFirebase)
    if (this.props.loading === true) {
      return (
        <View
          style={[
            {
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            },
            { backgroundColor: FitlyBlue }
          ]}
        >
          <Text style={loginStyles.logo}>Fitly</Text>
          <ActivityIndicator
            animating={true}
            style={{ height: 80 }}
            size="large"
            color={'#fff'}
          />
        </View>
      );
    }
    return (
      <View style={{ flex: 1, backgroundColor: '#1D2F7B' }}>
        {this.state.agreement ? (
          <Modal
            animationType={'fade'}
            transparent={true}
            visible={!!this.state.agreement}
            onRequestClose={() =>
              this.setState({
                agreement: false
              })
            }
          >
            {this._renderAgreement(this.state.agreement)}
          </Modal>
        ) : (
          <View style={{ flex: 1 }}>
            <ScrollView
              keyboardDismissMode={isAndroid ? 'none' : 'on-drag'}
              contentContainerStyle={loginStyles.container}
            >
              <KeyboardAvoidingView
                behavior="position"
                style={loginStyles.container}
              >
                <StatusBar barStyle="light-content" />
                <Text style={loginStyles.logo}>Fitly</Text>
                <TouchableHighlight
                  onPress={() =>
                    this.props.navigation.dispatch(
                      NavigationActions.reset({
                        index: 1,
                        actions: [
                          NavigationActions.navigate({
                            routeName: 'WelcomeView'
                          }),
                          NavigationActions.navigate({
                            routeName: 'SignUpView',
                            params: { FitlyFirebase: this.props.FitlyFirebase }
                          })
                        ]
                      })
                    )
                  }
                  style={{ marginBottom: 10 }}
                >
                  <Text style={loginStyles.boldSmall}>
                    {"Don't have an account?\n Register now"}
                  </Text>
                </TouchableHighlight>
                {!this.state.emailSignIn ? (
                  <View style={{ alignItems: 'center' }}>
                    <View
                      style={{
                        marginTop: 10
                      }}
                    >
                      <GoogleloginBtn
                        navigation={this.props.navigation}
                        FitlyFirebase={FitlyFirebase}
                        style={{ marginBottom: 20 }}
                      />
                      <FBloginBtn
                        navigation={this.props.navigation}
                        FitlyFirebase={FitlyFirebase}
                        style={{ marginBottom: 20 }}
                      />

                      {
                        //   <LinkedInloginBtn
                        //   navigation={this.props.navigation}
                        //   FitlyFirebase={FitlyFirebase}
                        // />
                      }
                    </View>
                    <TouchableHighlight
                      style={[
                        loginStyles.FBbtn,
                        {
                          borderRadius: 30,
                          alignItems: 'center',
                          justifyContent: 'center'
                        }
                      ]}
                      onPress={() => {
                        this.props.action.clearAuthError();
                        this.setState({ emailSignIn: true });
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          alignItems: 'center',
                          backgroundColor: 'transparent'
                        }}
                      >
                        <FIcon name="envelope-o" size={32} color={FitlyBlue} />
                        <Text
                          style={[
                            loginStyles.btnText,
                            {
                              color: FitlyBlue,
                              width: 190
                            }
                          ]}
                        >
                          Continue with Email
                        </Text>
                      </View>
                    </TouchableHighlight>
                  </View>
                ) : (
                  this._renderLoginForm()
                )}
                {this.props.error || this.state.error ? (
                  <Text
                    style={[
                      commonStyle.error,
                      {
                        backgroundColor: '#f00',
                        color: '#fff',
                        width: Dimensions.get('window').width,
                        padding: 10,
                        fontWeight: 'bold',
                        lineHeight: 21,
                        marginVertical: 10
                      }
                    ]}
                  >
                    {this.props.error || this.state.error}
                  </Text>
                ) : (
                  <Text style={commonStyle.hidden}> </Text>
                )}
                <Text
                  style={[
                    loginStyles.disclamerText,
                    { marginTop: 20, textAlign: 'center', alignSelf: 'center' }
                  ]}
                >
                  By tapping "Continue", you agree to our{'\n'}
                  <Text
                    style={{
                      color: 'white',
                      backgroundColor: 'transparent',
                      textDecorationLine: 'underline',
                      textDecorationColor: '#fff'
                    }}
                    onPress={() => {
                      this.setState({ agreement: 'term' });
                    }}
                  >
                    Terms Of Service
                  </Text>{' '}
                  ,{' '}
                  <Text
                    style={{
                      color: 'white',
                      backgroundColor: 'transparent',
                      textDecorationLine: 'underline',
                      textDecorationColor: '#fff'
                    }}
                    onPress={() => {
                      this.setState({ agreement: 'privacy' });
                    }}
                  >
                    Privacy Policy
                  </Text>
                  {'\n'}
                  &{' '}
                  <Text
                    style={{
                      color: 'white',
                      backgroundColor: 'transparent'
                    }}
                  >
                    Cookie Policy
                  </Text>
                </Text>
              </KeyboardAvoidingView>
              <Text style={{ height: 100 }} />
            </ScrollView>
            {this.state.emailSignIn ? (
              <TouchableHighlight
                style={[
                  loginStyles.swipeBtn,
                  {
                    position: 'absolute',
                    zIndex: 99,
                    bottom: 0,
                    left: 0
                  }
                ]}
                onPress={() => this._handleEmailSignin()}
              >
                <Text style={loginStyles.btnText}>LOG IN</Text>
              </TouchableHighlight>
            ) : (
              false
            )}
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    loading: state.app.loading,
    error: state.auth.errorMsg,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        setFirebaseUID,
        setSignInMethod,
        printAuthError,
        clearAuthError,
        setLoadingState,
        storeUserProfile,
        setSearchLocation,
        setBlocks
      },
      dispatch
    ),
    Exnavigation: bindActionCreators({ resetTo }, dispatch)
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SignInView);
