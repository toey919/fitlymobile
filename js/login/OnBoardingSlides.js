import React from 'react';
import { NavigationActions } from 'react-navigation';
import {
  Platform,
  View,
  TouchableHighlight,
  Dimensions,
  Animated,
  ScrollView,
  Text
} from 'react-native';
import { FitlyBlue } from '../styles/styles';
let { width, height } = Dimensions.get('window');
const Imgs = [
  require('../../img/active.jpeg'),
  require('../../img/fit.jpeg'),
  require('../../img/join-a-league.jpg'),
  require('../../img/pickup-sports.jpg'),
  require('../../img/new-friends.jpeg')
];
const desc = [
  'Get Active!',
  'Find Classes',
  'Join a League!',
  'Pickup Sports',
  'Meet new Friends!'
];
const TIMING = 500;
class OnBoardingSlides extends React.Component {
  state = {
    currentPos: 0,
    anim1: true,
    anim2: false,
    anim3: false,
    anim4: false,
    anim5: false,
    opacity1: new Animated.Value(1),
    opacity2: new Animated.Value(0),
    opacity3: new Animated.Value(0),
    opacity4: new Animated.Value(0),
    opacity5: new Animated.Value(0)
  };
  componentDidUpdate(prevState) {
    switch (this.state.currentPos) {
      case 0: {
        if (!this.state.anim1) {
          Animated.parallel([
            Animated.timing(this.state.opacity1, {
              toValue: 1,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity2, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity3, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity4, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity5, {
              toValue: 0,
              duration: TIMING
            })
          ]).start(() => {
            this.setState({
              anim1: true,
              anim2: false,
              anim3: false,
              anim4: false,
              anim5: false
            });
          });
        }
        break;
      }
      case 1: {
        if (!this.state.anim2) {
          Animated.parallel([
            Animated.timing(this.state.opacity1, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity2, {
              toValue: 1,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity3, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity4, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity5, {
              toValue: 0,
              duration: TIMING
            })
          ]).start(() => {
            this.setState({
              anim1: false,
              anim2: true,
              anim3: false,
              anim4: false,
              anim5: false
            });
          });
        }
        break;
      }
      case 2: {
        if (!this.state.anim3) {
          Animated.parallel([
            Animated.timing(this.state.opacity1, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity2, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity3, {
              toValue: 1,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity4, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity5, {
              toValue: 0,
              duration: TIMING
            })
          ]).start(() => {
            this.setState({
              anim1: false,
              anim2: false,
              anim3: true,
              anim4: false,
              anim5: false
            });
          });
        }
        break;
      }
      case 3: {
        if (!this.state.anim4) {
          this.props.revertColor();
          Animated.parallel([
            Animated.timing(this.state.opacity1, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity2, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity3, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity4, {
              toValue: 1,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity5, {
              toValue: 0,
              duration: TIMING
            })
          ]).start(() => {
            this.setState({
              anim1: false,
              anim2: false,
              anim3: false,
              anim4: true,
              anim5: false
            });
          });
        }
        break;
      }
      case 4: {
        if (!this.state.anim5) {
          this.props.changeColor();
          Animated.parallel([
            Animated.timing(this.state.opacity1, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity2, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity3, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity4, {
              toValue: 0,
              duration: TIMING
            }),
            Animated.timing(this.state.opacity5, {
              toValue: 1,
              duration: TIMING
            })
          ]).start(() => {
            this.setState({
              anim1: false,
              anim2: false,
              anim3: false,
              anim4: false,
              anim5: true
            });
          });
        }
        break;
      }
    }
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: 'transparent',
          height: height,
          width: width,
          alignSelf: 'center',
          overflow: 'visible',
          alignItems: 'center',
          paddingVertical: 28
        }}
      >
        <Animated.Image
          style={{
            width,
            height,
            position: 'absolute',
            top: 0,
            left: 0,
            opacity: this.state.opacity1
          }}
          source={Imgs[0]}
          resizeMode={'cover'}
        />
        <Animated.Image
          style={{
            width,
            height,
            position: 'absolute',
            top: 0,
            left: 0,
            opacity: this.state.opacity2
          }}
          source={Imgs[1]}
          resizeMode={'cover'}
        />
        <Animated.Image
          style={{
            width,
            height,
            position: 'absolute',
            top: 0,
            left: 0,
            opacity: this.state.opacity3
          }}
          source={Imgs[2]}
          resizeMode={'cover'}
        />
        <Animated.Image
          style={{
            width,
            height,
            position: 'absolute',
            top: 0,
            left: 0,
            opacity: this.state.opacity4
          }}
          source={Imgs[3]}
          resizeMode={'cover'}
        />
        <Animated.Image
          style={{
            width,
            height,
            position: 'absolute',
            top: 0,
            left: 0,
            opacity: this.state.opacity5
          }}
          source={Imgs[4]}
          resizeMode={'cover'}
        />
        <ScrollView
          bounces={false}
          ref={'miniPhone'}
          horizontal
          pagingEnabled
          decelerationRate={'normal'}
          onScroll={({ nativeEvent }) => {
            this.setState({
              currentPos: Math.round(nativeEvent.contentOffset.x / width)
            });
          }}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
        >
          {desc.map((currDesc, index) => (
            <View
              style={{
                flex: 0,
                height,
                width,
                alignItems: 'center',
                justifyContent: 'center'
              }}
              key={index}
            >
              <Text
                style={{
                  color: '#eee',
                  fontSize: 28,
                  fontWeight: 'bold',
                  letterSpacing: 2,
                  top: -height * 0.3
                }}
              >
                {currDesc}
              </Text>
            </View>
          ))}
        </ScrollView>
        <View
          style={{
            height: 20,
            width: 80,
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            position: 'absolute',
            bottom: 70,
            left: width / 2 - 40
          }}
        >
          {desc.map((item, index) => (
            <View
              style={{
                height: 6,
                width: 6,
                borderRadius: 5,
                backgroundColor:
                  index === this.state.currentPos
                    ? 'rgba(255,255,255,1)'
                    : 'rgba(0,0,0,0.3)',
                transform: [
                  { scale: index === this.state.currentPos ? 1.7 : 1 }
                ]
              }}
              key={index}
            />
          ))}
        </View>
      </View>
    );
  }
}
export default OnBoardingSlides;
