/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StatusBar,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Modal,
  Slider,
  Platform,
  Dimensions
} from 'react-native';
import FIcon from 'react-native-vector-icons/FontAwesome';
import {
  loginStyles,
  loadingStyle,
  commonStyle,
  FitlyBlue
} from '../styles/styles.js';
import FBloginBtn from '../common/FBloginBtn.js';
import GoogleloginBtn from '../common/GoogleloginBtn.js';
// import LinkedInloginBtn from '../common/LinkeInloginBtn';
import {
  setFirebaseUID,
  setSignUpMethod,
  printAuthError,
  clearAuthError
} from '../actions/auth.js';
import { setLoadingState } from '../actions/app.js';
import { resetTo } from '../actions/navigation.js';
import { PrivacyText, Terms } from '../constants/privacyTermsNCookies';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Firebase from 'firebase';
import Btn from '../common/Btn';
import Icon from 'react-native-vector-icons/Ionicons';

import { NavigationActions } from 'react-navigation';
const isAndroid = Platform.OS === 'android';
const { width, height } = Dimensions.get('window');
class SignUpView extends Component {
  constructor(props) {
    super(props);
    //refactor to redux later, must validate input
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordConfirm: '',
      agreement: false,
      loadingSubmit: false
    };
  }

  componentWillMount() {
    this.props.action.clearAuthError();
  }
  _handleEmailSignup() {
    //TODO error reporting for login error
    //TODO validate the email, password and names before sending it out
    const { navigation, action } = this.props;
    const { FitlyFirebase } = navigation.state.params;

    (async () => {
      try {
        action.clearAuthError();
        await FitlyFirebase.auth().signOut();
        const authData = await FitlyFirebase.auth().createUserWithEmailAndPassword(
          this.state.email,
          this.state.password
        );
        await authData.updateProfile({
          displayName: this.state.firstName + ' ' + this.state.lastName
        });
        action.setSignUpMethod('Email');
        action.setFirebaseUID(authData.uid);
        //TODO: send verification email
        authData.sendEmailVerification();
        const userRef = FitlyFirebase.database().ref('users/' + authData.uid);
        const serverVal = await Firebase.database.ServerValue;
        await userRef.set({
          public: {
            account: 'default',
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            provider: 'Firebase',
            followerCount: 1,
            followingCount: 0,
            isActive: true,
            sessionCount: 0,
            profileComplete: false,
            dateJoined: serverVal.TIMESTAMP,
            picture:
              'https://firebasestorage.googleapis.com/v0/b/brawlmoney.appspot.com/o/default%2Fdefault-user-image.png?alt=media&token=28b12112-9d7c-4bf3-b7f8-345927c120e7',
            profileProgress: {
              uploadPhoto: false,
              completeTutorial: false,
              hostAnEvent: false,
              attendAnEvent: false,
              joinAWorkoutNowSession: false,
              follow4Accounts: false,
              get10Followers: false,
              invite3Friends: false,
              leaveAReviewOnTheAppStore: false,
              sendFeedBack: false,
              shareToSocialMedia: false,
              shareAPhoto: false
            }
          },
          private: {
            email: this.state.email
          }
        });
        this.database
          .ref('/followers/' + authData.uid + '/4hbknUXtBTQbqrzroZJaYow6h7G3')
          .set(true);
        this.database
          .ref('/followings/4hbknUXtBTQbqrzroZJaYow6h7G3' + '/' + authData.uid)
          .set(true);
        this.setState({ loadingSubmit: false });
        const verifyEmailActions = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: 'VerifyEmailView',
              params: {
                authData,
                email: this.state.email,
                password: this.state.password
              }
            })
          ]
        });
        // navigation.navigate("VerifyEmailView", {authData: authData, email: this.state.email, password: this.state.password});
        navigation.dispatch(verifyEmailActions);
        action.setLoadingState(false);
      } catch (error) {
        action.setLoadingState(false);
        action.printAuthError(error.message);
        this.setState({ loadingSubmit: false });
      }
    })();
  }

  focusNextField = nextField => {
    this.props.action.clearAuthError();
    this.refs[nextField].focus();
  };

  _terms() {
    return <Text>{Terms}</Text>;
  }

  _privacy() {
    return <Text>{PrivacyText}</Text>;
  }

  _renderAgreement(view) {
    return (
      <View style={{ flex: 1, margin: 20, marginTop: 80, marginBottom: 80 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#fff',
            padding: 10,
            borderRadius: 5,
            shadowColor: 'black',
            shadowOpacity: 0.6,
            elevation: 2,
            shadowOffset: { width: 0, height: 0 },
            shadowRadius: 2,
            marginBottom: 20
          }}
        >
          <ScrollView>
            {view === 'privacy' ? this._privacy() : this._terms()}
          </ScrollView>
        </View>
        <Btn
          style={{ backgroundColor: 'white', alignSelf: 'center' }}
          textStyle={{ color: '#1D2F7B' }}
          text={'Done'}
          onPress={() => this.setState({ agreement: false })}
        />
      </View>
    );
  }
  _renderSignUpForm() {
    return (
      <View style={{ width, alignItems: 'center' }}>
        <View style={loginStyles.form}>
          <TextInput
            underlineColorAndroid={'transparent'}
            returnKeyType="next"
            maxLength={30}
            clearButtonMode="always"
            ref="1"
            onSubmitEditing={() => this.focusNextField('2')}
            style={loginStyles.input}
            onChangeText={text => this.setState({ firstName: text })}
            value={this.state.firstName}
            placeholder="First Name"
            autoCorrect={false}
            placeholderTextColor={'#aaa'}
          />
          {this.state.firstName.length > 0 ? (
            <Text
              style={{
                color: '#aaa',
                fontSize: 10,
                backgroundColor: 'transparent',
                letterSpacing: 2,
                position: 'absolute',
                top: 2,
                left: 22
              }}
            >
              FIRST NAME
            </Text>
          ) : (
            false
          )}
        </View>
        <View style={loginStyles.form}>
          <TextInput
            underlineColorAndroid={'transparent'}
            returnKeyType="next"
            maxLength={30}
            clearButtonMode="always"
            ref="2"
            onSubmitEditing={() => this.focusNextField('3')}
            style={loginStyles.input}
            onChangeText={text => this.setState({ lastName: text })}
            value={this.state.lastName}
            autoCorrect={false}
            placeholder="Last Name"
            placeholderTextColor={'#aaa'}
          />
          {this.state.lastName.length > 0 ? (
            <Text
              style={{
                color: '#aaa',
                fontSize: 10,
                backgroundColor: 'transparent',
                letterSpacing: 2,
                position: 'absolute',
                top: 2,
                left: 22
              }}
            >
              LAST NAME
            </Text>
          ) : (
            false
          )}
        </View>
        <View style={loginStyles.form}>
          <TextInput
            underlineColorAndroid={'transparent'}
            returnKeyType="next"
            maxLength={128}
            clearButtonMode="always"
            ref="3"
            onSubmitEditing={() => this.focusNextField('4')}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            style={loginStyles.input}
            onChangeText={text => this.setState({ email: text })}
            value={this.state.email}
            placeholder="Email"
            placeholderTextColor={'#aaa'}
          />
          {this.state.email.length > 0 ? (
            <Text
              style={{
                color: '#aaa',
                fontSize: 10,
                backgroundColor: 'transparent',
                letterSpacing: 2,
                position: 'absolute',
                top: 2,
                left: 22
              }}
            >
              EMAIL
            </Text>
          ) : (
            false
          )}
        </View>
        <View style={loginStyles.form}>
          <TextInput
            underlineColorAndroid={'transparent'}
            returnKeyType="next"
            maxLength={128}
            clearButtonMode="always"
            secureTextEntry={true}
            ref="4"
            onSubmitEditing={() => this.focusNextField('5')}
            style={loginStyles.input}
            onChangeText={text => this.setState({ password: text })}
            value={this.state.password}
            placeholder="Choose Password"
            placeholderTextColor={'#aaa'}
          />
          {this.state.password.length > 0 ? (
            <Text
              style={{
                color: '#aaa',
                fontSize: 10,
                backgroundColor: 'transparent',
                letterSpacing: 2,
                position: 'absolute',
                top: 2,
                left: 22
              }}
            >
              CHOOSE PASSWORD
            </Text>
          ) : (
            false
          )}
        </View>
        <View style={[loginStyles.form, { marginBottom: 0 }]}>
          {/* make sure the confirm password is the same */}
          <TextInput
            underlineColorAndroid={'transparent'}
            returnKeyType="join"
            maxLength={128}
            clearButtonMode="always"
            secureTextEntry={true}
            ref="5"
            onSubmitEditing={() => {
              this.setState({ loadingSubmit: true });
              this._handleEmailSignup();
            }}
            style={loginStyles.input}
            onChangeText={text => this.setState({ passwordConfirm: text })}
            value={this.state.passwordConfirm}
            placeholder="Confirm Password"
            placeholderTextColor={'#aaa'}
          />
          {this.state.passwordConfirm.length > 0 ? (
            <Text
              style={{
                color: '#aaa',
                fontSize: 10,
                backgroundColor: 'transparent',
                letterSpacing: 2,
                position: 'absolute',
                top: 2,
                left: 22
              }}
            >
              CONFIRM PASSWORD
            </Text>
          ) : (
            false
          )}
        </View>
      </View>
    );
  }
  render() {
    const { FitlyFirebase } = this.props.navigation.state.params;
    if (this.props.loading === true) {
      return (
        <View
          style={[
            {
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            },
            { backgroundColor: FitlyBlue }
          ]}
        >
          <Text style={loginStyles.logo}>Fitly</Text>
          <ActivityIndicator
            animating={this.state.loading}
            style={{ height: 80 }}
            size="large"
            color={'#fff'}
          />
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: '#1D2F7B' }}>
          {this.state.agreement ? (
            <Modal
              animationType={'fade'}
              transparent={true}
              visible={!!this.state.agreement}
              onRequestClose={() =>
                this.setState({
                  agreement: false
                })
              }
            >
              {this._renderAgreement(this.state.agreement)}
            </Modal>
          ) : (
            <View style={{ flex: 1 }}>
              <ScrollView
                keyboardDismissMode={isAndroid ? 'none' : 'on-drag'}
                contentContainerStyle={[
                  loginStyles.container,
                  { paddingBottom: 50 }
                ]}
              >
                <KeyboardAvoidingView
                  behavior="position"
                  style={loginStyles.KeyboardAvoidingContainer}
                >
                  <StatusBar barStyle="light-content" />

                  <Text style={loginStyles.logo}>Fitly</Text>
                  <TouchableHighlight
                    onPress={() =>
                      this.props.navigation.dispatch(
                        NavigationActions.reset({
                          index: 1,
                          actions: [
                            NavigationActions.navigate({
                              routeName: 'WelcomeView'
                            }),
                            NavigationActions.navigate({
                              routeName: 'SignInView',
                              params: {
                                FitlyFirebase: this.props.navigation.state
                                  .params.FitlyFirebase
                              }
                            })
                          ]
                        })
                      )
                    }
                    style={{ marginBottom: 20 }}
                  >
                    <Text style={loginStyles.boldSmall}>
                      {'Already have an account?\n Log in'}
                    </Text>
                  </TouchableHighlight>
                  {this.state.emailSignUp ? (
                    this._renderSignUpForm()
                  ) : (
                    <View>
                      <View style={{ alignItems: 'center' }}>
                        <View>
                          <GoogleloginBtn
                            navigation={this.props.navigation}
                            FitlyFirebase={FitlyFirebase}
                            style={{ marginBottom: 20 }}
                          />
                          <FBloginBtn
                            navigation={this.props.navigation}
                            FitlyFirebase={FitlyFirebase}
                            style={{ marginBottom: 20 }}
                          />
                          {
                            //   <LinkedInloginBtn
                            //   navigation={this.props.navigation}
                            //   FitlyFirebase={FitlyFirebase}
                            // />
                          }
                        </View>
                      </View>

                      <TouchableHighlight
                        style={[
                          loginStyles.FBbtn,
                          {
                            borderRadius: 30,
                            alignItems: 'center',
                            justifyContent: 'center'
                          }
                        ]}
                        onPress={() => {
                          this.props.action.clearAuthError();
                          this.setState({ emailSignUp: true });
                        }}
                      >
                        <View
                          style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: 'transparent'
                          }}
                        >
                          <FIcon
                            name="envelope-o"
                            size={32}
                            color={FitlyBlue}
                          />
                          <Text
                            style={[
                              loginStyles.btnText,
                              {
                                color: FitlyBlue,
                                width: 190
                              }
                            ]}
                          >
                            Continue with Email
                          </Text>
                        </View>
                      </TouchableHighlight>
                    </View>
                  )}
                  {this.props.error ? (
                    <Text
                      style={[
                        commonStyle.error,
                        {
                          backgroundColor: '#f00',
                          color: '#fff',
                          width: Dimensions.get('window').width,
                          padding: 10,
                          fontWeight: 'bold',
                          lineHeight: 21,
                          marginVertical: 10
                        }
                      ]}
                    >
                      {this.props.error}
                    </Text>
                  ) : (
                    <Text style={commonStyle.hidden}> </Text>
                  )}

                  <Text
                    style={[
                      loginStyles.disclamerText,
                      {
                        marginTop: 20,
                        textAlign: 'center',
                        alignSelf: 'center'
                      }
                    ]}
                  >
                    By tapping "Continue", you agree to our{'\n'}
                    <Text
                      style={{
                        color: 'white',
                        backgroundColor: 'transparent',
                        textDecorationLine: 'underline',
                        textDecorationColor: '#fff',
                        lineHeight: 22
                      }}
                      onPress={() => {
                        this.setState({ agreement: 'term' });
                      }}
                    >
                      Terms Of Service
                    </Text>{' '}
                    ,{' '}
                    <Text
                      style={{
                        color: 'white',
                        backgroundColor: 'transparent',

                        textDecorationLine: 'underline',
                        textDecorationColor: '#fff'
                      }}
                      onPress={() => {
                        this.setState({ agreement: 'privacy' });
                      }}
                    >
                      Privacy Policy
                    </Text>
                    {'\n'}
                    &{' '}
                    <Text
                      style={{
                        color: 'white',
                        backgroundColor: 'transparent'
                      }}
                    >
                      Cookie Policy
                    </Text>
                  </Text>
                </KeyboardAvoidingView>
              </ScrollView>
              {/* TODO: use SwipeableListView? */}
            </View>
          )}
          {this.state.emailSignUp ? (
            <TouchableHighlight
              style={[
                loginStyles.swipeBtn,
                {
                  position: 'absolute',
                  zIndex: 99,
                  bottom: 0,
                  left: 0
                }
              ]}
              onPress={() => {
                this.setState({ loadingSubmit: true });
                this._handleEmailSignup();
              }}
            >
              <View>
                {!this.state.loadingSubmit ? (
                  <Text style={loginStyles.btnText}>JOIN</Text>
                ) : (
                  <ActivityIndicator
                    animating={this.state.loadingSubmit}
                    size="large"
                    style={{ height: 50 }}
                    color={FitlyBlue}
                  />
                )}
              </View>
            </TouchableHighlight>
          ) : null}
        </View>
      );
    }
  }
}

const mapStateToProps = function(state) {
  return {
    loading: state.app.loading,
    error: state.auth.errorMsg
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        setFirebaseUID,
        setSignUpMethod,
        printAuthError,
        clearAuthError,
        setLoadingState
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ resetTo }, dispatch)
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUpView);
