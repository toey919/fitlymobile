import React, { Component } from 'react';
import {
  Text,
  Dimensions,
  ScrollView,
  Platform,
  Animated,
  UIManager,
  findNodeHandle
} from 'react-native';
import {
  TabViewAnimated,
  TabBarTop,
  TabViewPagerScroll
} from 'react-native-tab-view';
import Feeds from './Feeds.js';
import PhotoFeeds from './PhotoFeeds.js';
import { TabNavigator } from 'react-navigation';
const screenWidth = Dimensions.get('window').width;
import { profileStyle } from '../styles/styles.js';

export default class FeedTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [{ key: '1', title: 'Feed' }, { key: '2', title: 'Photos' }]
    };
  }
  _handleChangeTab = index => {
    this.props.resetScroll();
    this.setState({ index });
    if (index == 1) {
      this.props.blockScrollHeight();
      this.props.disableTut2();
    } else this.props.releaseScrollHeight();
  };

  _renderHeader = props => {
    const indicatorWidth = 65;
    const marginleft = screenWidth / 4 - indicatorWidth / 2;
    return (
      <TabBarTop
        {...props}
        style={{ backgroundColor: 'white' }}
        labelStyle={{ fontSize: 14, color: 'grey' }}
        indicatorStyle={{
          backgroundColor: '#326fd1',
          alignSelf: 'center',
          marginLeft: marginleft,
          width: indicatorWidth
        }}
      />
    );
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case '1':
        return (
          <Feeds
            screenProps={this.props.screenProps}
            navigation={this.props.navigation}
            feeds={this.props.feeds}
            profile={this.props.profile}
            viewing={this.props.viewing}
            blockTouchables={this.props.blockTouchables}
          />
        );

      case '2':
        return (
          <PhotoFeeds
            blockTouchables={this.props.blockTouchables}
            screenProps={this.props.screenProps}
            navigation={this.props.navigation}
            feeds={this.props.feeds}
            profile={this.props.profile}
          />
        );
    }
  };

  _renderPager = props => {
    return <TabViewPagerScroll {...props} swipeEnabled animationEnabled />;
  };

  render() {
    return (
      <TabViewAnimated
        navigationState={this.state}
        style={{ flex: 1 }}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onRequestChangeTab={this._handleChangeTab}
        renderPager={this._renderPager}
        initialLayout={{ width: screenWidth, height: 0 }}
      />
    );
  }
}
