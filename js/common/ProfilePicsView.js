import React, { Component } from 'react';
import {
  ScrollView,
  Image,
  View,
  Text,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Firebase from 'firebase';
import Icon from 'react-native-vector-icons/Ionicons';
import Carousel from 'react-native-carousel';
import {
  selectPictureCropper,
  getImageFromCam
} from '../library/pictureHelper.js';
import { uploadPhoto } from '../library/firebaseHelpers.js';

import HeaderInView from '../header/HeaderInView.js';

const { width, height } = Dimensions.get('window');
const isAndroid = Platform.OS === 'android';

class ProfilePicsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pictures: []
    };

    this.database = this.props.FitlyFirebase.database();
    this.uID = this.props.navigation.state.params.uID;
    this.dbLocation = 'users/' + this.uID + '/public/';
    this.storageLocation = 'users/' + this.uID + '/profilePic/';
  }

  componentDidMount() {
    this.database
      .ref(this.dbLocation + 'profilePictures')
      .once('value')
      .then(snap => {
        if (!snap.val()) return;
        this.setState({
          pictures: this.state.pictures.concat(Object.values(snap.val()))
        });
      });
  }

  _renderHeader = () => {
    const customStyles = { zIndex: 0 };
    return (
      <HeaderInView
        customStyles={isAndroid ? customStyles : []}
        leftElement={{ icon: 'ios-close' }}
        title={'Profile Photos'}
        _onPressLeft={() => this.props.navigation.goBack()}
      />
    );
  };
  _takePhoto = () => {
    getImageFromCam(picture => this._storePhoto(picture.path));
  };
  _uploadPhoto = () => {
    selectPictureCropper(true).then(picture => this._storePhoto(picture.uri));
  };
  _storePhoto = uri => {
    this.setState({ loading: true });
    uploadPhoto(`users/${this.props.uID}/profilePic/`, uri, {
      profile: false
    })
      .then(link => {
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}/public/picture`)
          .set(link);
        return link;
      })
      .then(link => {
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}/public/profilePictures`)
          .push()
          .set(link);
        if (
          !this.props.user.public.profileProgress ||
          !this.props.user.public.profileProgress.uploadPhoto
        ) {
          this.props.FitlyFirebase.database()
            .ref(`users/${this.props.uID}/public/profileProgress`)
            .update({ uploadPhoto: true });
          this.props.FitlyFirebase.database()
            .ref(`users/${this.props.uID}`)
            .on('value', snap => this.props.action.storeUserProfile(snap.val()))
            .catch(err => console.log(err));
        }
        this.setState(
          { loading: false, pictures: this.state.pictures.concat(link) },
          () => this.props.navigation.goBack()
        );
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log('upload profile pic', error);
      });
  };
  render() {
    //console.log("pictures", this.state.pictures);
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {this._renderHeader()}
        <View style={{ flex: 0.9 }}>
          <Carousel
            width={width}
            loop={false}
            animate={false}
            indicatorSize={30}
            indicatorOffset={0}
            indicatorColor="black"
            inactiveIndicatorColor="white"
          >
            {this.state.pictures.map((p, i) => (
              <View key={i} style={{ flex: 1, width: width }}>
                <Image
                  source={{ uri: p }}
                  //style={{width: undefined, height: undefined}}
                  style={{ flex: 1 }}
                />
              </View>
            ))}
          </Carousel>
        </View>
        <View style={{ flex: 0.1 }}>
          <View style={{ flex: 1, flexDirection: 'row', borderTopWidth: 0.5 }}>
            <TouchableOpacity
              onPress={this._takePhoto}
              style={{
                flex: 0.5,
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0.5
              }}
            >
              <Text>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this._uploadPhoto}
              style={{
                flex: 0.5,
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0.5
              }}
            >
              <Text>Upload Photo</Text>
            </TouchableOpacity>
          </View>
        </View>
        {this.state.loading && (
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,.5)',
              height,
              width,
              position: 'absolute',
              top: 0,
              left: 0,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <ActivityIndicator
              animating={this.state.loading}
              size={'large'}
              color={'#fff'}
            />
            <Text style={{ color: '#fff', textAlign: 'center' }}>
              Please wait...{'\n'}We are uploading the picture{'\n'}It may take
              a few moments
            </Text>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    loading: state.app.loading,
    user: state.user.user,
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    //exnavigation: bindActionCreators({ push }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePicsView);
