import React, { Component } from 'react';
import { TextInput, Text } from 'react-native';

class AutoExpandingTextInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      parts: null
    };
  }
  _disintegrateText(text) {
    let delimiter = /\s+/;
    let _text = text;
    let token,
      index,
      parts = [];
    while (_text) {
      delimiter.lastIndex = 0;
      token = delimiter.exec(_text);
      if (token === null) {
        break;
      }
      index = token.index;
      if (token[0].length === 0) {
        index = 1;
      }
      parts.push(_text.substr(0, index));
      parts.push(token[0]);
      index = index + token[0].length;
      _text = _text.slice(index);
    }
    parts.push(_text);
    //highlight hashtags
    parts = parts.map(text => {
      if (/^#/.test(text)) {
        return (
          <Text
            style={{
              backgroundColor: 'rgba(100, 170, 185, 0.22)',
              fontWeight: '600'
            }}
            key={text}
          >
            {text}
          </Text>
        );
      } else {
        return text;
      }
    });
    this.setState({ parts });
  }
  _intialParts() {
    this._disintegrateText(this.props.initialValue);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.dynamicValue !== this.props.dynamicValue) {
      this._disintegrateText(nextProps.dynamicValue);
    }
  }
  componentWillMount() {
    this._intialParts();
  }
  render() {
    return (
      <TextInput
        {...this.props}
        ref={ref => {
          if (this.props.thisRef) this.props.thisRef(ref);
        }}
        autoCapitalize={'sentences'}
        // defaultValue={this.props.initialValue}
        // onChangeText={text => {
        //   // this._disintegrateText(text);
        //   this.props.onChangeText(text);
        // }}
        underlineColorAndroid={'transparent'}
        onContentSizeChange={event => {
          this.setState({ height: event.nativeEvent.contentSize.height });
        }}
        style={[
          {
            height: Math.max(25, this.state.height),
            flex: 1,
            color: 'transparent'
          },
          this.props.style
        ]}
      >
        <Text>{this.state.parts}</Text>
      </TextInput>
    );
  }
}

export default AutoExpandingTextInput;
