var {
  GooglePlacesAutocomplete
} = require('react-native-google-places-autocomplete');
import React, { Component } from 'react';
import { PLACE_API_KEY } from '../../credentials/GOOGLE_PLACE_API_KEY.js';
import { Dimensions } from 'react-native';
const { width } = Dimensions.get('window');
export default (PlaceSearchBox = props => {
  return (
    <GooglePlacesAutocomplete
      placeholder={props.placeholder || 'Search'}
      minLength={2} // minimum length of text to search
      autoFocus={true}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed={true} // true/false/undefined
      fetchDetails={true}
      // renderDescription={row => row.terms[0].value} // display street only
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        props.onPress(data, details);
      }}
      getDefaultValue={text => {
        // this.props.getDefaultValue(text);
      }}
      underlineColorAndroid={'rgba(0,0,0,0)'}
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: PLACE_API_KEY,
        language: 'en' // language of the results
      }}
      styles={{
        description: {
          fontWeight: '200',
          letterSpacing: 1.5
        },
        textInputContainer: {
          backgroundColor: 'white',
          paddingTop: 2.5,
          borderRadius: 20,
          borderWidth: 0.5,
          marginHorizontal: 5,
          height: 35,
          borderColor: '#ccc',
          borderTopColor: '#ccc',
          borderBottomColor: '#ccc',
          justifyContent: 'center',
          paddingHorizontal: 15
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        },
        listView: { backgroundColor: '#fff', width: width - 30, left: 15 },
        textInput: {
          paddingTop: 0,
          paddingBottom: 0,
          paddingLeft: 0,
          paddingRight: 0,
          marginTop: 8,
          marginRight: 0,
          marginTop: 0,
          marginLeft: 0
        },
        powered: {
          width: null,
          height: null
        },
        poweredContainer: {
          height: 0,
          margin: 0,
          padding: 0
        }
      }}
      // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={
        {
          // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
        }
      }
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        types: 'food'
      }}
      filterReverseGeocodingByTypes={[
        'locality',
        'administrative_area_level_3'
      ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
    />
  );
});
