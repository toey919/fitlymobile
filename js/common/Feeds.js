import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions } from 'react-native';
import { feedEntryStyle } from '../styles/styles.js';
import { push } from '../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TimeAgo from 'react-native-timeago';
import {
  getWeekdayMonthDay,
  getHrMinDuration
} from '../library/convertTime.js';
import Icon from 'react-native-vector-icons/Ionicons';
import { FitlyFirebase } from '../library/firebaseHelpers.js';

const screenWidth = Dimensions.get('window').width;

class Feeds extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
      details: false,
      hide: {}
      // taggedUsersNames: ['sample', 'sample']
    };

    this.profile = this.props.profile;

    // TODO: settings will dictate what kind of feed should trigger a push notification and should be rendered
  }

  _renderUpdateMsg(feed) {
    let description;
    const article =
      feed.contentType && 'aeiou'.indexOf(feed.contentType[0]) > -1
        ? 'an'
        : 'a';
    if (feed.type === 'post') {
      description = `posted a new ${feed.description}`;
    } else if (feed.type === 'follow') {
      description = 'followed';
    } else if (feed.type === 'share') {
      const artical = feed.contentType;
      description = `shared ${article} ${feed.contentType}`;
    } else if (feed.type === 'event') {
      if (feed.organizers[feed.ownerID]) {
        description = 'created an event';
      } else {
        description = 'shared an event';
      }
    } else if (feed.type === 'like') {
      description = `liked ${article} ${feed.contentType}`;
    }
    return <Text style={feedEntryStyle.description}>{description}</Text>;
  }

  _renderPhotos(feed) {
    const { photos = [] } = feed;
    const { contentID } = feed;
    return (
      <View style={feedEntryStyle.imgContainer}>
        {photos.map((photo, index) => (
          <View
            activeOpacity={1}
            style={[feedEntryStyle.imagesTouchable, { marginLeft: -20 }]}
            key={`feedPhotos${index}`}
            onPress={() =>
              this.props.navigation.navigate('PostView', {
                postID: contentID
              })
            }
          >
            <Image
              style={feedEntryStyle.images}
              source={{ uri: photo.link }}
              style={feedEntryStyle.images}
              defaultSource={require('../../img/default-photo-image.png')}
            />
          </View>
        ))}
      </View>
    );
  }
  _renderPostFeedEntry(feed) {
    return (
      <View style={{ flex: 0 }}>
        <View style={{ marginLeft: 40 }}>
          {feed.contentTitle ? (
            <Text style={{ marginVertical: 5 }}>{feed.contentTitle}</Text>
          ) : null}
          {feed.contentSnipet ? (
            <Text style={{ marginBottom: 5 }}>
              {`${feed.contentSnipet}...`}
            </Text>
          ) : null}
          {feed.taggedUsers && feed.taggedUsers.length ? (
            <Text
              style={{
                fontSize: 10,
                color: '#666',
                alignSelf: 'flex-end',
                marginBottom: 10
              }}
            >
              with{' '}
              <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#444' }}>
                {feed.taggedUsers[0].name
                  .replace(/\s(.)/g, function($1) {
                    return $1.toUpperCase();
                  })
                  .replace(/\s/g, ' ')
                  .replace(/^(.)/, function($1) {
                    return $1.toUpperCase();
                  })}
              </Text>
              {feed.taggedUsers.length >= 2 ? ' and ' : ''}
              {feed.taggedUsers.length >= 2 ? (
                <Text
                  style={{ fontSize: 10, fontWeight: 'bold', color: '#444' }}
                >
                  {feed.taggedUsers.length > 2
                    ? feed.taggedUsers.length - 1
                    : feed.taggedUsers[1].name}
                </Text>
              ) : (
                false
              )}
              {feed.taggedUsers.length > 2 ? ' others' : ''}
            </Text>
          ) : (
            false
          )}
        </View>
        {this._renderPhotos(feed)}
      </View>
    );
  }

  _renderActivityEntry(feed) {
    const startDate = new Date(feed.startDate);
    const endDate = new Date(feed.endDate);
    const eventExpired = endDate < new Date();
    const color =
      feed.status !== 'cancelled' && !eventExpired ? 'green' : 'red';

    return (
      <View style={{ flex: 0 }}>
        <View>
          <Text
            style={{
              paddingBottom: 10,
              textAlign: 'center'
            }}
          >
            {feed.contentTitle}
          </Text>
        </View>
        <View
          style={{
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderBottomColor: color,
            borderTopColor: color,
            position: 'absolute',
            paddingVertical: 2,
            paddingHorizontal: 20,
            right: -35,
            top: -5,
            transform: [{ rotate: '-35deg' }]
          }}
        >
          <Text
            style={{
              fontSize: 10,
              color
            }}
          >
            {feed.status === 'cancelled'
              ? ' (CANCELLED)'
              : eventExpired
                ? ' (FINISHED)'
                : ' (UPCOMING)'}
          </Text>
        </View>
        {this._renderPhotos(feed.photos)}
      </View>
    );
  }

  _renderFeedEntryContent(feed) {
    if (feed.type === 'post') {
      return this._renderPostFeedEntry(feed);
    } else if (feed.type === 'event') {
      return this._renderActivityEntry(feed);
    } else return null;
  }

  _goToProfile(id) {
    if (id === this.props.uID || id === this.props.viewing) {
    } else {
      this.props.navigation.navigate('ProfileEntry', {
        otherUID: id
      });
    }
  }

  _renderMenu() {
    const feedKey = {};
    feedKey[this.state.details.feedKey] = true;
    return (
      <View
        style={{
          position: 'absolute',
          top: 10,
          right: 60,
          borderColor: '#aaa',
          borderWidth: 1,
          flex: 1,
          flexDirection: 'column',
          zIndex: 200,
          backgroundColor: '#fff',
          width: 100,
          padding: 10,
          shadowColor: 'black',
          shadowOpacity: 0.6,
          elevation: 2,
          shadowOffset: { width: 0, height: 0 },
          shadowRadius: 2
        }}
      >
        <TouchableOpacity
          onPress={() => {
            this.removeFeedItem(this.state.details);
            this.setState({
              menuOpen: false,
              hide: Object.assign({}, this.state.hide, feedKey)
            });
          }}
        >
          <Text>Hide</Text>
        </TouchableOpacity>
      </View>
    );
  }

  removeFeedItem(details) {
    this.props.FitlyFirebase.database()
      .ref(`feeds/${this.props.uID}/${details.feedKey}`)
      .remove();
  }

  _renderMenuButton(id, feed) {
    return (
      <TouchableOpacity
        style={{
          position: 'absolute',
          top: 20,
          right: 20,
          backgroundColor: 'transparent'
        }}
        onPress={() => {
          this.setState({
            menuOpen:
              this.state.menuOpen && this.state.menuOpen === id ? false : id,
            details: feed
          });
        }}
      >
        <Icon name="ios-more" size={30} color="gray" />
      </TouchableOpacity>
    );
  }

  render() {
    const currentFeedFitlerCondition = feed =>
      !(
        feed.type === 'like' ||
        feed.type === 'follow' ||
        feed.type === 'share' ||
        !feed.isActive
      );
    if (
      this.props.feeds.filter(feed => currentFeedFitlerCondition(feed)).length >
      0
    ) {
      return (
        <View style={{ flex: 0, alignSelf: 'stretch' }}>
          {this.props.feeds
            .filter(feed => currentFeedFitlerCondition(feed))
            .map((feed, index) => {
              let onPress;
              if (feed.type === 'post') {
                onPress = () =>
                  this.props.navigation.navigate('PostView', {
                    postID: feed.contentID
                  });
              } else if (feed.type === 'event') {
                const isAdmin =
                  (feed.organizers && !!feed.organizers[this.props.uID]) ||
                  false;
                onPress = () =>
                  this.props.navigation.navigate('EventScene', {
                    eventID: feed.contentID,
                    isAdmin,
                    fromProfile: true,
                    rootNav: this.props.screenProps.rootNavigation
                  });
              } else {
                onPress = () => {};
              }
              return (
                <TouchableOpacity
                  onPress={!this.props.blockTouchables && onPress}
                  activeOpacity={1}
                  style={[feedEntryStyle.container, { overflow: 'hidden' }]}
                  key={`feed${index}`}
                >
                  <View
                    style={[feedEntryStyle.profileRow, { marginBottom: 0 }]}
                  >
                    <Image
                      source={
                        feed.ownerPicture
                          ? { uri: feed.ownerPicture }
                          : require('../../img/default-user-image.png')
                      }
                      style={feedEntryStyle.defaultImg}
                      defaultSource={require('../../img/default-user-image.png')}
                    />
                    <View>
                      <Text
                        style={feedEntryStyle.username}
                        onPress={() => this._goToProfile(feed.ownerID)}
                      >
                        {feed.ownerName}
                      </Text>
                      {this._renderUpdateMsg(feed)}
                    </View>
                  </View>
                  <TimeAgo
                    style={feedEntryStyle.timestamp}
                    time={feed.timestamp}
                  />

                  {this.profile
                    ? null
                    : this._renderMenuButton(feed.contentID, feed)}
                  {this.state.menuOpen === feed.contentID
                    ? this._renderMenu()
                    : null}
                  {this._renderFeedEntryContent(feed)}
                </TouchableOpacity>
              );
            })}
        </View>
      );
    }
    return (
      <View style={{ flex: 1, width: screenWidth }}>
        <Text
          style={{
            textAlign: 'center',
            color: '#ccc',
            marginTop: 30,
            marginLeft: 30,
            marginRight: 30
          }}
        >
          Your feeds are empty, let's find someone you want to follow
        </Text>
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    exnavigation: bindActionCreators({ push }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Feeds);
