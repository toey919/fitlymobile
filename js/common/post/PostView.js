import React, { Component } from 'react';
import {
  postStyle,
  feedEntryStyle,
  composeStyle,
  headerStyle,
  profileStyle,
  FitlyBlue
} from '../../styles/styles.js';
import {
  Modal,
  View,
  TextInput,
  Text,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  Alert
} from 'react-native';
const { width } = Dimensions.get('window');
// import AutoExpandingTextInput from '../../common/AutoExpandingTextInput.js';
import Icon from 'react-native-vector-icons/Ionicons';
import { save, clear } from '../../actions/drafts.js';

import { push, pop, resetTo } from '../../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  convertFBObjToArray,
  saveUpdateToDB,
  turnOnCommentListener,
  turnOffCommentListener,
  UpdateUserUpdateToDB,
  randomString
} from '../../library/firebaseHelpers.js';
import { NavigationActions } from 'react-navigation';
import TimeAgo from 'react-native-timeago';
import Firebase from 'firebase';
import CommentsModal from '../comment/CommentsModal.js';
import SocialBtns from '../SocialBtns.js';
import Author from '../Author.js';
import HeaderInView from '../../header/HeaderInView.js';
import Spinner from 'react-native-loading-spinner-overlay';
import NewCommentIcon from '../NewCommentIcon';

class PostView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: null,
      commentLoading: true,
      menuOpen: false,
      author: {},
      deleting: false
    };
    this.FitlyFirebase = this.props.FitlyFirebase;
    this.database = this.FitlyFirebase.database();
    this.uID = this.props.uID;
    this.user = this.props.user;
  }

  componentWillMount() {
    this.postRef = this.database.ref(
      `posts/${this.props.navigation.state.params.postID}`
    );
    this._turnOnPostListener();
  }
  _turnOnPostListener() {
    const handlePostUpdates = async postSnap => {
      const postObj = postSnap.val();
      if (!postObj) {
        return;
      }
      postObj.photos = await convertFBObjToArray(postSnap.child('photos'));
      postObj.tags = Object.keys(postObj.tags || {});
      this.setState({
        post: postObj
      });
      this.database
        .ref('users/' + postObj.author + '/public')
        .once('value')
        .then(data => {
          this.setState({ author: data.val() });
        });
      this.postRef.off('value');
    };
    this.postRef.on('value', handlePostUpdates);
  }

  _renderPost() {
    const initialRoute = {
      contentID: this.props.navigation.state.params.postID,
      contentType: 'post',
      parentAuthor: this.state.post.author
    };
    return (
      <View style={{ flex: 1, marginTop: 79 }}>
        <CommentsModal
          navigation={this.props.navigation}
          screenProps={this.props.screenProps}
          modalVisible={true}
          initialRoute={initialRoute}
        />
      </View>
    );
  }

  _openMenu() {
    this.setState({ menuOpen: !this.state.menuOpen });
  }
  async _deletePost() {
    await this.postRef.update({ isActive: false, inactiveOrNoPhotos: true });
    await UpdateUserUpdateToDB(
      { isActive: false, inactiveOrNoPhotos: true },
      this.props.navigation.state.params.postID,
      this.uID
    );
    this.setState({ deleting: false });
    this.props.screenProps.rootNavigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'TabNavigator',
            params: { newUser: false }
          })
        ]
      })
    );
  }
  _renderMenu() {
    return (
      <View
        style={{
          position: 'absolute',
          top: 60,
          right: 20,
          borderColor: '#aaa',
          borderWidth: 1,
          flex: 1,
          flexDirection: 'column',
          zIndex: 9999,
          backgroundColor: '#fff',
          width: 100,
          padding: 10,
          shadowColor: 'black',
          shadowOpacity: 0.6,
          elevation: 2,
          shadowOffset: { width: 0, height: 0 },
          shadowRadius: 2
        }}
        onTouchStart={event => {
          event.stopPropagation();
        }}
      >
        <TouchableOpacity
          onPress={() => {
            this._openMenu();
            this.props.screenProps.rootNavigation.navigate('ReportScene', {
              type: 'post',
              details: this.state.post,
              contentID: this.props.navigation.state.params.postID
            });
          }}
          style={{
            height: 30,
            justifyContent: 'center',
            borderBottomColor: '#ccc',
            borderBottomWidth: 0.5
          }}
        >
          <Text>Report...</Text>
        </TouchableOpacity>
        {this.state.post.author === this.props.uID && (
          <TouchableOpacity
            onPress={() => {
              this._openMenu();
              Alert.alert(
                'Are you sure?',
                'Do you really want to delete this awesome post?',
                [
                  {
                    text: 'I changed my mind',
                    onPress: () => {}
                  },
                  {
                    text: 'Just do it!',
                    onPress: () => {
                      this.setState({ deleting: true });
                      this._deletePost();
                    }
                  }
                ]
              );
            }}
            style={{ height: 30, justifyContent: 'center' }}
          >
            <Text>Delete</Text>
          </TouchableOpacity>
        )}
        {this.state.post.author === this.props.uID && (
          <TouchableOpacity
            onPress={() => {
              this._openMenu();
              const draftRef = randomString();
              this.props.draftsAction.save(draftRef, {
                category: 'Workout Plan',
                title: '',
                content: '',
                tags: [],
                photos: [],
                photoRefs: null
              });
              this.props.navigation.navigate('ComposePost', {
                post: this.state.post,
                draftRef,
                postID: this.props.navigation.state.params.postID
              });
            }}
            style={{ height: 30, justifyContent: 'center' }}
          >
            <Text>Edit</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
  render() {
    return (
      <View
        style={{ flex: 1, backgroundColor: '#fff' }}
        onTouchStart={() => this.setState({ menuOpen: false })}
      >
        {this.state.post || this.state.deleting ? (
          this._renderPost()
        ) : (
          <Spinner
            visible={!this.state.post}
            color={FitlyBlue}
            size={'small'}
            overlayColor={'transparent'}
          />
        )}

        <View
          style={{
            zIndex: 999,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0
          }}
        >
          <HeaderInView
            leftElement={{ icon: 'ios-arrow-round-back-outline' }}
            rightElement={{ icon: 'ios-more' }}
            title={this.state.post ? `${this.state.post.category}` : ''}
            _onPressLeft={() => this.props.navigation.goBack()}
            _onPressRight={this._openMenu.bind(this)}
          />
        </View>
        {this.state.menuOpen && this._renderMenu()}
      </View>
    );
  }
}

PostView.propTypes = {
  postID: React.PropTypes.string
};

const mapStateToProps = state => ({
  user: state.user.user,
  uID: state.auth.uID,
  FitlyFirebase: state.app.FitlyFirebase
});

const mapDispatchToProps = dispatch => ({
  exnavigation: bindActionCreators({ push, pop, resetTo }, dispatch),
  draftsAction: bindActionCreators({ save, clear }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PostView);
