import React, { Component } from 'react';
import { composeStyle, headerStyle, FitlyBlue } from '../../styles/styles.js';
import {
  Modal,
  View,
  TextInput,
  Text,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform
} from 'react-native';
import AutoExpandingTextInput from '../../common/AutoExpandingTextInput.js';
import Icon from 'react-native-vector-icons/Ionicons';
import TagInput from 'react-native-tag-input';
const hashTagRegex = /^\w+$/g;

const isAndroid = Platform.OS === 'android';

const ImageEditModal = props => {
  const renderFullSizeImgs = draftState => {
    return draftState.photos.map((photo, index) => {
      return (
        <View key={index} style={composeStyle.imgLarge}>
          <TouchableOpacity
            style={composeStyle.closeBtn}
            onPress={() => props.onRemoveImage(index)}
          >
            <Icon
              name="ios-close-outline"
              size={50}
              color="rgba(255,255,255,.7)"
            />
          </TouchableOpacity>
          <Image
            style={{ width: null, height: 400 }}
            source={{ uri: photo.path, isStatic: true }}
          />
          <AutoExpandingTextInput
            clearButtonMode="always"
            onChangeText={text => props.onCaptionChange(text, index)}
            style={[composeStyle.input, { fontSize: 16 }]}
            multiline={true}
            placeholder="caption"
            placeholderTextColor="grey"
          />
          <View style={[composeStyle.hashTagInput, { borderWidth: 0 }]}>
            <Text style={composeStyle.hashTag}>#</Text>
            <TagInput
              value={photo.tags ? photo.tags : []}
              onChange={tags => props.onTagChange(tags, index)}
              regex={hashTagRegex}
            />
          </View>
        </View>
      );
    });
  };

  return (
    <Modal
      animationType={'slide'}
      transparent={false}
      visible={props.visible}
      onRequestClose={props.onRequestClose}
    >
      <StatusBar barStyle="light-content" backgroundColor="#112F7B" />
      <View
        style={{
          height: 70,
          backgroundColor: FitlyBlue,
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
          paddingTop: 10
        }}
      >
        <TouchableOpacity
          style={{
            flex: 2,
            marginLeft: 20
          }}
          onPress={props.onBack}
        >
          <Icon name="md-arrow-back" size={30} color="#FFF" />
        </TouchableOpacity>

        <Text
          style={{
            fontSize: 20,
            alignSelf: 'center',
            color: '#FFF',
            flex: 2,
            fontWeight: 'bold'
          }}
        >
          Edit Images
        </Text>
        <View
          style={{
            flex: 2
          }}
        />
      </View>
      <KeyboardAvoidingView behavior="position" style={{ flex: 0 }}>
        <ScrollView
          keyboardDismissMode={isAndroid ? 'none' : 'on-drag'}
          contentContainerStyle={{ flex: 0 }}
        >
          {renderFullSizeImgs(props.draftState)}
          <View
            style={[
              composeStyle.photosSection,
              {
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 70
              }
            ]}
          >
            <TouchableOpacity
              style={composeStyle.photoThumbnail}
              onPress={props.getImageFromLib}
            >
              <Icon name="ios-image-outline" size={30} color="#bbb" />
            </TouchableOpacity>
            <TouchableOpacity
              style={composeStyle.photoThumbnail}
              onPress={props.getImageFromCam}
            >
              <Icon name="ios-camera-outline" size={30} color="#bbb" />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export default ImageEditModal;
