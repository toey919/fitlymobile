import React, { Component } from 'react';
import { composeStyle, headerStyle, FitlyBlue } from '../../styles/styles.js';
import {
  Modal,
  View,
  TextInput,
  Text,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  Platform,
  Dimensions
} from 'react-native';
import { postCategories } from '../../constants/categories';
import AutoExpandingTextInput from '../../common/AutoExpandingTextInput.js';
import HeaderInView from '../../header/HeaderInView.js';
import ImageEditModal from './ImageEditModal.js';
import Icon from 'react-native-vector-icons/Ionicons';
import { pop, push, resetTo } from '../../actions/navigation.js';
import { save, clear } from '../../actions/drafts.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { storeUserProfile } from '../../actions/user';
import {
  savePhotoToDB,
  saveUpdateToDB,
  saveTags,
  UpdateUserUpdateToDB
} from '../../library/firebaseHelpers.js';
import {
  getImageFromCam,
  getImageFromLib
} from '../../library/pictureHelper.js';
import Firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay';

const isAndroid = Platform.OS === 'android';
import { randomString } from '../../library/firebaseHelpers';
import { NavigationActions } from 'react-navigation';
import TagInput from 'react-native-tag-input';
import UserListView from '../UserListView.js';
import Query from '../../library/Query.js';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const { width, height } = Dimensions.get('window');
// TODO: input validation??
const hashTagRegex = /^\w+$/g;

class ComposePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCategory: 0,
      currentItem: 0,
      loading: false,
      modalVisible: false,
      contentType: 'light-content',
      extraContent: '',
      searchResults: [],
      friendsDetails: {},
      text: '',
      posOfSearchUsers: 0
    };
    this.draftsAction = this.props.draftsAction;
    this.setDraftState = this.props.draftsAction.save.bind(
      this,
      this.props.navigation.state.params.draftRef
    );
    this.clearState = this.props.draftsAction.clear.bind(
      this,
      this.props.navigation.state.params.draftRef
    );
    this.post = this.props.navigation.state.params.post;
    this.user = this.props.user;
    this.uID = this.props.uID;
    this.postID = this.props.navigation.state.params.postID;
    this.FitlyFirebase = this.props.FitlyFirebase;
    this.userQuery = new Query('user', this.uID);
    this.doneTypingInterval = 500;
    this.typingTimer;
  }
  componentDidMount() {
    this.refs.scrollView.scrollTo({
      x: this.post
        ? this.post.category.toLowerCase() === 'workout plan'
          ? width
          : this.post.category.toLowerCase() === 'meal plan'
            ? 2 * width
            : 3 * width
        : 3 * width,
      animated: false
    });
  }
  componentWillMount() {
    if (this.post) {
      const {
        title,
        tags,
        photos,
        taggedUsers,
        content,
        extraContent,
        category
      } = this.post;
      this.setDraftState({
        title,
        tags,
        photos,
        friends: taggedUsers,
        content,
        extraContent,
        category
      });
      let friendsDetails = {};
      taggedUsers.forEach(friend => {
        const { userID, name } = friend;
        friendsDetails[userID] = friend;
      });
      this.setState({ friendsDetails });
    }
  }
  // TODO: when hit summit does the view redirects to the post display view directly?
  // disables cliking send
  _savePostToDB() {
    // tables to update: posts, userPosts, userUpdatesMajor, userUpdatesAll
    (async () => {
      try {
        const draftState = this.props.drafts[
          this.props.navigation.state.params.draftRef
        ];
        if (
          !draftState.photos.length &&
          (!draftState.title && !draftState.content)
        ) {
          this.setState(
            {
              loading: false
            },
            () => {
              Alert.alert(
                'Information Missing\nPlease fill out at least the title and content or add a picture'
              );
            }
          );
          return;
        }
        this.setState({ loading: true });
        const postKey = this.postID
          ? this.postID
          : this.FitlyFirebase.database()
              .ref()
              .child('posts')
              .push().key;
        const authorInfo = {
          author: this.uID,
          authorName: `${this.user.public.first_name} ${
            this.user.public.last_name
          }`,
          authorPicture: this.user.public.picture
        };
        let notifImage = {};
        let photoRefObject;
        if (draftState.photos.length) {
          let newPhotos = [];
          let oldPhotos = [];
          draftState.photos.forEach(photo => {
            if (photo.path) newPhotos.push(photo);
            else oldPhotos.push(photo);
          });

          const photoRefs = await savePhotoToDB(
            newPhotos,
            authorInfo,
            `/posts/${postKey}`
          );
          photoRefObject = photoRefs.reduce((refObj, photoRef) => {
            if (!notifImage) notifImage = photoRef.link;
            refObj[photoRef.key] = {
              link: photoRef.link,
              timestamp: Firebase.database.ServerValue.TIMESTAMP
            };
            return refObj;
          }, {});
          oldPhotos.forEach(oP => {
            photoRefObject[oP.key] = {
              link: oP.link,
              timestamp: Firebase.database.ServerValue.TIMESTAMP
            };
          });
        }
        const noPhotos = !draftState.photos.length;
        const photos = draftState.photos.length ? photoRefObject : {};
        const tagRegEx = /(^|\s)(#[a-z\d-]+)/gi;
        let newTags = [];
        let tags = [];
        let tagContent = draftState.content
          ? draftState.content.match(tagRegEx)
          : null;
        let tagDesc = draftState.extraContent
          ? draftState.extraContent.match(tagRegEx)
          : null;
        let tagTitle = draftState.title.match(tagRegEx);
        if (tagDesc) newTags = newTags.concat(tagDesc);
        if (tagContent) newTags = newTags.concat(tagContent);
        if (tagTitle) newTags = newTags.concat(tagTitle);
        for (let i = 0; i < newTags.length; i++) {
          tags.push(newTags[i].replace('#', ''));
        }
        this.setDraftState({ tags: tags });
        let tagsArray = tags || [];
        let tagObj = tagsArray.reduce((tags, tag) => {
          tags[tag] = true;
          return tags;
        }, {});
        if (draftState.tags.length) saveTags(draftState.tags, postKey, 'post');
        let postObj = {
          author: this.uID,
          authorName: `${this.user.public.first_name} ${
            this.user.public.last_name
          }`,
          authorPicture: this.user.public.picture,
          title: draftState.title,
          content: draftState.content,
          category: draftState.category,
          extraContent: draftState.extraContent || '',
          tags: tagObj,
          taggedUsers: draftState.friends || []
        };
        const newObj = {
          shareCount: 0,
          saveCount: 0,
          replyCount: 0,
          isActive: true,
          noPhotos,
          inactiveOrNoPhotos: noPhotos,
          likeCount: 0,
          createdAt: Firebase.database.ServerValue.TIMESTAMP
        };
        postObj = { ...postObj, photos, ...(this.post ? newObj : {}) };
        this.FitlyFirebase.database()
          .ref(`/posts/${postKey}`)
          .set(postObj)
          .then(post => {
            this.FitlyFirebase.database()
              .ref(`/userPosts/${this.uID}/${postKey}`)
              .set({ timestamp: Firebase.database.ServerValue.TIMESTAMP });
            let updateObj = {
              type: 'post',
              contentID: postKey,
              contentlink: `/posts/${postKey}`,
              ownerID: this.uID,
              ownerName: `${this.user.public.first_name} ${
                this.user.public.last_name
              }`,
              isActive: true,
              noPhotos,
              inactiveOrNoPhotos: noPhotos,
              ownerPicture: this.user.public.picture,
              contentTitle: draftState.title,
              taggedUsers: draftState.friends || [],
              contentSnipet: draftState.content.slice(0, 200),
              description: draftState.category.toLowerCase(),
              timestamp: Firebase.database.ServerValue.TIMESTAMP
            };
            const photos = photoRefObject ? photoRefObject : {};
            if (notifImage) updateObj = { ...updateObj, photos, notifImage };
            if (this.post)
              UpdateUserUpdateToDB(updateObj, this.postID, this.uID);
            else saveUpdateToDB(updateObj, this.uID);
            //send Notification to tagged users
            if (draftState.friends) {
              draftState.friends.forEach(taggedFriend => {
                const updateNotifObj = {
                  type: 'tag',
                  ownerID: this.uID,
                  ownerName:
                    this.user.public.first_name +
                    ' ' +
                    this.user.public.last_name,
                  ownerPicture: this.user.public.picture,
                  // tagID: taggedFriend.userID,
                  isHidden: false,
                  contentID: postKey,
                  timestamp: Firebase.database.ServerValue.TIMESTAMP
                };
                this.FitlyFirebase.database()
                  .ref('/otherNotifications/' + taggedFriend.userID)
                  .push(updateNotifObj);
              });
            }
            this.setState({ loading: false }, () => {
              if (
                !this.user.public.profileProgress ||
                !this.user.public.profileProgress.shareAPhoto
              ) {
                this.props.FitlyFirebase.database()
                  .ref(`users/${this.props.uID}/public/profileProgress`)
                  .update({ shareAPhoto: true });
                this.props.FitlyFirebase.database()
                  .ref(`users/${this.props.uID}`)
                  .on('value', snap =>
                    this.props.action.storeUserProfile(snap.val())
                  );
              }
              this.props.navigation.goBack();
            });
          });
      } catch (error) {
        this.setState({ loading: false });
        console.log('create post error', error);
      }
    })();
  }

  _checkIncomplete(draftState) {
    let error;
    if (!draftState.title.length) {
      error = 'missing title';
    } else if (!draftState.startDate.date) {
      error = 'missing startDate';
    } else if (!draftState.endDate.date) {
      error = 'missing endDate';
    } else if (!draftState.location.address) {
      error = 'missing address';
    } else {
      return true;
    }
    Alert.alert('information missing', error);
    return false;
  }

  _getImageFromCam() {
    const draftState = this.props.drafts[
      this.props.navigation.state.params.draftRef
    ];
    getImageFromCam(image => {
      const newPhotos = draftState.photos;
      newPhotos.push(image);
      this.setDraftState({ photos: newPhotos });
    });
  }

  _getImageFromLib() {
    const draftState = this.props.drafts[
      this.props.navigation.state.params.draftRef
    ];
    getImageFromLib(images => {
      const newPhotos = draftState.photos.concat(images);
      this.setDraftState({ photos: newPhotos });
    });
  }

  _renderHeader() {
    const draftState = this.props.drafts[
      this.props.navigation.state.params.draftRef
    ];
    return (
      <HeaderInView
        leftElement={{ icon: 'ios-arrow-round-back-outline' }}
        rightElement={{ text: 'Publish' }}
        title={'Create Post'}
        _onPressRight={() => this._savePostToDB()}
        _onPressLeft={() => {
          this.props.navigation.goBack();
        }}
      />
    );
  }

  _renderThumbnails(photos) {
    if (photos.length == 1)
      return (
        <TouchableOpacity
          onPress={() =>
            this.setState({ modalVisible: true, contentType: 'default' })
          }
        >
          <Image
            style={{ height: height / 2.3, width }}
            source={{
              uri: photos[0].path ? photos[0].path : photos[0].link,
              isStatic: true
            }}
          />
        </TouchableOpacity>
      );
    else if (photos.length > 1) {
      return (
        <View style={{ height: height / 1.9, width }}>
          <ScrollView
            horizontal
            snapToInterval={width}
            decelerationRate={'fast'}
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            scrollEventThrottle={16}
            onScroll={({ nativeEvent }) => {
              this.setState({
                currentItem: Math.round(nativeEvent.contentOffset.x / width)
              });
            }}
          >
            {photos.map((photo, index) => (
              <TouchableOpacity
                onPress={() =>
                  this.setState({ modalVisible: true, contentType: 'default' })
                }
                key={`Photo${index + JSON.stringify(photo)}`}
              >
                <Image
                  style={{ height: height / 2, width }}
                  source={{
                    uri: photo.path ? photo.path : photo.link,
                    isStatic: true
                  }}
                />
              </TouchableOpacity>
            ))}
          </ScrollView>
          <View
            style={{
              height: 20,
              position: 'absolute',
              bottom: 20,
              width,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              zIndex: 999
            }}
          >
            <View
              style={{
                flexDirection: 'row'
              }}
            >
              {photos.map((item, index) => (
                <View
                  style={
                    {
                      width: 9,
                      height: 9,
                      borderRadius: 10,
                      marginRight: 10,
                      backgroundColor:
                        index == this.state.currentItem
                          ? 'rgba(255,255,255,1)'
                          : 'rgba(0,0,0,0.5)'
                    }
                    //   [
                    //   normalStyle,
                    //   index == this.state.currentItem ? currentItemStyle : {}
                    // ]
                  }
                  key={index}
                />
              ))}
            </View>
          </View>
        </View>
      );
    }
    return null;
  }

  _renderImgModal(draftState) {
    const removeImg = index => {
      const newPhotos = draftState.photos.slice();
      newPhotos.splice(index, 1);
      this.setDraftState({ photos: newPhotos });
    };

    return (
      <ImageEditModal
        draftState={draftState}
        visible={this.state.modalVisible}
        onBack={() =>
          this.setState({ modalVisible: false, contentType: 'light-content' })
        }
        onRequestClose={() => this.setState({ modalVisible: false })}
        getImageFromLib={() => this._getImageFromLib()}
        getImageFromCam={() => this._getImageFromCam()}
        onRemoveImage={index => removeImg(index)}
        onCaptionChange={(text, index) => {
          const newPhotos = draftState.photos.slice();
          newPhotos[index].description = text;
          this.setDraftState({ photos: newPhotos });
        }}
        onTagChange={(tags, index) => {
          const newPhotos = draftState.photos.slice();
          newPhotos[index].tags = tags;
          this.setDraftState({ photos: newPhotos });
        }}
      />
    );
  }

  _renderPhotoSection(draftState, renderThumnails) {
    let thumbnails;
    if (renderThumnails) {
      thumbnails = this._renderThumbnails(draftState.photos);
    }
    return (
      <View style={[composeStyle.photosSection, { zIndex: 99 }]}>
        {thumbnails}
        <View
          style={{
            flexDirection: 'row',
            width: width - 32,
            justifyContent: 'center',
            marginHorizontal: 16
          }}
        >
          <TouchableOpacity
            style={[
              composeStyle.photoThumbnail,
              {
                borderColor: '#ff00c9',
                borderBottomWidth: 1,
                borderTopWidth: 1
              }
            ]}
            onPress={() => this._getImageFromLib()}
          >
            <Icon name="ios-image-outline" size={30} color="#ff00c9" />
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              composeStyle.photoThumbnail,
              {
                borderColor: '#f68f26',
                borderBottomWidth: 1,
                borderTopWidth: 1
              }
            ]}
            onPress={() => this._getImageFromCam()}
          >
            <Icon name="ios-camera-outline" size={30} color="#f68f26" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  _textChange(text) {
    this.setState({ text });
    clearTimeout(this.typingTimer);
    this.setState({
      searchResults: []
    });
    if (this.state.text.length) {
      this.typingTimer = setTimeout(() => {
        this._getUserSearch(this.state.text);
      }, this.doneTypingInterval);
    } else {
      this.setState({ searchResults: [] });
    }
  }

  _getUserSearch(text) {
    const draftState = this.props.drafts[
      this.props.navigation.state.params.draftRef
    ];
    this.userQuery
      .searchByInput('full_name', text)
      .then(results => {
        let users = Array.from(results).map(u => {
          let id = { userID: u._id };
          return Object.assign({}, u._source, id);
        });
        console.log(users, 'users');
        this.setState({
          searchResults: users
        });
      })
      .catch(error => {
        this.setState({
          searchResults: []
        });
        console.log(error);
      });
  }
  _onPressTagFriend(userID, name, pic, account) {
    this.setState({ text: '' });
    const draftState = this.props.drafts[
      this.props.navigation.state.params.draftRef
    ];
    let taggedFriends = draftState.friends ? draftState.friends.slice() : [];
    let friendsDetails = Object.assign({}, this.state.friendsDetails);
    let index = taggedFriends.findIndex(item => item.userID === userID);
    if (index !== -1) {
      taggedFriends.splice(index, 1);
      delete friendsDetails[userID];
    } else {
      taggedFriends.push({ userID, name });
      friendsDetails[userID] = { userID, name, pic, account };
    }
    this.setDraftState({ friends: taggedFriends });
    this.setState({
      friendsDetails: friendsDetails
    });
  }
  render() {
    const draftState = this.props.drafts[
      this.props.navigation.state.params.draftRef
    ];
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <StatusBar barStyle={this.state.contentType} />
        <Spinner
          visible={this.state.loading}
          // textContent={'creating post...'}
          textStyle={{ color: '#FFF' }}
        />
        {this._renderImgModal(draftState)}
        {this._renderHeader()}
        <KeyboardAwareScrollView
          keyboardDismissMode={isAndroid ? 'none' : 'on-drag'}
          contentContainerStyle={composeStyle.scrollContentContainer}
        >
          {this.state.currentCategory !== 3 && (
            <View style={composeStyle.inputBox}>
              <TextInput
                underlineColorAndroid={'transparent'}
                autoCapitalize={'sentences'}
                returnKeyType="done"
                maxLength={30}
                clearButtonMode="always"
                onChangeText={text => this.setDraftState({ title: text })}
                style={[
                  composeStyle.input,
                  { fontWeight: '300', minHeight: 60 }
                ]}
                value={draftState.title}
                placeholder={
                  postCategories[this.state.currentCategory].includes('Meal')
                    ? 'Title of the dish'
                    : 'Title of the event'
                }
                placeholderTextColor="#ccc"
              />
            </View>
          )}
          <View style={composeStyle.inputBox}>
            <AutoExpandingTextInput
              clearButtonMode="always"
              onChangeText={text => this.setDraftState({ content: text })}
              style={[composeStyle.input, { fontSize: 16 }]}
              multiline
              dynamicValue={draftState.content}
              placeholder="Enter description here"
              placeholderTextColor="#ccc"
            />
          </View>
          {this.state.currentCategory !== 3 && (
            <View style={composeStyle.inputBox}>
              <AutoExpandingTextInput
                clearButtonMode="always"
                onChangeText={text => {
                  this.setDraftState({ extraContent: text });
                }}
                dynamicValue={draftState.extraContent}
                style={[composeStyle.input, { fontSize: 16 }]}
                multiline
                placeholder={
                  postCategories[this.state.currentCategory].includes('Meal')
                    ? 'Ingredients required'
                    : postCategories[this.state.currentCategory].includes(
                        'Workout'
                      )
                      ? 'Equipment needed'
                      : 'Additional details'
                }
                placeholderTextColor="#ccc"
              />
            </View>
          )}

          <View
            style={{
              width,
              flexDirection: 'row',
              flexWrap: 'wrap',
              flexGrow: 1
            }}
          >
            {Object.values(this.state.friendsDetails).map(friend => (
              <View
                style={{
                  paddingHorizontal: 5,
                  margin: 5,
                  height: 25,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  borderRadius: 10,
                  backgroundColor: 'rgba(100, 170, 185, 0.22)'
                }}
                key={JSON.stringify(friend)}
              >
                <Text>{friend.name}</Text>
                <TouchableOpacity
                  onPress={() =>
                    this._onPressTagFriend(
                      friend.userID,
                      friend.name,
                      friend.pic,
                      friend.account
                    )
                  }
                  style={{ marginLeft: 10 }}
                >
                  <Text>x</Text>
                </TouchableOpacity>
              </View>
            ))}
          </View>
          <View
            style={[
              composeStyle.inputBox,
              { flexDirection: 'row', paddingHorizontal: 16 }
            ]}
            onLayout={({ nativeEvent }) => {
              this.setState({
                posOfSearchUsers:
                  nativeEvent.layout.y + nativeEvent.layout.height + 50
              });
            }}
          >
            <Text style={[composeStyle.hashTag, { alignSelf: 'center' }]}>
              @
            </Text>
            <TextInput
              autoCapitalize={'words'}
              placeholder={'Tag your friends'}
              value={this.state.text}
              onChangeText={text => this._textChange(text)}
              style={[composeStyle.input, { fontSize: 16, width, flexGrow: 1 }]}
            />
          </View>

          {this._renderPhotoSection(draftState, true)}
          <View style={{ height: 100, marginTop: 50 }}>
            <Text style={{ alignSelf: 'center', textAlign: 'center' }}>
              Please select the category of this post:
              {'\n (swipe left and right to change)'}
            </Text>
            <ScrollView
              horizontal
              ref="scrollView"
              scrollEventThrottle={16}
              decelerationRate={'normal'}
              showsHorizontalScrollIndicator={false}
              pagingEnabled
              onScroll={({ nativeEvent }) => {
                let offset = nativeEvent.contentOffset.x;
                if (offset % width === 0) {
                  const divisor = Math.round(offset / width);
                  this.setDraftState({ category: postCategories[divisor] });
                  this.setState({ currentCategory: divisor }, () => {
                    if (this.state.currentCategory === 0) {
                      this.refs.scrollView.scrollTo({
                        x: width * 3,
                        y: 0,
                        animated: false
                      });
                    } else if (this.state.currentCategory === 4) {
                      this.refs.scrollView.scrollTo({
                        x: width,
                        y: 0,
                        animated: false
                      });
                    }
                  });
                }
              }}
              style={{ height: 100 }}
            >
              {postCategories.map((item, index) => (
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: 22,
                    fontWeight: 'bold',
                    color: FitlyBlue,
                    width,
                    textAlign: 'center'
                  }}
                >
                  {item}
                </Text>
              ))}
            </ScrollView>

            <TouchableOpacity
              style={{
                position: 'absolute',
                left: 16,
                bottom: 0,
                height: 50,
                width: 50,
                backgroundColor: 'transparent'
              }}
              onPress={() => {
                if (this.state.currentCategory !== 0)
                  this.refs.scrollView.scrollTo({
                    x: (this.state.currentCategory - 1) * width,
                    animated: true
                  });
              }}
            >
              <Icon name="ios-arrow-back" size={30} color={'#ccc'} />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                position: 'absolute',
                right: 16,
                bottom: 0,
                height: 50,
                width: 50,
                backgroundColor: 'transparent',
                alignItems: 'flex-end'
              }}
              onPress={() => {
                if (this.state.currentCategory !== 4)
                  this.refs.scrollView.scrollTo({
                    x: (this.state.currentCategory + 1) * width,
                    animated: true
                  });
              }}
            >
              <Icon name="ios-arrow-forward" size={30} color={'#ccc'} />
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <View
          style={{
            height: 300,
            overflow: 'hidden',
            width: width - 100,
            borderWidth: 0.5,
            borderColor: '#ccc',
            elevation: 10,
            shadowColor: '#000',
            shadowOffset: { width: 2, height: 2 },
            shadowOpacity: 0.1,
            shadowRadius: 3,
            position: 'absolute',
            top:
              this.state.text.length && this.state.searchResults.length > 0
                ? this.state.posOfSearchUsers
                : -300,
            left: 50
          }}
        >
          <ScrollView>
            <UserListView
              {...this.props}
              userSearch={true}
              data={this.state.searchResults.filter(
                user =>
                  !Object.keys(this.state.friendsDetails).includes(user.userID)
              )}
              // noHeader={true}
              onPress={this._onPressTagFriend.bind(this)}
              includes={draftState.friends || []}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}

ComposePost.defaultProps = {
  draftRef: randomString()
};

const mapStateToProps = function(state) {
  return {
    drafts: state.drafts.drafts,
    user: state.user.user,
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        storeUserProfile
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ pop, push, resetTo }, dispatch),
    draftsAction: bindActionCreators({ save, clear }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ComposePost);
