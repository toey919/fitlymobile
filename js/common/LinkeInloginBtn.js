/**
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  AsyncStorage
} from 'react-native';
import { loginStyles, FitlyBlue } from '../styles/styles.js';
import Icon from 'react-native-vector-icons/Ionicons';

import LinkedinLogin from 'react-native-linkedin-login';
import {
  setFirebaseUID,
  setSignUpMethod,
  printAuthError,
  clearAuthError
} from '../actions/auth.js';
import { storeUserProfile } from '../actions/user.js';
import { setBlocks } from '../actions/blocks.js';
import { setLoadingState, setSearchLocation } from '../actions/app.js';
import { resetTo } from '../actions/navigation.js';
import { connect } from 'react-redux';
import Firebase from 'firebase';
import { bindActionCreators } from 'redux';
import { updateCurrentLocationInDB } from '../library/firebaseHelpers.js';
import {
  getCurrentPlace,
  getPlaceByName
} from '../library/asyncGeolocation.js';

import { NavigationActions } from 'react-navigation';
const FB_PHOTO_WIDTH = 300;

class FBloginBtn extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.state = {
      user: null
    };
  }
  componentWillMount() {
    LinkedinLogin.init(['r_emailaddress', 'r_basicprofile']);
    this.getUserSession();
  }
  getUserSession() {
    AsyncStorage.getItem('user', (err, result) => {
      if (result) {
        const user = JSON.parse(result);

        // set the api session if found
        LinkedinLogin.setSession(user.accessToken, user.expiresOn);

        this.setState({
          user
        });

        console.log('user', user);
      }
    });
  }

  login() {
    LinkedinLogin.login()
      .then(user => {
        console.log('User logged in: ', user);
        this.setState({ user });
        AsyncStorage.setItem('user', JSON.stringify(user), () => {
          this.getUserProfile();
        });
      })
      .catch(e => {
        console.log('Error XXXXX', e.description);
      });

    return true;
  }

  logout() {
    LinkedinLogin.logout();
    console.log('user logged out');

    // AsyncStorage.removeItem('user');
    this.setState({ user: null });
  }

  getUserProfile(user) {
    LinkedinLogin.getProfile()
      .then(data => {
        console.log('received profile', data);
        const userdata = Object.assign({}, this.state.user, data);

        console.log('user: ', userdata);
        this.setState({ user: userdata });

        AsyncStorage.setItem('user', JSON.stringify(userdata), () => {
          this.getUserProfileImage();
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  getUserProfileImage() {
    LinkedinLogin.getProfileImages()
      .then(images => {
        console.log('received profile image', images);

        const userdata = Object.assign({}, this.state.user, { images });

        AsyncStorage.setItem('user', JSON.stringify(userdata), () => {
          this.setState({ user: userdata });
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  //TODO error reporting for login error
  _handleFBLogin() {
    const { FitlyFirebase, navigation, action } = this.props;
    const resetToInterests = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'InterestsView' })]
    });

    const resetToTabNavigation = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'TabNavigator' })]
    });
  }

  render() {
    return (
      <TouchableOpacity
        style={[
          loginStyles.FBbtn,
          this.props.style,
          {
            flexDirection: 'row',
            alignItems: 'center',
            width: 260,
            height: 50,
            justifyContent: 'center',
            borderRadius: 30,
            backgroundColor: '#007bb6'
          }
        ]}
        onPress={() => this.login()}
      >
        <View
          style={{
            backgroundColor: '#fff',
            borderRadius: 5,
            top: -2
          }}
        >
          <Icon
            name={'logo-linkedin'}
            color={'#007bb6'}
            size={42}
            style={{
              backgroundColor: 'transparent',
              height: 29,
              paddingTop: -5.5
            }}
          />
        </View>
        <View
          style={{
            paddingLeft: 10,
            borderLeftColor: '#ccc',
            borderLeftWidth: 1,
            height: 30,
            width: 190,
            justifyContent: 'center'
          }}
        >
          <Text style={[loginStyles.btnText, { color: '#fff' }]}>
            Continue with LinkedIn
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    loading: state.app.loading
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        setFirebaseUID,
        setSignUpMethod,
        printAuthError,
        setLoadingState,
        storeUserProfile,
        clearAuthError,
        setSearchLocation,
        setBlocks
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ resetTo }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FBloginBtn);
