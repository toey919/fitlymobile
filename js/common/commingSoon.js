import React, { Component } from 'react';
import {
  postStyle,
  feedEntryStyle,
  composeStyle,
  headerStyle,
  profileStyle
} from '../styles/styles';
import {
  Modal,
  View,
  TextInput,
  Text,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions
} from 'react-native';
// import AutoExpandingTextInput from '../../common/AutoExpandingTextInput.js';
import Icon from 'react-native-vector-icons/Ionicons';
import { push, pop, resetTo } from '../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  convertFBObjToArray,
  saveUpdateToDB,
  turnOnCommentListener,
  turnOffCommentListener,
  FitlyFirebase
} from '../library/firebaseHelpers.js';
import TimeAgo from 'react-native-timeago';
import Firebase from 'firebase';
import CommentsModal from './comment/CommentsModal.js';
import SocialBtns from './SocialBtns.js';
import Author from './Author.js';
import HeaderInView from '../header/HeaderInView.js';
import Spinner from 'react-native-loading-spinner-overlay';
import NewCommentIcon from './NewCommentIcon';

class ComingSoon extends Component {
  componentWillMount() {
    console.log(
      this.props.user.private.email,
      this.props.user.public.userCurrentLocation,
      'Save the data here'
    );
    // this.props.FitlyFirebase.database()
    //   .ref()
    //   .child('trainerRequirements')
    //   .push({
    //     user: this.props.user.private.email,
    //     location: this.props.user.public.userCurrentLocation
    //   }); // when trying to save the data for analytics permission denied
  }
  render() {
    return (
      <Text
        style={{
          color: '#bbb',
          fontWeight: '200',
          fontSize: 20,
          textAlign: 'center'
        }}
      >
        Coming Soon!
        {'\n'}
        Thanks for being a part of the Beta!
      </Text>
    );
  }
}

ComingSoon.propTypes = {
  onPress: React.PropTypes.func
};

const mapStateToProps = state => ({
  user: state.user.user,
  uID: state.auth.uID,
  FitlyFirebase: state.app.FitlyFirebase
});

export default connect(mapStateToProps, null)(ComingSoon);
