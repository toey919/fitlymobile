import React, { Component } from 'react';
import {
  TouchableHighlight,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Platform,
  Image
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { optionStyle, container, FitlyBlue } from '../../styles/styles.js';
import Icon from 'react-native-vector-icons/Ionicons';
import { push, resetTo } from '../../actions/navigation.js';
import { save, clear } from '../../actions/drafts.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SearchBar from 'react-native-search-box';
import Contacts from 'react-native-contacts';
import { Entry, Separator } from '../PressableEntry.js';
import Query from '../../library/Query';
import UserListView from '../UserListView';
import HeaderInView from '../../header/HeaderInView.js';
import { UpdateUserUpdateToDB } from '../../library/firebaseHelpers.js';

const isAndroid = Platform.OS === 'android';

class SelectInvitesScene extends Component {
  constructor(props) {
    super(props);
    this.event = this.props.navigation.state.params.event;
    this.eventID = this.props.navigation.state.params.eventID;
    this.organizers = this.event.organizersArray.slice();
    this.uID = this.props.navigation.state.params.uID;
    this.organizers.splice(this.organizers.indexOf(this.uID), 1);
    this.adminDetails = {};
    this.FitlyFirebase = this.props.navigation.state.params.firebase;
    this.state = {
      loadingContacts: true,
      contacts: [],
      searchResults: [],
      searchMode: false,
      invitedAdmins: this.organizers,
      adminDetails: this.adminDetails || {}
    };
    this.userQuery = new Query('user', this.props.uID);
    this.doneTypingInterval = 500;
    this.typingTimer;
  }

  componentDidMount() {
    this.refs.searchBar.focus();
    Contacts.getAll((error, contacts) => {
      if (error && error.type === 'permissionDenied') {
        console.error(error);
      } else {
        this.setState({
          loadingContacts: false,
          contacts: contacts
        });
      }
    });
  }

  _renderEntries(boolean) {
    const icon = boolean ? 'ios-remove-outline' : 'ios-add-outline';
    const adminLength = this.state.invitedAdmins.length;
    return (
      <View>
        {!adminLength ? (
          <View
            style={[
              optionStyle.entry,
              { minHeight: 40, justifyContent: 'center' }
            ]}
          >
            <Text style={{ textAlign: 'center', color: 'grey' }}>empty</Text>
          </View>
        ) : null}

        {Object.keys(this.state.adminDetails).map((admin, i) => {
          let { userID } = this.state.adminDetails[admin];
          return (
            <Entry
              key={i + admin}
              text={admin}
              icon={icon}
              onPress={() => this._uninviteAdmin(admin, userID)}
            />
          );
        })}
      </View>
    );
  }

  _onPressUserInvite(userID, name, pic, account) {
    // const { otherOrganizers } = this.props.drafts[this.draftRef];
    let adminCopy = Array.from(this.state.invitedAdmins);
    adminCopy.push(userID);
    let updatedAdmins = Object.assign({}, this.state.adminDetails);
    if (!updatedAdmins[name]) {
      updatedAdmins[name] = { userID: userID, userPic: pic, account: account };
    } else {
      delete updatedAdmins[name];
    }
    this.setState(
      {
        invitedAdmins: adminCopy,
        adminDetails: updatedAdmins
      },
      this.updateAdminInvite(userID)
    );
  }

  _uninviteAdmin(name, userID) {
    const { otherOrganizers } = this.props.drafts[this.draftRef];
    let adminCopy = Array.from(otherOrganizers);
    let i = adminCopy.indexOf(userID);
    adminCopy = [...adminCopy.slice(0, i), ...adminCopy.slice(i + 1)];
    let updatedAdmins = Object.assign({}, this.state.adminDetails);
    delete updatedAdmins[name];
    this.setState(
      {
        invitedAdmins: adminCopy,
        adminDetails: updatedAdmins
      },
      this.updateAdminInvite(userID)
    );
  }

  updateAdminInvite(userID) {
    let updateResults = Array.from(this.state.searchResults).map(user => {
      if (user.userID === userID) {
        let newValue = { value: !user.value };
        return Object.assign({}, user, newValue);
      } else {
        return user;
      }
    });
    this.setState({
      searchResults: updateResults
    });
  }

  _textChange(text) {
    clearTimeout(this.typingTimer);
    this.setState({
      searchResults: []
    });
    if (text.length) {
      this.typingTimer = setTimeout(() => {
        this._getUserSearch(text);
      }, this.doneTypingInterval);
    } else {
      this.setState({ searchResults: [] });
    }
  }

  _getUserSearch(text) {
    this.userQuery
      .searchByInput('full_name', text)
      .then(results => {
        let users = Array.from(results).map(u => {
          let id = { userID: u._id };
          let invited = { value: this.state.invitedAdmins[u._id] };
          return Object.assign({}, u._source, id, invited);
        });
        this.setState({
          searchResults: users
        });
      })
      .catch(error => {
        this.setState({
          searchResults: []
        });
      });
  }
  _renderInvitedAdmins() {
    return (
      <ScrollView
        horizontal
        style={{
          height: 90,
          backgroundColor: FitlyBlue,
          flexDirection: 'row'
        }}
      >
        {this.state.invitedAdmins.map((item, index) => {
          let user = Array.from(this.state.searchResults).find(
            user => user.userID === item
          );
          if (user) {
            return (
              <View
                style={{
                  flex: 1,
                  alignSelf: 'center',
                  marginRight: 5,
                  marginBottom: 2,
                  paddingTop: 2
                }}
                key={JSON.stringify(item) + index}
              >
                <TouchableOpacity
                  style={{
                    width: 20,
                    height: 20,
                    borderRadius: 10,
                    backgroundColor: '#fff',
                    alignItems: 'center',
                    position: 'absolute',
                    right: 0,
                    zIndex: 99
                  }}
                  onPress={() => {
                    let currentAdmin = Object.keys(
                      this.state.adminDetails
                    ).find(
                      admin => this.state.adminDetails[admin].userID === item
                    );
                    this._uninviteAdmin(currentAdmin, item);
                  }}
                >
                  <Icon
                    name={'ios-close'}
                    style={{
                      fontSize: 20,
                      color: FitlyBlue,
                      backgroundColor: 'transparent'
                    }}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    alignItems: 'center',
                    width: 80,
                    height: 80
                  }}
                >
                  <Image
                    style={{
                      height: 50,
                      width: 50,
                      borderRadius: 25,
                      borderColor: '#fff',
                      borderWidth: 1
                    }}
                    // style={UserStyle[`${user.account}Img`]}
                    source={{ uri: user.picture }}
                    defaultSource={require('../../../img/default-user-image.png')}
                  />
                  <Text
                    style={{
                      color: '#fff',
                      fontSize: 12,
                      textAlign: 'center'
                    }}
                    numberOfLines={2}
                    ellipsizeMode={'tail'}
                  >{`${user.first_name}\n ${user.last_name}`}</Text>
                </View>
              </View>
            );
          }
        })}
      </ScrollView>
    );
  }
  async _handleAddAdmins() {
    try {
      const updatedOrganizers = { ...this.event.organizers };
      this.state.invitedAdmins.forEach(
        admin => (updatedOrganizers[admin] = true)
      );
      const updateVal = [
        ...this.event.organizersArray,
        ...this.state.invitedAdmins
      ];
      this.setState({ loading: true });
      await this.FitlyFirebase.database()
        .ref('events/' + this.eventID + '/organizersArray')
        .set(updateVal);
      await this.FitlyFirebase.database()
        .ref('events/' + this.eventID + '/organizers')
        .set(updatedOrganizers);
      await UpdateUserUpdateToDB(
        {
          organizersArray: updateVal,
          organizers: updatedOrganizers
        },
        this.eventID,
        this.uID
      );
      this.setState({ loading: false });
      this.props.navigation.goBack();
    } catch (e) {
      this.setState({ loading: false });
      console.log(e);
    }
  }
  _renderHeaderInvitation(leftPress) {
    return (
      <View
        style={{
          height: 80,
          backgroundColor: FitlyBlue,
          flexDirection: 'row',
          alignItems: 'center'
        }}
      >
        <View style={{ flex: 1, alignItems: 'center' }}>
          <TouchableOpacity
            style={{ flex: 1, justifyContent: 'center' }}
            onPress={() => this._handleAddAdmins()}
          >
            <Text style={{ fontSize: 16, color: '#fff' }}>Done</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 3, alignItems: 'center' }}>
          <Text style={{ fontSize: 20, color: '#fff' }}>Add Organizers</Text>
        </View>
        <View style={{ flex: 1 }} />
      </View>
    );
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        {this._renderHeaderInvitation()}
        {this.state.loading && (
          <Spinner
            visible={this.state.loading}
            textContent={'Please wait...'}
            textStyle={{ color: FitlyBlue }}
          />
        )}
        <SearchBar
          ref="searchBar"
          placeholder="Search Users"
          // showsCancelButton={false}
          onFocus={() => this.setState({ searchMode: true })}
          // onCancel={() => this.setState({ searchMode: false })}
          onChangeText={this._textChange.bind(this)}
          backgroundColor={'grey'}
        />
        <ScrollView
          keyboardDismissMode={isAndroid ? 'none' : 'interactive'}
          style={{ flex: 1, backgroundColor: 'white' }}
        >
          {this.state.searchMode ? (
            <View style={{ flex: 1 }}>
              {this._renderInvitedAdmins()}
              <UserListView
                {...this.props}
                userSearch={true}
                data={this.state.searchResults}
                noHeader={true}
                onPress={this._onPressUserInvite.bind(this)}
                includes={this.state.adminDetails}
              />
            </View>
          ) : (
            <View style={{ flex: 0 }}>
              <Separator text="invited" />
              {this._renderEntries(true)}
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    drafts: state.drafts.drafts,
    user: state.user.user,
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = dispatch => {
  return {
    exnavigation: bindActionCreators({ push, resetTo }, dispatch),
    draftsAction: bindActionCreators({ save, clear }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectInvitesScene);
