import React, { Component } from 'react';
import { View, Text, Alert } from 'react-native';
import HeaderInView from '../../header/HeaderInView.js';
import { Entry, Separator } from '../PressableEntry.js';
import {
  UpdateUserUpdateToDB,
  uploadPhoto
} from '../../library/firebaseHelpers.js';
import Spinner from 'react-native-loading-spinner-overlay';
import ActionSheet from 'react-native-actionsheet';
const CANCEL_INDEX = 2;
const DESTRUCTIVE_INDEX = 4;
const options = ['Gallery...', 'Camera...', 'Cancel'];
const title = 'Upload from...';
import { NavigationActions } from 'react-navigation';
import {
  selectPictureCropper,
  selectPictureCropperCam
} from '../../library/pictureHelper.js';
class EventOptionsMenu extends Component {
  ActionSheet;
  constructor(props) {
    super(props);
    this.state = {
      eventStatus: null,
      loading: false
    };
    this.eventID = this.props.navigation.state.params.eventID;
    this.uID = this.props.navigation.state.params.uID;
    this.rootNav = this.props.navigation.state.params.rootNav;
    this.event = this.props.navigation.state.params.event;
    this.fromProfile = this.props.navigation.state.params.fromProfile;
  }
  _renderAcitionSheet() {
    return (
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        title={title}
        options={options}
        cancelButtonIndex={CANCEL_INDEX}
        destructiveButtonIndex={DESTRUCTIVE_INDEX}
        onPress={i => {
          if (i == 0) {
            this._updateProfileGalleryPic();
          } else if (i == 1) {
            this._updateProfileCameraPic();
          }
        }}
      />
    );
  }
  componentDidMount() {
    this.props.navigation.state.params.FitlyFirebase.database()
      .ref('events/' + this.eventID + '/status')
      .on('value', eventStatusSnap => {
        this.setState({ eventStatus: eventStatusSnap.val() });
      });
  }

  componentWillUnmount() {
    this.props.navigation.state.params.FitlyFirebase.database()
      .ref('events/' + this.props.navigation.state.params.eventID + '/status')
      .off('value');
  }
  _updateProfileGalleryPic = () => {
    selectPictureCropper(false)
      .then(picture => this._storePhoto(picture.uri))
      //.then(picture => console.log(picture))
      .catch(err => console.log(err));
  };
  _updateProfileCameraPic = () => {
    selectPictureCropperCam(false)
      .then(picture => this._storePhoto(picture.uri))
      //.then(picture => console.log(picture))
      .catch(err => console.log(err));
  };
  _storePhoto = uri => {
    this.setState({ loading: true });
    uploadPhoto(`events/${this.eventID}/backgroundImage/`, uri)
      .then(async link => {
        await this.props.navigation.state.params.FitlyFirebase.database()
          .ref('events/' + this.eventID + '/backgroundImage')
          .set(link);
        await UpdateUserUpdateToDB(
          {
            backgroundImage: link
          },
          this.eventID,
          this.uID
        );
        this.setState({ loading: false });
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log('upload profile pic', error);
      });
  };

  _renderHeader() {
    return (
      <HeaderInView
        leftElement={{ icon: 'ios-arrow-round-back-outline' }}
        title="Event Options"
        _onPressLeft={this.props.navigation.goBack}
      />
    );
  }
  _onChangeImagePress() {
    this.ActionSheet.show();
  }
  _onDeletePress() {
    Alert.alert(
      'WARNING',
      `You are about to delete this event. It will be gone forever`,
      [
        {
          text: 'Delete Event',
          onPress: async () => {
            this.setState({ loading: true });
            await this.props.navigation.state.params.FitlyFirebase.database()
              .ref('events/' + this.eventID + '/isActive')
              .set(false);
            await UpdateUserUpdateToDB(
              {
                isActive: false
              },
              this.eventID,
              this.uID
            );
            this.setState({ loading: false });
            (this.fromProfile ? this.rootNav : this.props.navigation).dispatch(
              NavigationActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'TabNavigator',
                    params: { newUser: false }
                  })
                ]
              })
            );
          },
          style: 'cancel'
        },
        {
          text: 'Abort',
          onPress: () => {
            return;
          }
        }
      ]
    );
  }

  _onPress() {
    let text, status;
    if (this.state.eventStatus === 'normal') {
      text = 'Cancel Event';
      status = 'cancelled';
    } else {
      text = 'Reactivate Event';
      status = 'normal';
    }
    Alert.alert(
      'WARNING',
      `You are about to ${
        status === 'cancelled' ? 'cancel' : 're-activate'
      } this event`,
      [
        {
          text: text,
          onPress: async () => {
            this.setState({ loading: true });
            await this.props.navigation.state.params.FitlyFirebase.database()
              .ref('events/' + this.eventID + '/status')
              .set(status);

            await UpdateUserUpdateToDB(
              {
                status
              },
              this.eventID,
              this.uID
            );
            this.setState({ loading: false });
            (this.fromProfile ? this.rootNav : this.props.navigation).dispatch(
              NavigationActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'TabNavigator',
                    params: { newUser: false }
                  })
                ]
              })
            );
          },
          style: 'cancel'
        },
        {
          text: 'Abort',
          onPress: () => {
            return;
          }
        }
      ]
    );
  }
  _duplicateEvent() {
    this.props.navigation.navigate('CreateActivityScene', {
      event: this.event
    });
  }
  render() {
    return (
      <View>
        {this._renderHeader()}
        {this.state.loading && (
          <Spinner
            visible={this.state.loading}
            textContent={'Please wait...'}
            textStyle={{ color: '#FFF' }}
          />
        )}
        {this._renderAcitionSheet()}
        <Separator
          text={
            this.state.eventStatus ? 'Event is ' + this.state.eventStatus : ''
          }
        />
        <Entry
          text={
            this.state.eventStatus === 'normal'
              ? 'Cancel Event'
              : 'Reactivate Event'
          }
          onPress={() => this._onPress()}
        />
        <Entry
          text={'Delete this event'}
          onPress={() => this._onDeletePress()}
        />
        <Entry
          text={'Change promo image'}
          onPress={() => this._onChangeImagePress()}
        />
        <Entry
          text={'Duplicate Event'}
          onPress={() => this._duplicateEvent()}
        />
      </View>
    );
  }
}

export default EventOptionsMenu;
