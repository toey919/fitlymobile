import React, { Component } from 'react';
import {
  eventStyle,
  postStyle,
  feedEntryStyle,
  composeStyle
} from '../../styles/styles.js';
import {
  View,
  TextInput,
  Text,
  StatusBar,
  Modal,
  ScrollView,
  Animated,
  Picker,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import { storeUserProfile } from '../../actions/user'; // import AutoExpandingTextInput from '../../common/AutoExpandingTextInput.js';
import Icon from 'react-native-vector-icons/Ionicons';
import { push, pop, resetTo } from '../../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
const { width } = Dimensions.get('window');
import {
  convertFBObjToArray,
  createContentListener,
  turnOffContentListner
} from '../../library/firebaseHelpers.js';
import TimeAgo from 'react-native-timeago';
import Firebase from 'firebase';
import CommentsModal from '../comment/CommentsModal.js';
import SocialBtns from '../SocialBtns.js';
import Author from '../Author.js';
import {
  getWeekdayMonthDay,
  getHrMinDuration,
  getDateStringByFormat
} from '../../library/convertTime.js';
import RenderUserBadges from '../RenderUserBadges.js';
import HeaderInView from '../../header/HeaderInView.js';
import Spinner from 'react-native-loading-spinner-overlay';
import AutoExpandingTextInput from '../AutoExpandingTextInput.js';
import _ from 'lodash';
//TODO: below two function will be refactored out
//check if event is expired/canceled/happening/going to happen
class EventScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event: null,
      commentLoading: true,
      modalVisible: false,
      members: [],
      menuOpen: false,
      editEventDetials: false,
      text: ''
    };
    this.FitlyFirebase = this.props.FitlyFirebase;
    this.database = this.FitlyFirebase.database();
    this.uID = this.props.uID;
    this.isAdmin = this.props.navigation.state.params.isAdmin;
    this.user = this.props.user;
    this.eventRef = this.database.ref(
      'events/' + this.props.navigation.state.params.eventID
    );
    this.eventMemberRef = this.database.ref(
      'eventMembers/' + 'going/' + this.props.navigation.state.params.eventID
    );
    this.attendStatusRef = this.database.ref(
      'eventMembers/' +
        'main/' +
        this.props.navigation.state.params.eventID +
        '/' +
        this.uID
    );
    this.pushToRoute = this.props.navigation.navigate.bind(this);
  }

  componentDidMount() {
    createContentListener(this.eventRef, eventObj => {
      this.setState(
        {
          event: eventObj
        },
        () => {
          this.setState({ text: this.state.event.details });
        }
      );
    })();
    if (!this.isAdmin) {
      const handleUpdates = snap => {
        let snapObj = snap.val();
        this.setState({ attendStatus: snapObj || {} }, () => {
          if (!_.isEmpty(this.state.attendStatus))
            Animated.parallel([
              Animated.timing(this.opacityGoing, {
                toValue: this.state.attendStatus.going ? 1 : 0,
                duration: 200
              }),
              Animated.timing(this.opacityInterested, {
                toValue: this.state.attendStatus.interested ? 1 : 0,
                duration: 200
              }),
              Animated.timing(this.opacityNotGoing, {
                toValue: this.state.attendStatus.notGoing ? 1 : 0,
                duration: 200
              }),
              Animated.timing(this.opacityMaybe, {
                toValue: this.state.attendStatus.maybe ? 1 : 0,
                duration: 200
              }),
              Animated.timing(this.transGoing, {
                toValue: 145 / 375 * width,
                duration: 200
              }),
              Animated.timing(this.transInterested, {
                toValue: 53 / 375 * width,
                duration: 200
              }),
              Animated.timing(this.transMaybe, {
                toValue: -135 / 375 * width,
                duration: 200
              }),
              Animated.timing(this.transNotGoing, {
                toValue: -38 / 375 * width,
                duration: 200
              })
            ]).start();
        });
      };
      this.attendStatusRef.on('value', handleUpdates);
    }
    createContentListener(this.eventMemberRef, membersObj => {
      this.setState({ members: membersObj ? Object.keys(membersObj) : [] });
    })();
  }

  componentWillUnmount() {
    turnOffContentListner(this.eventRef);
    turnOffContentListner(this.attendStatusRef);
  }

  // if admin: show edit btns, delete btn, invite btns, disable social btns
  // if not admin: no edit, delete btns, show going btns
  //if public: show all social btns
  //if private: like btn only
  _updateAttendStatus(status) {
    let mainPath =
      '/eventMembers/' +
      'main/' +
      this.props.navigation.state.params.eventID +
      '/' +
      this.uID;
    let interestedPath =
      '/eventMembers/' +
      'interested/' +
      this.props.navigation.state.params.eventID +
      '/' +
      this.uID;
    let goingPath =
      '/eventMembers/' +
      'going/' +
      this.props.navigation.state.params.eventID +
      '/' +
      this.uID;
    let notGoingPath =
      '/eventMembers/' +
      'notGoing/' +
      this.props.navigation.state.params.eventID +
      '/' +
      this.uID;
    let maybePath =
      '/eventMembers/' +
      'maybe/' +
      this.props.navigation.state.params.eventID +
      '/' +
      this.uID;
    let mainObj = {
      [mainPath]: {
        going: false,
        notGoing: false,
        maybe: false,
        interested: false
      }
    };
    let goingObj = { [goingPath]: null };
    let notGoingObj = { [notGoingPath]: null };
    let maybeObj = { [maybePath]: null };
    let interestedObj = { [interestedPath]: null };

    if (status === 'going') {
      this.attendStatusRef.once('value').then(snap => {
        const snapObj = snap.val();
        if (!snapObj || !snapObj.going) {
          this.eventRef
            .child('memberCount')
            .transaction(memberCount => memberCount + 1);
        }
      });
      if (
        !this.props.user.public.profileProgress ||
        !this.props.user.public.profileProgress.attendAnEvent
      ) {
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}/public/profileProgress`)
          .update({ attendAnEvent: true });
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}`)
          .on('value', snap => this.props.action.storeUserProfile(snap.val()));
      }
      mainObj = {
        [mainPath]: {
          going: true,
          notGoing: false,
          maybe: false,
          interested: false
        }
      };
      goingObj = { [goingPath]: true };
    } else if (status === 'notGoing') {
      this.attendStatusRef.once('value').then(snap => {
        if (snap.val().going) {
          this.eventRef
            .child('memberCount')
            .transaction(memberCount => memberCount - 1);
        }
      });
      mainObj = {
        [mainPath]: {
          going: false,
          notGoing: true,
          maybe: false,
          interested: false
        }
      };
      notGoingObj = { [notGoingPath]: true };
    } else if (status === 'maybe') {
      this.attendStatusRef.once('value').then(snap => {
        if (snap.val().going) {
          this.eventRef
            .child('memberCount')
            .transaction(memberCount => memberCount - 1);
        }
      });
      mainObj = {
        [mainPath]: {
          going: false,
          notGoing: false,
          maybe: true,
          interested: false
        }
      };
      maybeObj = { [maybePath]: true };
    } else if (status === 'interested') {
      this.attendStatusRef.once('value').then(snap => {
        if (snap.val().going) {
          this.eventRef
            .child('memberCount')
            .transaction(memberCount => memberCount - 1);
        }
      });
      mainObj = {
        [mainPath]: {
          going: false,
          notGoing: false,
          maybe: false,
          interested: true
        }
      };
      interestedObj = { [interestedPath]: true };
    }
    this.database.ref().update({
      ...mainObj,
      ...goingObj,
      ...notGoingObj,
      ...maybeObj,
      ...interestedObj
    });
  }
  opacityGoing = new Animated.Value(1);
  transGoing = new Animated.Value(0);
  opacityNotGoing = new Animated.Value(1);
  transNotGoing = new Animated.Value(0);
  opacityMaybe = new Animated.Value(1);
  transMaybe = new Animated.Value(0);
  opacityInterested = new Animated.Value(1);
  transInterested = new Animated.Value(0);
  _renderAttendBtns(event) {
    const { attendStatus } = this.state;
    let goingStyle, interestedStyle, notGoingStyle, maybeStyle;
    if (!attendStatus) {
      return null;
    }
    if (
      !attendStatus.going &&
      !attendStatus.interested &&
      !attendStatus.notGoing &&
      !attendStatus.maybe
    ) {
      goingStyle = eventStyle.btnFalse;
      interestedStyle = eventStyle.btnFalse;
      notGoingStyle = eventStyle.btnFalse;
      maybeStyle = eventStyle.btnFalse;
    } else {
      goingStyle = attendStatus.going
        ? eventStyle.btnTrue
        : eventStyle.btnFalse;
      interestedStyle = attendStatus.interested
        ? eventStyle.btnTrue
        : eventStyle.btnFalse;
      notGoingStyle = attendStatus.notGoing
        ? eventStyle.btnTrue
        : eventStyle.btnFalse;
      maybeStyle = attendStatus.maybe
        ? eventStyle.btnTrue
        : eventStyle.btnFalse;
    }

    return (
      <View>
        {this._renderCost(event.cost)}
        <View
          style={[
            eventStyle.attendStatusContainer,
            {
              borderBottomWidth: 0.5,
              borderColor: '#aaa',
              paddingBottom: 10,
              paddingTop: 10,
              marginTop: 10
            }
          ]}
        >
          <Animated.View
            style={{
              opacity: this.opacityGoing,
              transform: [{ translateX: this.transGoing }]
            }}
          >
            <TouchableOpacity
              style={goingStyle}
              onPress={() => this._updateAttendStatus('going')}
            >
              <Icon name="md-checkmark" color="green" size={48} />
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              opacity: this.opacityInterested,
              transform: [{ translateX: this.transInterested }]
            }}
          >
            <TouchableOpacity
              style={interestedStyle}
              onPress={() => this._updateAttendStatus('interested')}
            >
              <Icon name="md-star" color="yellow" size={48} />
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              opacity: this.opacityNotGoing,
              transform: [{ translateX: this.transNotGoing }]
            }}
          >
            <TouchableOpacity
              style={notGoingStyle}
              onPress={() => this._updateAttendStatus('notGoing')}
            >
              <Icon name="md-close" color="red" size={48} />
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              opacity: this.opacityMaybe,
              transform: [{ translateX: this.transMaybe }]
            }}
          >
            <TouchableOpacity
              style={maybeStyle}
              onPress={() => this._updateAttendStatus('maybe')}
            >
              <Icon name="md-help" color="white" size={48} />
            </TouchableOpacity>
          </Animated.View>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
              position: 'absolute',
              right: 10,
              top: 35
            }}
            onPress={() => {
              this.setState({ showStatusPicker: true });
            }}
          >
            <Icon name="ios-arrow-down" color="#ccc" size={24} />
          </TouchableOpacity>
        </View>
        <Text style={{ alignSelf: 'center', textAlign: 'center' }}>
          You are{' '}
          {attendStatus.going
            ? 'attending'
            : attendStatus.notGoing
              ? 'not attending'
              : attendStatus.interested
                ? 'interested in'
                : 'maybe attending (have to confirm ASAP!)'}{' '}
          this event!{'\n'}
          Tap on the drop down menu to change your choice
        </Text>
        {this.state.showStatusPicker && (
          <View
            style={{
              height: 120,
              width: 120,
              backgroundColor: '#fff',
              shadowColor: '#000',
              shadowOffset: { width: 0, height: 0 },
              shadowOpacity: 0.1,
              shadowRadius: 3,
              elevation: 6,
              position: 'absolute',
              right: 25,
              top: 80
            }}
          >
            {_.range(1, 5).map(item => (
              <TouchableOpacity
                style={{
                  height: 30,
                  width: 120,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderBottomWidth: 0.5,
                  borderBottomColor: '#ccc'
                }}
                onPress={() => {
                  this.setState({ showStatusPicker: false });
                  switch (item) {
                    case 1: {
                      this._updateAttendStatus('going');
                      break;
                    }
                    case 2: {
                      this._updateAttendStatus('notGoing');
                      break;
                    }
                    case 3: {
                      this._updateAttendStatus('interested');
                      break;
                    }
                    case 4: {
                      this._updateAttendStatus('maybe');
                      break;
                    }
                  }
                }}
              >
                <Text>
                  {item === 1
                    ? 'Going'
                    : item === 2
                      ? 'Not Going'
                      : item === 3
                        ? 'Interested'
                        : 'Maybe'}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
        )}
      </View>
    );
  }

  _renderCost(event) {
    return (
      <View>
        <Text
          style={[
            eventStyle.text,
            {
              textAlign: 'center',
              color: 'black',
              fontSize: 20,
              fontWeight: '200'
            }
          ]}
        >
          Cost: {event.cost ? event.cost : 'Free'}
        </Text>
      </View>
    );
  }

  _renderOrganizers(event) {
    return (
      <View
        style={[
          eventStyle.reverseEntryContainer,
          {
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            borderBottomWidth: 0.5,
            borderColor: '#aaa'
          }
        ]}
      >
        <Text
          style={[
            eventStyle.text,
            {
              color: 'black',
              fontSize: 20,
              fontWeight: '200'
            }
          ]}
        >
          Organizers:
        </Text>
        <RenderUserBadges
          userIDs={this.state.event.organizersArray}
          FitlyFirebase={this.FitlyFirebase}
          textStyle={{ color: 'grey' }}
          pushToRoute={this.pushToRoute}
          uID={this.uID}
        />
        {this.isAdmin &&
          event.endDate > new Date() && (
            <TouchableOpacity
              onPress={() => {
                let nav;
                if (this.props.navigation.state.params.fromProfile)
                  nav = this.props.navigation.state.params.rootNav;
                else nav = this.props.navigation;
                nav.navigate('EditAdminScene', {
                  eventID: this.props.navigation.state.params.eventID,
                  event,
                  uID: this.uID,
                  firebase: this.props.FitlyFirebase
                  // renderHeader: this._renderHeaderInvitation.bind(this)
                });
              }}
              style={{ position: 'absolute', top: 5, right: 0 }}
            >
              <Icon name={'md-add'} size={20} color={'#ccc'} />
            </TouchableOpacity>
          )}
      </View>
    );
  }

  _openMenu() {
    this.setState({ menuOpen: !this.state.menuOpen });
  }

  _renderMenu() {
    return (
      <View
        style={{
          position: 'absolute',
          top: 10,
          right: 10,
          borderColor: '#aaa',
          borderWidth: 1,
          flex: 1,
          flexDirection: 'column',
          zIndex: 200,
          backgroundColor: '#fff',
          width: 100,
          padding: 10,
          shadowColor: 'black',
          shadowOpacity: 0.6,
          elevation: 2,
          shadowOffset: { width: 0, height: 0 },
          shadowRadius: 2
        }}
      >
        <TouchableOpacity
          onPress={() => {
            this._openMenu();
            this.props.navigation.navigate('ReportScene', {
              type: 'event',
              details: this.state.event,
              contentID: this.props.navigation.state.params.eventID
            });
          }}
        >
          <Text>Report...</Text>
        </TouchableOpacity>
      </View>
    );
  }

  _renderHeader() {
    return this.isAdmin ? (
      <HeaderInView
        leftElement={{ icon: 'ios-arrow-round-back-outline' }}
        rightElement={{ icon: 'ios-options-outline' }}
        title="Event Details"
        _onPressRight={() =>
          this.props.navigation.navigate('EventOptionsMenu', {
            uID: this.uID,
            navigateBack: this.props.navigation,
            event: this.state.event,
            rootNav: this.props.navigation.state.params.rootNav,
            fromProfile: this.props.navigation.state.params.fromProfile,
            FitlyFirebase: this.FitlyFirebase,
            eventID: this.props.navigation.state.params.eventID
          })
        }
        _onPressLeft={() => this.props.navigation.goBack()}
      />
    ) : (
      <HeaderInView
        leftElement={{ icon: 'ios-arrow-round-back-outline' }}
        rightElement={{ icon: 'ios-more' }}
        title="Event Details"
        _onPressLeft={() => this.props.navigation.goBack()}
        _onPressRight={this._openMenu.bind(this)}
      />
    );
  }

  _renderMembers() {
    return (
      <View
        style={[
          eventStyle.reverseEntryContainer,
          {
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            borderBottomWidth: 0.5,
            borderColor: '#aaa',
            marginBottom: 10
          }
        ]}
      >
        <Text
          style={[
            eventStyle.text,
            {
              color: 'black',
              fontSize: 20,
              fontWeight: '200'
            }
          ]}
        >
          Members:
        </Text>
        {this.state.members.length ? (
          <RenderUserBadges
            userIDs={this.state.members}
            displayName={false}
            FitlyFirebase={this.FitlyFirebase}
            pushToRoute={this.pushToRoute}
            uID={this.uID}
          />
        ) : (
          <Text style={postStyle.textContent}>no members yet</Text>
        )}
      </View>
    );
  }

  _renderEventBody() {
    const { event } = this.state;
    return (
      <View>
        <View>
          <Image // TODO: Add Image changing option
            style={{ width: width, justifyContent: 'center', height: 250 }}
            resizeMode="cover"
            source={
              event.backgroundImage
                ? { uri: event.backgroundImage, isStatic: true }
                : require('../../../img/default-photo-image.png')
            }
            defaultSource={require('../../../img/default-photo-image.png')}
          >
            <View
              style={{
                width: width,
                height: 250,
                backgroundColor: 'rgba(0,0,0,.5)'
              }}
            >
              <View style={eventStyle.titleContainer}>
                <Text style={eventStyle.title}>{event.title}</Text>
                <Text style={eventStyle.title}>{event.category}</Text>
              </View>
              <View
                style={{ position: 'absolute', bottom: 5, left: 0, right: 0 }}
              >
                <Text
                  style={{
                    fontSize: 15,
                    textAlign: 'center',
                    color: 'white',
                    paddingBottom: 10
                  }}
                >
                  {event.isPublic ? 'public event' : 'private event'} -
                  <Text style={eventStyle.textContent}>
                    {event.memberCount} going
                  </Text>
                </Text>
              </View>
            </View>
          </Image>
          <View>
            {this.isAdmin
              ? this._renderCost(event)
              : this._renderAttendBtns(event)}
          </View>

          {this._renderOrganizers(event)}

          <View
            style={{
              marginHorizontal: 15,
              paddingVertical: 5,
              borderBottomWidth: 0.5,
              borderColor: '#aaa',
              alignItems: 'flex-start',
              flexDirection: 'column',
              justifyContent: 'center'
            }}
          >
            {this.state.editEventDetials && event.endDate > new Date() ? (
              <AutoExpandingTextInput
                placeholder={'Event Details'}
                value={this.state.text}
                style={[
                  {
                    fontSize: 14,
                    color:
                      this.state.text === event.details ? '#000' : 'transparent'
                  }
                ]}
                onBlur={() => {
                  if (this.state.text !== event.details)
                    this.eventRef.child('details').set(this.state.text);
                  this.setState({ editEventDetials: false });
                }}
                onChangeText={text => this.setState({ text })}
              />
            ) : (
              <View>
                <Text
                  style={[
                    eventStyle.text,
                    {
                      color: 'black',
                      fontSize: 20,
                      fontWeight: '200'
                    }
                  ]}
                >
                  Event Details:
                </Text>
                <Text style={postStyle.textContent}>
                  {event.details || 'none'}
                </Text>
              </View>
            )}
            {this.isAdmin &&
              !this.state.editEventDetials &&
              event.endDate > new Date() && (
                <Text
                  onPress={() => this.setState({ editEventDetials: true })}
                  style={{ position: 'absolute', right: 0, top: 20 }}
                >
                  edit
                </Text>
              )}
          </View>
          <View
            style={{
              marginHorizontal: 15,
              paddingVertical: 5,
              borderBottomWidth: 0.5,
              borderColor: '#aaa',
              alignItems: 'flex-start',
              flexDirection: 'column',
              justifyContent: 'center',
              alignSelf: 'stretch'
            }}
          >
            <Text
              style={[
                eventStyle.text,
                {
                  color: 'black',
                  fontSize: 20,
                  fontWeight: '200'
                }
              ]}
            >
              Time:
            </Text>
            <View
              style={{
                alignSelf: 'stretch',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
            >
              <View>
                <Text style={[eventStyle.text, { color: 'black' }]}>
                  {getWeekdayMonthDay(event.startDate)}
                </Text>
                <Text style={[eventStyle.text, { color: 'black' }]}>
                  {getHrMinDuration(event.startDate, event.endDate)}
                </Text>
              </View>
              <View>
                <Icon
                  style={[eventStyle.icon, { paddingLeft: 10 }]}
                  name="ios-calendar-outline"
                  size={30}
                  color="#bbb"
                />
                {this.isAdmin && event.endDate > new Date() ? (
                  <Text
                    style={{ marginLeft: 10, color: 'black' }}
                    onPress={() =>
                      this.props.navigation.navigate('SelectDateScene', {
                        eventID: this.props.navigation.state.params.eventID,
                        startDate: event.startDate,
                        endDate: event.endDate
                      })
                    }
                  >
                    edit
                  </Text>
                ) : null}
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            marginHorizontal: 15,
            paddingVertical: 5,
            borderBottomWidth: 0.5,
            borderColor: '#aaa',
            alignItems: 'flex-start',
            flexDirection: 'column',
            justifyContent: 'center',
            alignSelf: 'stretch'
          }}
        >
          <Text
            style={[
              eventStyle.text,
              {
                color: 'black',
                fontSize: 20,
                fontWeight: '200'
              }
            ]}
          >
            Location:
          </Text>
          <View
            style={{
              alignSelf: 'stretch',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <View style={{ flex: 0.7 }}>
              {event.location.placeName.toLowerCase() !== 'unamed' && (
                <Text>{event.location.placeName}</Text>
              )}
              <Text>{event.location.address}</Text>
            </View>
            <View
              style={{
                flex: 0.3,
                flexDirection: 'column',
                alignItems: 'flex-end',
                justifyContent: 'space-around'
              }}
            >
              <Icon
                style={eventStyle.icon}
                name="ios-map-outline"
                size={30}
                color="#bbb"
              />
              {this.isAdmin && event.endDate > new Date() ? (
                <TouchableHighlight
                  onPress={() =>
                    this.props.navigation.navigate('SelectLocationScene', {
                      eventID: this.props.navigation.state.params.eventID,
                      location: event.location,
                      header: true
                    })
                  }
                >
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ color: 'black' }}>edit</Text>
                  </View>
                </TouchableHighlight>
              ) : null}
            </View>
          </View>
        </View>
        {this._renderMembers()}
        {this._renderPhotos(event.photos)}
        {this._renderTags(event.tags)}
        {this._renderSocialBtns()}
      </View>
    );
  }

  _renderEvent() {
    //implement reply notification for organizer only when there is one organizer
    const initialRoute = {
      contentID: this.props.navigation.state.params.eventID,
      contentType: 'event',
      parentAuthor:
        this.state.event.organizers && this.state.event.organizers.length === 1
          ? this.state.event.organizers[0]
          : null,
      isPrivateContent: !this.state.event.public
    };
    return (
      <View>
        {this._renderEventBody()}
        <Modal
          animationType={'slide'}
          onRequestClose={() => this.setState({ modalVisible: false })}
          visible={this.state.modalVisible}
        >
          <HeaderInView
            leftElement={{ icon: 'ios-arrow-round-back-outline' }}
            title="Discussion"
            _onPressLeft={() => this.setState({ modalVisible: false })}
          />
          <CommentsModal
            screenProps={this.props.screenProps}
            navigation={this.props.navigation}
            modalVisible={this.state.modalVisible}
            noNav={true}
            openModal={() => this.setState({ modalVisible: true })}
            closeModal={() => this.setState({ modalVisible: false })}
            initialRoute={initialRoute}
          />
        </Modal>
      </View>
    );
  }

  _renderSocialBtns() {
    //render social btn differently depending on whether the event is public
    const contentInfo = {
      contentID: this.props.navigation.state.params.eventID,
      contentType: 'event',
      parentAuthor: this.state.event.organizersArray.length
        ? this.state.event.organizersArray[0]
        : null,
      isPrivateContent: !this.state.event.isPublic
    };
    return (
      <SocialBtns
        contentInfo={contentInfo}
        content={this.state.event}
        buttons={{ comment: true, like: true, share: true, save: true }}
        onComment={() => this.setState({ modalVisible: true })}
      />
    );
  }

  //TODO: below two functions will be refactored out
  _renderPhotos(photos) {
    if (!photos) {
      return null;
    }
    return (
      <View style={postStyle.imgContainer}>
        {photos.map((photo, index) => {
          return (
            <TouchableOpacity
              style={postStyle.imagesTouchable}
              key={'postPhotos' + index}
              onPress={() =>
                this.props.navigation.navigate('EventImagesView', {
                  photos
                })
              }
            >
              <Image
                style={postStyle.images}
                source={{ uri: photo.link }}
                defaultSource={require('../../../img/default-photo-image.png')}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }

  _renderTags(tags) {
    if (!tags) {
      return null;
    }
    return (
      <View style={postStyle.tagsRow}>
        {tags.map((tag, index) => {
          return (
            <Text style={postStyle.tags} key={'tag' + index}>
              #{tag}
            </Text>
          );
        })}
      </View>
    );
  }

  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        {this._renderHeader()}
        <ScrollView
          bounces={false}
          contentContainerStyle={postStyle.scrollContentContainer}
          showsVerticalScrollIndicator={false}
        >
          {this.state.menuOpen ? this._renderMenu() : null}
          {this.state.event ? (
            this._renderEvent()
          ) : (
            <Spinner
              visible={!this.state.event}
              textContent={''}
              textStyle={{ color: '#FFF' }}
            />
          )}
          {/* <View style={{height: 100}}></View> */}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user.user,
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = dispatch => {
  return {
    action: bindActionCreators(
      {
        storeUserProfile
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ push, pop, resetTo }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventScene);
