import React, { Component } from 'react';
import { Modal, View } from 'react-native';
import ComposeComment from './ComposeComment.js';
export default class CommentsModal extends Component {
  constructor(props) {
    super(props);
    let initialRouteStack = this.props.initialRoute
      ? [this.props.initialRoute]
      : [];
    this.isPrivateContent =
      this.props.initialRoute && this.props.initialRoute.isPrivateContent
        ? this.props.initialRoute.isPrivateContent
        : false;
    this.state = {
      routeStack: initialRouteStack,
      skipInititalRoute: false
    };
  }

  _pushRoute(route) {
    let newRoutes = this.state.routeStack.slice();
    newRoutes.push(route);
    this.setState({ routeStack: newRoutes });
  }
  _popRoute() {
    const pop = () => {
      let newRoutes = this.state.routeStack.slice();
      newRoutes.pop();
      this.setState({ routeStack: newRoutes });
    };

    let minStackSize = this.state.skipInititalRoute ? 2 : 1;
    if (this.state.routeStack.length > minStackSize) {
      pop();
    } else {
      if (this.state.skipInititalRoute) {
        this.setState({ skipInititalRoute: false });
        pop();
      }
      // this.props.closeModal();
    }
  }
  render() {
    const stackSize = this.state.routeStack.length;
    const latestRoute = this.state.routeStack[this.state.routeStack.length - 1];
    return (
      <ComposeComment
        navigation={this.props.navigation}
        screenProps={this.props.screenProps}
        contentInfo={latestRoute}
        noNav={this.props.noNav}
        isPrivateContent={this.isPrivateContent}
        pushRoute={this._pushRoute.bind(this)}
        closeModal={() => this._popRoute()}
        goBackName={this.props.goBackName}
      />
    );
  }
}

CommentsModal.propTypes = {
  modalVisible: React.PropTypes.bool,
  renderParent: React.PropTypes.func,
  closeModal: React.PropTypes.func,
  initialRoute: React.PropTypes.object
};
