import React, { Component } from 'react';
import {
  composeStyle,
  headerStyle,
  postStyle,
  FitlyBlue
} from '../../styles/styles.js';
import {
  Modal,
  View,
  TextInput,
  Text,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
  Keyboard,
  Animated
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AutoExpandingTextInput from '../../common/AutoExpandingTextInput.js';
import HeaderInView from '../../header/HeaderInView.js';
import TagInput from 'react-native-tag-input';
import Icon from 'react-native-vector-icons/Ionicons';
import { pop, push, resetTo } from '../../actions/navigation.js';
import { save, clear } from '../../actions/drafts.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  savePhotoToDB,
  saveUpdateToDB
} from '../../library/firebaseHelpers.js';
import { selectPictureCropper } from '../../library/pictureHelper.js';
import Firebase from 'firebase';
import ParentView from './ParentView.js';
import CommentsView from './CommentsView.js';
import { NavigationActions } from 'react-navigation';
const isAndroid = Platform.OS === 'android';
let keyBoardHeight = 0;
//TODO: input validation??
const hashTagRegex = /^\w+$/g;
class ComposeComment extends Component {
  translateY = new Animated.Value(0);
  constructor(props) {
    super(props);
    this.initialState = {
      loading: false,
      contentType: 'light-content',
      content: '',
      photo: null,
      tags: [],
      scrollTo: 0,
      keyboardVisible: false,
      disabled: false
    };
    this.state = { ...this.initialState };
    this.user = this.props.user;
    this.uID = this.props.uID;
    this.database = this.props.FitlyFirebase.database();
    this.isPrivateContent = this.props.isPrivateContent;
    this.msgRef = this.database.ref('/messages/');
    this.userMsgRef = this.database.ref('/userMessages/' + this.uID);
    this._setRefs(this.props);
    if (this.props.contentInfo.parentAuthor) {
      this.parentAuthor = this.props.contentInfo.parentAuthor;
      this.notificationRef = this.database.ref(
        '/otherNotifications/' + this.parentAuthor
      );
    }
    Keyboard.addListener('keyboardWillShow', e => {
      Animated.timing(this.translateY, {
        toValue: this.props.noNav
          ? -e.endCoordinates.height
          : -e.endCoordinates.height + 50,
        duration: 200,
        useNativeDriver: true
      }).start();
    });
    Keyboard.addListener('keyboardWillHide', e => {
      Animated.timing(this.translateY, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true
      }).start();
    });
  }

  _setRefs(props) {
    this.contentID = props.contentInfo.contentID;
    this.contentType = props.contentInfo.contentType;

    if (this.contentType === 'post') {
      this.contentLink = '/posts/' + this.contentID;
      this.parentRef = this.database.ref(this.contentLink);
      this.parentCommentRef = this.database.ref(
        '/postComments/' + this.contentID
      );
    } else if (this.contentType === 'event') {
      this.contentLink = '/events/' + this.contentID;
      this.parentRef = this.database.ref(this.contentLink);
      this.parentCommentRef = this.database.ref(
        '/eventComments/' + this.contentID
      );
    } else if (this.contentType === 'message') {
      this.contentLink = '/messages/' + this.contentID;
      this.parentRef = this.database.ref(this.contentLink);
      this.parentCommentRef = this.parentRef.child('replies');
    } else if (this.contentType === 'photo') {
      this.contentLink = '/photos/' + this.contentID;
      this.parentRef = this.database.ref(this.contentLink);
      this.parentCommentRef = this.parentRef.child('replies');
    }
  }

  componentWillReceiveProps(nextProps) {
    this._setRefs(nextProps);
  }

  _sendMsg() {
    if ((this.state.content && this.state.content.length) || this.state.photo)
      (async () => {
        try {
          this.setState({ loading: true });
          let draftState = this.state;
          let msgKey = this.msgRef.push().key;
          let authorInfo = {
            author: this.uID,
            authorName:
              this.user.public.first_name + ' ' + this.user.public.last_name,
            authorPicture: this.user.public.picture
          };

          let msgObj = {
            ...authorInfo,
            contentlink: this.contentLink,
            content: draftState.content,
            replyCount: 0,
            likeCount: 0,
            shareCount: 0,
            saveCount: 0,
            createdAt: Firebase.database.ServerValue.TIMESTAMP
          };
          if (this.state.photo) {
            let photoRefs = await savePhotoToDB(
              [this.state.photo],
              authorInfo,
              '/messages/' + msgKey
            );
            msgObj.photo = photoRefs[0];
            msgObj.content = null;
          }
          this.msgRef.child(msgKey).set(msgObj);
          this.userMsgRef
            .child(msgKey)
            .set({ timestamp: Firebase.database.ServerValue.TIMESTAMP });
          this.parentCommentRef
            .child(msgKey)
            .set({ timestamp: Firebase.database.ServerValue.TIMESTAMP });
          this.parentRef.child('replyCount').transaction(count => count + 1);
          this.parentRef
            .child('lastRepliedAt')
            .set(Firebase.database.ServerValue.TIMESTAMP);
          this.parentRef.child('lastMsg').set(this.state.content);

          if (this.parentAuthor) {
            const updateObj = {
              type: 'reply',
              sourceType: this.contentType,
              sourceID: this.contentID,
              msgID: msgKey,
              ownerID: this.uID,
              ownerName: authorInfo.authorName,
              ownerPicture: authorInfo.authorPicture,
              message: draftState.content,
              timestamp: Firebase.database.ServerValue.TIMESTAMP,
              isPrivate: this.isPrivateContent
            };

            // only adds to parentAuthor notifications if they are not the comment author
            if (this.parentAuthor !== this.uID) {
              this.notificationRef.push(updateObj);
            }
          }
          this.setState({ ...this.initialState });
        } catch (error) {
          this.setState({ loading: false });
          console.log('create post error', error);
        }
      })();
  }

  _sendPhotoMsg() {
    selectPictureCropper()
      .then(photo => {
        this.setState({ photo: { path: photo.uri } }, () => this._sendMsg());
      })
      .catch(error => {
        console.log('error getting photo');
      });
  }

  _renderHeader() {
    return (
      <HeaderInView
        leftElement={{ icon: 'ios-arrow-round-back-outline' }}
        title="Post View"
        _onPressLeft={() => {
          this.props.navigation.goBack();
        }}
      />
    );
  }
  inputBox = null;
  _renderInputBar() {
    return (
      <Animated.View
        style={[
          postStyle.inputBar,
          { transform: [{ translateY: this.translateY }] }
        ]}
      >
        <AutoExpandingTextInput
          thisRef={ref => (this.inputBox = ref)}
          clearButtonMode="always"
          onChangeText={text => this.setState({ content: text })}
          style={postStyle.replyInput}
          placeholderTextColor="grey"
          placeholder="Add a comment..."
          returnKeyType="send"
          onSubmitEditing={() => this._sendMsg()}
        />
        {this.state.loading ? (
          <ActivityIndicator
            animating={this.state.loading}
            size="small"
            color={FitlyBlue}
          />
        ) : (
          <TouchableOpacity
            style={postStyle.cameraBtn}
            onPress={() => this._sendPhotoMsg()}
          >
            <Icon
              name="ios-camera-outline"
              size={35}
              color="#888"
              style={{ backgroundColor: 'transparent' }}
            />
          </TouchableOpacity>
        )}
      </Animated.View>
    );
  }
  _renderPostContent() {
    return (
      <View
        style={{
          borderBottomWidth: 0,
          paddingBottom: 250,
          flex: 1
        }}
      >
        <ParentView
          screenProps={this.props.screenProps}
          navigation={this.props.navigation}
          route={this.props.contentInfo}
          disabled={this.state.disabled}
          isPrivateContent={this.isPrivateContent}
          onComment={() => this.inputBox.focus()}
          pushRoute={this.props.pushRoute}
        />
        <CommentsView
          navigation={this.props.navigation}
          screenProps={this.props.screenProps}
          route={this.props.contentInfo}
          isPrivateContent={this.isPrivateContent}
          pushRoute={this.props.pushRoute}
        />
      </View>
    );
  }
  render() {
    let draftState = this.state;
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          bounces={false}
          keyboardShouldPersistTaps={'always'}
          onScrollBeginDrag={() => {
            this.setState({ disabled: true });
          }}
          onScrollEndDrag={() => {
            this.setState({ disabled: false });
          }}
          showsVerticalScrollIndicator={false}
        >
          {this._renderPostContent()}
        </ScrollView>
        {this._renderInputBar()}
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    user: state.user.user,
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    exnavigation: bindActionCreators({ pop, push, resetTo }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ComposeComment);
