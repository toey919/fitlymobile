import React, { Component } from 'react';
import {
  postStyle,
  feedEntryStyle,
  eventStyle,
  FitlyBlue
} from '../../styles/styles.js';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  TouchableHighlight,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { push } from '../../actions/navigation.js';
import TimeAgo from 'react-native-timeago';
import SocialBtns from '../SocialBtns.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Author from '../Author.js';
import { convertFBObjToArray } from '../../library/firebaseHelpers.js';
import FitImage from '../../library/FitImage.js';
const { width, height } = Dimensions.get('window');
class ParentView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: null,
      like: false,
      shared: false,
      saved: false,
      route: this.props.route,
      currentPhoto: 0
    };
    this._renderPhotos = this._renderPhotos.bind(this);
    this.uID = this.props.uID;
    this.database = this.props.FitlyFirebase.database();
  }

  componentDidMount() {
    this._getContent(this.props.route);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.route.contentID !== this.state.route.contentID) {
      this._getContent(nextProps.route);
    }
  }

  _getContent(route) {
    this.setState({ route: route, content: null });
    const { contentType, contentID } = route;
    this.contentRef = this.database.ref(contentType + 's').child(contentID);
    this.contentRef.once('value').then(snap => {
      let contentObj = snap.val();
      if (contentObj.photos) {
        contentObj.photos = convertFBObjToArray(snap.child('photos'));
      }
      contentObj.tags = Object.keys(contentObj.tags || {});
      this.setState({
        content: contentObj
      });
    });
  }

  _renderPhotos(content) {
    if (!content.photos) return null;
    if (content.photos.length)
      return (
        <View
          style={{ zIndex: 99 }}
          onTouchStart={event => {
            event.stopPropagation();
          }}
        >
          <ScrollView
            horizontal
            bounces={false}
            pagingEnabled
            decelerationRate={'fast'}
            alwaysBounceVertical={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ height: height / 2 }}
            showsHorizontalScrollIndicator={false}
            onScroll={({ nativeEvent }) => {
              this.setState({
                currentPhoto: Math.round(nativeEvent.contentOffset.x / width)
              });
            }}
            onScrollBeginDrag={() => this.setState({ disabled: true })}
            onScrollEndDrag={() => this.setState({ disabled: false })}
            scrollEventThrottle={16}
          >
            {content.photos.map((item, index) => {
              const route = {
                contentID: item.key,
                contentType: 'photo',
                authorName: content.author
              };
              return (
                <TouchableHighlight
                  style={[postStyle.imagesTouchable, { height: height / 2 }]}
                  key={'postPhotos' + index}
                  disabled={this.state.disabled || content.photos.length === 1}
                  onPress={e => {
                    if (!this.props.disabled) this.props.pushRoute(route);
                  }}
                >
                  <Image
                    style={[postStyle.images, { height: height / 2 }]}
                    resizeMode="cover"
                    resizeMethod="auto"
                    source={{ uri: item.link }}
                    defaultSource={require('../../../img/default-photo-image.png')}
                  />
                </TouchableHighlight>
              );
            })}
          </ScrollView>
          <View
            style={{
              height: 40,
              width,
              position: 'absolute',
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              zIndex: 999
            }}
          >
            {content.photos.length > 1 &&
              content.photos.map((item, index) => (
                <View
                  style={{
                    height: 7,
                    width: 7,
                    backgroundColor:
                      this.state.currentPhoto === index
                        ? 'rgba(255,255,255,1)'
                        : 'rgba(0,0,0,0.3)',
                    borderRadius: 3.5,
                    marginRight: 5,
                    zIndex: 999
                  }}
                  key={index}
                />
              ))}
          </View>
        </View>
      );
  }
  _renderTaggedFriends(content) {
    const { taggedUsers = [] } = content;
    if (!taggedUsers.length) return null;
    return (
      <Text
        style={{
          fontSize: 10,
          color: '#aaa',
          alignSelf: 'flex-end',
          marginRight: 10,
          marginBottom: 10
        }}
      >
        with{' '}
        <Text style={{ fontSize: 10, color: '#444', fontWeight: 'bold' }}>
          {taggedUsers[0].name}
        </Text>
        {taggedUsers.length == 2 && ' and '}
        {taggedUsers.length >= 2 && (
          <Text style={{ fontSize: 10, color: '#444', fontWeight: 'bold' }}>
            {taggedUsers.length > 2
              ? taggedUsers.length - 1
              : taggedUsers[1].name}
          </Text>
        )}
        {taggedUsers.length > 2 && ' others '}
      </Text>
    );
  }
  _renderTags(tags) {
    return (
      <View style={[postStyle.tagsRow, { width, flexWrap: 'wrap' }]}>
        {tags.map((tag, index) => {
          return (
            <Text style={postStyle.tags} key={'tag' + index}>
              #{tag}
            </Text>
          );
        })}
      </View>
    );
  }
  _renderPostContent(content) {
    return (
      <View style={{ marginLeft: 60, minHeight: 50, overflow: 'hidden' }}>
        <Text
          style={[
            postStyle.title,
            { paddingBottom: 0, fontSize: 14, fontWeight: 'bold' }
          ]}
        >
          {content.title}
        </Text>
        <Text style={[postStyle.textContent, { paddingBottom: 0 }]}>
          {content.content}
        </Text>
        {content.extraContent ? (
          <View>
            <Text style={[postStyle.textContent, { paddingBottom: 0 }]}>
              {(content.category.includes('Meal')
                ? 'Ingredients required'
                : content.category.includes('Workout')
                  ? 'Equipment needed'
                  : 'Additional details') + ' : '}
            </Text>
            <Text style={postStyle.textContent}>{content.extraContent}</Text>
            {this._renderTaggedFriends(content)}
          </View>
        ) : null}
      </View>
    );
  }
  _renderFirstComment(content) {
    return (
      <View style={{ marginTop: 15 }}>
        <TimeAgo style={feedEntryStyle.timestamp} time={content.createdAt} />
        <Author
          navigation={this.props.navigation}
          uID={this.props.uID}
          content={content}
          style={{ marginLeft: 15 }}
          screenProps={this.props.screenProps}
          nonClickable={true}
        />
        {this._renderPostContent(content)}
        <SocialBtns
          contentInfo={this.props.route}
          content={this.state.content}
          onComment={this.props.onComment}
          buttons={{ like: true, share: true, save: true, comment: true }}
        />
      </View>
    );
  }
  _renderPost(content) {
    return (
      <View style={postStyle.postContainer}>
        {content.photos && (
          <Author
            navigation={this.props.navigation}
            uID={this.props.uID}
            content={content}
            style={{ marginLeft: 15, marginBottom: 5 }}
            screenProps={this.props.screenProps}
            nonClickable={true}
          />
        )}
        <View style={postStyle.postContent}>
          {this._renderPhotos(content)}
          {this._renderFirstComment(content)}
        </View>
      </View>
    );
  }

  _renderMsg(content) {
    return (
      <View style={postStyle.postContainer}>
        <Author
          navigation={this.props.navigation}
          uID={this.props.uID}
          style={{ marginLeft: 15 }}
          content={content}
          nonClickable={true}
        />
        <TimeAgo style={feedEntryStyle.timestamp} time={content.createdAt} />
        {content.photo ? (
          <Image
            style={feedEntryStyle.images}
            source={{ uri: content.photo.link }}
            style={feedEntryStyle.images}
            defaultSource={require('../../../img/default-photo-image.png')}
          />
        ) : (
          <Text style={[postStyle.content, { marginLeft: 30 }]}>
            {content.content}
          </Text>
        )}
        <SocialBtns
          contentInfo={this.props.route}
          content={this.state.content}
          onComment={this.props.onComment}
          buttons={{ like: true, share: true, save: true, comment: true }}
        />
      </View>
    );
  }

  _renderPhoto(content) {
    return (
      <View style={postStyle.postContainer}>
        <Author
          navigation={this.props.navigation}
          uID={this.props.uID}
          screenProps={this.props.screenProps}
          style={{ marginLeft: 15 }}
          content={content}
        />
        <TimeAgo
          style={[feedEntryStyle.timestamp, { right: 15 }]}
          time={content.createdAt}
        />
        <Image
          style={{ width: width, height: height / 2 }}
          resizeMode="cover"
          source={{ uri: content.link }}
        />
        <Text style={postStyle.content}>{content.description}</Text>
        {this._renderTags(content.tags)}
        <SocialBtns
          contentInfo={this.props.route}
          content={this.state.content}
          onComment={this.props.onComment}
          buttons={{ like: true, share: true, save: true, comment: true }}
        />
      </View>
    );
  }

  _renderEvent(content) {
    return (
      <View style={postStyle.postContainer}>
        <Text style={{ textAlign: 'center' }}>{content.title}</Text>
        <Text style={{ textAlign: 'center' }}>Disscusion</Text>
      </View>
    );
  }

  render() {
    const { contentType, contentID } = this.state.route;
    const { content } = this.state;
    if (!content) {
      return (
        <ActivityIndicator
          animating={true}
          style={{ height: 80 }}
          size="small"
          color={FitlyBlue}
        />
      );
    } else if (contentType === 'post') {
      return this._renderPost(content);
    } else if (contentType === 'message') {
      return this._renderMsg(content);
    } else if (contentType === 'photo') {
      return this._renderPhoto(content);
    } else if (contentType === 'event') {
      return this._renderEvent(content);
    }
  }
}

const mapStateToProps = function(state) {
  return {
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    exnavigation: bindActionCreators({ push }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ParentView);
