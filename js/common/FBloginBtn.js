/**
 * @flow
 */

import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { loginStyles, FitlyBlue } from '../styles/styles.js';
import Icon from 'react-native-vector-icons/Ionicons';

import {
  asyncFBLoginWithPermission,
  asyncFBLogout,
  fetchFBProfile
} from '../library/asyncFBLogin.js';
import {
  setFirebaseUID,
  setSignUpMethod,
  printAuthError,
  clearAuthError
} from '../actions/auth.js';
import { storeUserProfile } from '../actions/user.js';
import { setBlocks } from '../actions/blocks.js';
import { setLoadingState, setSearchLocation } from '../actions/app.js';
import { resetTo } from '../actions/navigation.js';
import { connect } from 'react-redux';
import Firebase from 'firebase';
import { bindActionCreators } from 'redux';
import { updateCurrentLocationInDB } from '../library/firebaseHelpers.js';
import {
  getCurrentPlace,
  getPlaceByName
} from '../library/asyncGeolocation.js';

import { NavigationActions } from 'react-navigation';
const FB_PHOTO_WIDTH = 300;

class FBloginBtn extends Component {
  constructor(props) {
    super(props);
  }

  //TODO error reporting for login error
  _handleFBLogin() {
    const { FitlyFirebase, navigation, action } = this.props;
    const resetToTabNavigation = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'TabNavigator',
          params: { newUser: false }
        })
      ]
    });
    const resetToLocationSelect = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'SetupLocationView',
          params: { facebook: true }
        })
      ]
    });
    (async () => {
      try {
        action.clearAuthError();
        action.setLoadingState(true);
        await asyncFBLogout();
        const data = await asyncFBLoginWithPermission([
          'public_profile',
          'email',
          'user_friends',
          'user_location',
          'user_birthday'
        ]);
        action.setSignUpMethod('Facebook');
        const userFBprofile = await fetchFBProfile(data.credentials.token);

        if (!userFBprofile.email) {
          action.setLoadingState(false);
          action.printAuthError(
            'We need your access to your FB profile in order to create an account'
          );
          return;
        }
        const credential = Firebase.auth.FacebookAuthProvider.credential(
          data.credentials.token
        );
        const user = await FitlyFirebase.auth().signInWithCredential(
          credential
        );
        const userRef = FitlyFirebase.database().ref('users/' + user.uid + '/');
        action.setFirebaseUID(user.uid);
        const blocks = (await FitlyFirebase.database()
          .ref('blocks/' + user.uid)
          .once('value')).val();
        let api = `https://graph.facebook.com/v3.0/${
          userFBprofile.id
        }/picture?width=${FB_PHOTO_WIDTH}&redirect=false&access_token=${
          data.credentials.token
        }`;
        let userPhoto = userFBprofile.picture.data.url;
        fetch(api)
          .then(res => res.json())
          .then(resData => {
            userPhoto = resData.data.url;
          })
          .done();

        //update user's Facebook friends everytime they login with Facebook
        let firebaseUserData = (await userRef.once('value')).val();
        const userInterests = (await FitlyFirebase.database()
          .ref('interests/' + user.uid)
          .once('value')).val();
        if (firebaseUserData === null) {
          const {
            first_name,
            last_name,
            picture,
            email,
            gender,
            birthday,
            friends,
            id
          } = userFBprofile;
          let api = `https://graph.facebook.com/v2.3/${
            userFBprofile.id
          }/picture?width=${FB_PHOTO_WIDTH}&redirect=false&access_token=${
            data.credentials.token
          }`;
          let pictureUrl = (await fetch(api)).url;
          userRef.set({
            public: {
              account: 'default',
              first_name: first_name,
              last_name: last_name,
              picture: userPhoto,
              provider: 'Facebook',
              summary: '',
              isActive: true,
              profileComplete: false,
              FacebookID: id,
              dateJoined: Firebase.database.ServerValue.TIMESTAMP,
              lastActive: Firebase.database.ServerValue.TIMESTAMP,
              followerCount: 1,
              followingCount: 0,
              sessionCount: 0,
              profileProgress: {
                uploadPhoto: false,
                completeTutorial: false,
                hostAnEvent: false,
                attendAnEvent: false,
                joinAWorkoutNowSession: false,
                follow4Accounts: false,
                get10Followers: false,
                invite3Friends: false,
                leaveAReviewOnTheAppStore: false,
                sendFeedBack: false,
                shareToSocialMedia: false,
                shareAPhoto: false
              }
            },
            private: {
              email: email,
              gender: gender || null,
              birthday: birthday || null,
              friends: friends || null
            }
          });
          this.database
            .ref('/followers/' + user.uid + '/4hbknUXtBTQbqrzroZJaYow6h7G3')
            .set(true);
          this.database
            .ref('/followings/4hbknUXtBTQbqrzroZJaYow6h7G3' + '/' + user.uid)
            .set(true);
          await userRef
            .child('public')
            .child('profilePictures')
            .push()
            .set(pictureUrl);
          navigation.dispatch(resetToLocationSelect);
        } else if (!firebaseUserData.public.profileComplete) {
          navigation.dispatch(resetToLocationSelect);
        } else {
          await updateCurrentLocationInDB(user.uid);
          if (userFBprofile.friends) {
            await FitlyFirebase.database()
              .ref('users/' + user.uid + '/private/friends')
              .set(userFBprofile.friends);
          }
          await FitlyFirebase.database()
            .ref('users/' + user.uid + '/public/picture')
            .set(userPhoto);
          firebaseUserData = (await userRef.once('value')).val();
          firebaseUserData.public.picture = userPhoto;
          this.props.action.storeUserProfile(firebaseUserData);
          const coordinate = {
            coordinate: firebaseUserData.public.userCurrentLocation
              ? firebaseUserData.public.userCurrentLocation.coordinate
              : false
          };
          if (coordinate.coordinate) action.setSearchLocation(coordinate);
          action.setBlocks(blocks);
          navigation.dispatch(resetToTabNavigation);
        }
        action.setLoadingState(false);
      } catch (error) {
        action.setLoadingState(false);
        console.log(error, 'fberr');
        action.printAuthError(
          error === 'Cancel'
            ? 'You canceled the sign-in flow, Please select one of the sign-in method to continue.'
            : 'An unexpected error has occoured. Please try again'
        );
      }
    })();
  }

  render() {
    return (
      <TouchableOpacity
        style={[
          loginStyles.FBbtn,
          this.props.style,
          {
            flexDirection: 'row',
            alignItems: 'center',
            width: 260,
            height: 50,
            justifyContent: 'center',
            borderRadius: 30,
            backgroundColor: '#3b5998'
          }
        ]}
        onPress={() => this._handleFBLogin()}
      >
        <View
          style={{
            backgroundColor: '#fff',
            width: 30,
            height: 27,
            marginRight: 5,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Icon
            name={'logo-facebook'}
            color={'#3b5998'}
            size={42}
            style={{
              backgroundColor: 'transparent'
            }}
          />
        </View>
        <View
          style={{
            paddingLeft: 10,
            borderLeftColor: '#ccc',
            borderLeftWidth: 1,
            height: 30,
            width: 190,
            justifyContent: 'center'
          }}
        >
          <Text style={[loginStyles.btnText, { color: '#fff' }]}>
            Continue With Facebook
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    loading: state.app.loading
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        setFirebaseUID,
        setSignUpMethod,
        printAuthError,
        setLoadingState,
        storeUserProfile,
        clearAuthError,
        setSearchLocation,
        setBlocks
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ resetTo }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FBloginBtn);
