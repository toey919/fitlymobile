/**
 * @flow
 */

import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { loginStyles, FitlyBlue } from '../styles/styles.js';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  setFirebaseUID,
  setSignUpMethod,
  printAuthError,
  clearAuthError
} from '../actions/auth.js';
import { storeUserProfile } from '../actions/user.js';
import { setBlocks } from '../actions/blocks.js';
import { setLoadingState, setSearchLocation } from '../actions/app.js';
import { resetTo } from '../actions/navigation.js';
import { connect } from 'react-redux';
import Firebase from 'firebase';
import { bindActionCreators } from 'redux';
import { updateCurrentLocationInDB } from '../library/firebaseHelpers.js';
import {
  getCurrentPlace,
  getPlaceByName
} from '../library/asyncGeolocation.js';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { NavigationActions } from 'react-navigation';

class GoogleloginBtn extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this._setupGoogleSignin();
  }
  async _setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true });
      await GoogleSignin.configure({
        iosClientId:
          '356069342993-megogq783oeuc0598kl34dehvh2551h5.apps.googleusercontent.com',
        webClientId:
          '356069342993-1lflsl8sle1t4o94367ahmklcvk1adp0.apps.googleusercontent.com'
      });
    } catch (err) {
      console.log('Google signin error', err.code, err.message);
    }
  }
  //TODO error reporting for login error
  async _handleGoogleLogin() {
    const { FitlyFirebase, navigation, action } = this.props;
    const resetToTabNavigation = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'TabNavigator',
          params: { newUser: false }
        })
      ]
    });
    const resetToLocationSelect = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'SetupLocationView',
          params: { facebook: false }
        })
      ]
    });
    action.clearAuthError();
    action.setLoadingState(true);
    await this._signOut();
    GoogleSignin.signIn()
      .then(async data => {
        try {
          const credential = Firebase.auth.GoogleAuthProvider.credential(
            data.idToken,
            data.accessToken
          );
          const user = await FitlyFirebase.auth().signInWithCredential(
            credential
          );
          const userGoogleprofile = await GoogleSignin.currentUserAsync();
          const userRef = FitlyFirebase.database().ref(
            'users/' + user.uid + '/'
          );
          action.setFirebaseUID(user.uid);
          const blocks = (await FitlyFirebase.database()
            .ref('blocks/' + user.uid)
            .once('value')).val();
          let userPhoto = userGoogleprofile.photo;
          const userInterests = (await FitlyFirebase.database()
            .ref('interests/' + user.uid)
            .once('value')).val();
          let firebaseUserData = (await userRef.once('value')).val();
          if (firebaseUserData === null) {
            const { name, email, id } = userGoogleprofile;
            let names = name.split(' ');
            userRef.set({
              public: {
                account: 'default',
                first_name: names[0],
                last_name: names[1],
                picture: userPhoto,
                provider: 'Google',
                isActive: true,
                summary: '',
                profileComplete: false,
                GoogleID: id,
                dateJoined: Firebase.database.ServerValue.TIMESTAMP,
                lastActive: Firebase.database.ServerValue.TIMESTAMP,
                followerCount: 1,
                followingCount: 0,
                sessionCount: 0,
                profileProgress: {
                  uploadPhoto: false,
                  completeTutorial: false,
                  hostAnEvent: false,
                  attendAnEvent: false,
                  joinAWorkoutNowSession: false,
                  follow4Accounts: false,
                  get10Followers: false,
                  invite3Friends: false,
                  leaveAReviewOnTheAppStore: false,
                  sendFeedBack: false,
                  shareToSocialMedia: false,
                  shareAPhoto: false
                }
              },
              private: {
                email: email
              }
            });

            await userRef
              .child('public')
              .child('profilePictures')
              .push()
              .set(userPhoto);
            this.database
              .ref('/followers/' + user.uid + '/4hbknUXtBTQbqrzroZJaYow6h7G3')
              .set(true);
            this.database
              .ref('/followings/4hbknUXtBTQbqrzroZJaYow6h7G3' + '/' + user.uid)
              .set(true);
            // this.props.action.setSearchLocation(currentLocation.coordinate);
            navigation.dispatch(resetToLocationSelect);
          } else if (!firebaseUserData.public.profileComplete) {
            navigation.dispatch(resetToLocationSelect);
          } else {
            await updateCurrentLocationInDB(user.uid);
            await FitlyFirebase.database()
              .ref('users/' + user.uid + '/public/picture')
              .set(userPhoto);
            firebaseUserData = (await userRef.once('value')).val();
            firebaseUserData.public.picture = userPhoto;
            this.props.action.storeUserProfile(firebaseUserData);
            const coordinate = {
              coordinate: firebaseUserData.public.userCurrentLocation
                ? firebaseUserData.public.userCurrentLocation.coordinate
                : false
            };
            if (coordinate.coordinate) action.setSearchLocation(coordinate);
            action.setBlocks(blocks);
            navigation.dispatch(resetToTabNavigation);
          }
          action.setLoadingState(false);
        } catch (error) {
          action.setLoadingState(false);
          console.log(error);
          action.printAuthError(error.message);
        }
      })
      .catch(err => {
        action.setLoadingState(false);
        console.log('WRONG SIGNIN', err);
        action.printAuthError(
          err.code === -5
            ? 'You canceled the sign-in flow, Please select one of the sign-in method to continue.'
            : 'An unexpected error has occoured. Please try again'
        );
      })
      .done();
  }
  _signOut() {
    GoogleSignin.signOut()
      .then(() => {
        console.log('out');
      })
      .catch(err => {});
  }
  render() {
    return (
      <TouchableOpacity
        style={[
          loginStyles.FBbtn,
          this.props.style,
          {
            flexDirection: 'row',
            alignItems: 'center',
            width: 260,
            height: 50,
            justifyContent: 'center',
            borderRadius: 30,
            backgroundColor: '#fff',
            borderWidth: 1,
            borderColor: '#fff'
          }
        ]}
        onPress={() => this._handleGoogleLogin()}
      >
        <Image
          source={require('../../img/google-logo.png')}
          resizeMethod="resize"
          resizeMode="stretch"
          style={{
            width: 32,
            height: 32,
            marginRight: 5
          }}
        />
        <View
          style={{
            paddingLeft: 10,
            borderLeftColor: '#aaa',
            borderLeftWidth: 1,
            height: 30,
            width: 190,
            justifyContent: 'center'
          }}
        >
          <Text style={[loginStyles.btnText, { color: '#888' }]}>
            Continue with Google
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    loading: state.app.loading
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        setFirebaseUID,
        setSignUpMethod,
        printAuthError,
        setLoadingState,
        storeUserProfile,
        clearAuthError,
        setSearchLocation,
        setBlocks
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ resetTo }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GoogleloginBtn);
