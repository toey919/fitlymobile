import React, { Component } from 'react';
import { Dimensions, View, Text, Image, TouchableOpacity } from 'react-native';
import { feedEntryStyle } from '../styles/styles.js';
import { push } from '../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TimeAgo from 'react-native-timeago';
let screenWidth = Dimensions.get('window').width;

class PhotoFeed extends Component {
  constructor(props) {
    super(props);

    //TODO: settings will dictate what kind of feed should trigger a push notification and should be rendered
  }
  _renderPhotos(feed) {
    const picSize = screenWidth / 3 - 2;
    return feed.photos.map((photo, index) => {
      return (
        <TouchableOpacity
          style={{
            height: picSize,
            width: picSize,
            marginBottom: 2,
            marginLeft: 1,
            marginRight: 1
          }}
          key={'feedPhotos' + index}
          onPress={() => {
            if (!this.props.blockTouchables)
              this.props.navigation.navigate('PostView', {
                postID: feed.contentID
              });
          }}
        >
          <Image
            style={[
              feedEntryStyle.photoFeedEntry,
              { width: picSize, height: picSize }
            ]}
            source={{ uri: photo.link }}
            defaultSource={require('../../img/default-photo-image.png')}
          />
        </TouchableOpacity>
      );
    });
  }

  render() {
    if (
      this.props.feeds.filter(feed => !!feed.photos.length && feed.isActive)
        .length > 0
    ) {
      return (
        <View style={feedEntryStyle.photoFeedContainer}>
          {this.props.feeds
            .filter(feed => !!feed.photos.length && feed.isActive)
            .map((feed, index) => this._renderPhotos(feed))}
        </View>
      );
    }
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          paddingHorizontal: 30,
          paddingVertical: 30
        }}
      >
        <Text
          style={{
            textAlign: 'center',
            color: '#ccc'
          }}
        >
          Your photo feeds are empty, let's find someone you want to follow
        </Text>
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    exnavigation: bindActionCreators({ push }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoFeed);
