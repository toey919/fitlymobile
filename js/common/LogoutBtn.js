/**
 * @flow
 */

import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Dimensions } from 'react-native';
import { headerStyle, FitlyBlue } from '../styles/styles.js';
import { asyncFBLogout } from '../library/asyncFBLogin.js';
import { resetAuthState, printAuthError } from '../actions/auth.js';
import { clearUserProfile } from '../actions/user.js';
import { resetTo, clearLocalNavState } from '../actions/navigation.js';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { bindActionCreators } from 'redux';
import { NavigationActions } from 'react-navigation';
const { width } = Dimensions.get('window');
const btnStyle = {
  flexDirection: 'row',
  borderBottomWidth: 1,
  borderBottomColor: '#ccc',
  height: 40,
  alignItems: 'center',
  justifyContent: 'space-between',
  paddingLeft: 32,
  paddingRight: 20
};

class LogoutBtn extends Component {
  constructor(props) {
    super(props);
  }

  //refactor out later into a service or a logout btn component
  _logout() {
    (async () => {
      try {
        if (this.props.signInMethod === 'Facebook') {
          await asyncFBLogout();
        }
        const navAction = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'WelcomeView' })]
        });
        await this.props.FitlyFirebase.auth().signOut();
        this.props.action.resetAuthState();
        this.props.navigation.dispatch(navAction);

        // setting User to null will throw error for other components.
        // this.props.action.clearUserProfile();
      } catch (err) {
        this.props.action.printAuthError(err);
        console.log('Uh oh... something weird happened', err);
      }
    })();
  }

  render() {
    return (
      <TouchableOpacity style={btnStyle} onPress={() => this._logout()}>
        <Text style={{ fontSize: 16 }}>Logout</Text>
        <Icon name="ios-arrow-forward" />
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    signInMethod: state.auth.signInMethod,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      { resetAuthState, printAuthError, clearUserProfile },
      dispatch
    ),
    exnavigation: bindActionCreators({ resetTo, clearLocalNavState }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LogoutBtn);
