import React, { Component } from 'react';
import {
  postStyle,
  feedEntryStyle,
  composeStyle,
  headerStyle,
  FitlyBlue
} from '../styles/styles.js';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { NavigationActions } from 'react-navigation';

export default (Author = props => {
  const {
    content = {},
    navigation,
    screenProps,
    reversed,
    nonClickable,
    showName = true,
    uID
  } = props;
  const _goToProfile = id => {
    if (screenProps) {
      if (id === uID) {
        screenProps.tabNavigation.navigate('Profile');
      } else {
        navigation.navigate('ProfileEntry', { otherUID: id });
      }
    } else {
      navigation.navigate('ProfileEntry', { otherUID: id });
    }
  };

  const Wrapper = TouchableOpacity;
  const name = showName ? (
    <Text
      style={[
        feedEntryStyle.username,
        { marginTop: -10, color: FitlyBlue, fontWeight: 'bold' }
      ]}
    >
      {content.authorName || props.name}
    </Text>
  ) : null;
  return reversed ? (
    <Wrapper
      onPress={() => _goToProfile(content.author)}
      style={[feedEntryStyle.profileRow, props.style]}
    >
      {name}
      <Image
        source={
          content.authorPicture || props.picture
            ? { uri: content.authorPicture || props.picture }
            : require('../../img/default-user-image.png')
        }
        style={feedEntryStyle.defaultImg}
        defaultSource={require('../../img/default-user-image.png')}
      />
    </Wrapper>
  ) : (
    <Wrapper
      onPress={() => _goToProfile(content.author)}
      style={[feedEntryStyle.profileRow, props.style]}
    >
      <Image
        source={
          content.authorPicture || props.picture
            ? { uri: content.authorPicture || props.picture }
            : require('../../img/default-user-image.png')
        }
        style={feedEntryStyle.defaultImg}
        defaultSource={require('../../img/default-user-image.png')}
      />
      <Text style={{ color: FitlyBlue, marginLeft: 5 }}>{name}</Text>
    </Wrapper>
  );
});

Author.propTypes = {
  content: React.PropTypes.object,
  pushRoute: React.PropTypes.func
};
