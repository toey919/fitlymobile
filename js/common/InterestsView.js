import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  ScrollView,
  Dimensions,
  Slider,
  Switch,
  Platform,
  ActivityIndicator,
  FlatList,
  TextInput,
  StatusBar
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { storeUserProfile } from '../actions/user.js';
import { printError, clearError, setSearchLocation } from '../actions/app.js';
import { push, resetTo } from '../actions/navigation.js';
import { createUpdateObj } from '../library/firebaseHelpers.js';
import { loginStyles, loginStylesInverse, FitlyBlue } from '../styles/styles';
import { getNewCategoriesWithoutHeader } from '../constants/categories.js';
const { width, height } = Dimensions.get('window');
class InterestsView extends Component {
  constructor(props) {
    super(props);
    this.categories = getNewCategoriesWithoutHeader();
    // const ds = new ListView.DataSource({
    //   rowHasChanged: (r1, r2) => r1 !== r2
    // });
    this.state = {
      interests: {},
      loading: true,
      submitting: false,
      searchText: '',
      lastIndex: 17
    };
  }

  componentWillMount() {
    this.props.FitlyFirebase.database()
      .ref('interests/' + this.props.uID)
      .once('value')
      .then(data => {
        if (data.val()) {
          this.setState({
            interests: data.val(),
            loading: false
          });
        } else {
          const interests = {};
          for (let sport of this.categories) {
            console.log(sport);
            interests[sport.label] = false;
          }
          this.setState({ interests: interests, loading: false });
        }
      })
      .catch(err => {
        console.log('woops!!!', err);
      });
  }

  _handlePress() {
    this.setState({ submitting: true });
    const { FitlyFirebase } = this.props;
    //TODO: if picture not created yet, direct to picture upload scene
    this.props.action.clearError();
    (async () => {
      try {
        FitlyFirebase.database()
          .ref('interests/' + this.props.uID)
          .update(this.state.interests);
        this.setState({ submitting: false });
        if (this.props.navigation.state.params.facebook) {
          FitlyFirebase.database()
            .ref('users/' + this.props.uID + '/public')
            .update({ profileComplete: true });
          this.props.navigation.dispatch(
            NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'TabNavigator',
                  params: { newUser: true }
                })
              ]
            })
          );
        }
        this.props.navigation.navigate('SetupProfileView');
      } catch (error) {
        this.props.action.printError(error.message);
        this.setState({ submitting: false });
      }
    })();
  }

  _renderInterests() {
    const width = Dimensions.get('window').width - 60;
    return (
      <View style={{ width, flexWrap: 'wrap', flexDirection: 'row' }}>
        {Object.keys(this.state.interests)
          .filter(item => {
            if (this.state.searchText !== '')
              return item
                .toLowerCase()
                .includes(this.state.searchText.toLowerCase());
            return true;
          })
          .map((sport, i) => {
            if (i > this.state.lastIndex) return null;
            return (
              <TouchableHighlight
                key={i}
                underlayColor={'transparent'}
                onPress={() => {
                  let obj = Object.assign({}, this.state.interests);
                  obj[sport] = !this.state.interests[sport];
                  this.setState({
                    interests: obj
                  });
                }}
                style={{
                  width: width / 2 - 10,
                  height: 100,
                  borderWidth: 0.75,
                  borderColor: this.state.interests[sport] ? FitlyBlue : '#000',
                  marginRight: 5,
                  backgroundColor: this.state.interests[sport]
                    ? FitlyBlue
                    : '#fff',
                  marginVertical: 5,
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: this.state.interests[sport] ? '#fff' : '#000'
                  }}
                >
                  {sport}
                </Text>
              </TouchableHighlight>
            );
            return null;
          })}
      </View>
    );
  }
  count = 0;
  _onEndReached() {
    this.setState({ lastIndex: this.state.lastIndex + 8 });
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff'
        }}
      >
        <View style={{ flex: 1 }}>
          <StatusBar
            barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
          />
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexDirection: 'row',
              width,
              paddingBottom: 50
            }}
            onScroll={({ nativeEvent }) => {
              let paddingToBottom = 10;
              paddingToBottom += nativeEvent.layoutMeasurement.height;
              if (
                nativeEvent.contentOffset.y >
                nativeEvent.contentSize.height - paddingToBottom
              ) {
                if (this.count === 0) {
                  this._onEndReached();
                  this.count = 1;
                }
              }
            }}
            scrollEventThrottle={16}
            onScrollBeginDrag={() => {
              this.count = 0;
            }}
          >
            <View style={[loginStylesInverse.container, { paddingTop: 16 }]}>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap', width }}>
                {Object.values(this.state.interests).map((item, index) => {
                  if (!item) return null;
                  return (
                    <View
                      style={{
                        backgroundColor: 'rgba(100, 170, 185, 0.22)',
                        margin: 5,
                        height: 20,
                        borderRadius: 10,
                        paddingHorizontal: 5
                      }}
                      key={index}
                    >
                      <Text
                        style={{
                          fontWeight: '600'
                        }}
                      >
                        #{Object.keys(this.state.interests)[index]}
                      </Text>
                    </View>
                  );
                })}
              </View>
              <Text
                style={[
                  loginStylesInverse.header,
                  { paddingTop: 0, paddingBottom: 10 }
                ]}
              >
                Select Interests
              </Text>
              <Text style={loginStylesInverse.textMid}>
                We'll send you notifications when these events are created in
                your area.
              </Text>
              <View
                style={{
                  width: width - 30,
                  height: 38,
                  borderWidth: 0.75,
                  borderColor: '#ccc',
                  alignSelf: 'center',
                  borderRadius: 20,
                  paddingHorizontal: 30,
                  marginVertical: 10
                }}
              >
                <TextInput
                  style={{
                    width: width - 30,
                    height: 38
                  }}
                  underlineColorAndroid={'transparent'}
                  onChangeText={text => {
                    this.setState({ searchText: text });
                  }}
                  placeholder={'Search'}
                />
              </View>
              {this.state.loading ? (
                <ActivityIndicator
                  animating={true}
                  style={{ height: 80 }}
                  size="large"
                  color={FitlyBlue}
                />
              ) : (
                this._renderInterests()
              )}
            </View>
          </ScrollView>
        </View>
        <TouchableHighlight
          style={loginStylesInverse.swipeBtn}
          onPress={() => this._handlePress()}
        >
          <View>
            {!this.state.submitting ? (
              <Text style={loginStylesInverse.btnText}>NEXT ></Text>
            ) : (
              <ActivityIndicator
                animating={this.state.submitting}
                style={{ height: 50, justifyContent: 'center' }}
                size="large"
                color={'#fff'}
              />
            )}
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    uID: state.auth.uID,
    error: state.app.error,
    FitlyFirebase: state.app.FitlyFirebase,
    user: state.user.user
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      { printError, clearError, storeUserProfile, setSearchLocation },
      dispatch
    ),
    exnavigation: bindActionCreators({ push, resetTo }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InterestsView);
