export const postCategories = [
  'Photos',
  'Workout Plan',
  'Meal Plan',
  'Photos',
  'Workout Plan'
];

export const newCats = {
  A: ['Aerial', 'Aerobics', 'Adventure Racing', 'Aikido'],
  B: [
    'Bachata',
    'Badminton',
    'Ballet',
    'Barre',
    'Baseball',
    'Basketball',
    'Belly Dance',
    'Bowling',
    'Boxing',
    'Boxing',
    'Brazilian Jiu Jitsu'
  ],
  C: [
    'Camping',
    'Canoeing',
    'Canyoneering',
    'Caporeia',
    'Caving',
    'Cha Cha',
    'Cheerleading',
    'Circuit Training',
    'Climbing',
    'Core',
    'Country',
    'Cricket',
    'Crossfit',
    'Cumbia',
    'Curling',
    'Cycling',
    'Cycling(Indoor)'
  ],

  D: ['Dodgeball', 'Disc Sports', 'Duathlon', 'Duathlon Training'],
  E: ['East Coast Swing', 'Equestrian Sports'],
  F: ['Fencing', 'Fishing', 'Field Hockey', 'Fox Trot', 'Football'],
  G: ['Golf', 'Gymnastics'],
  H: ['Handball', 'Hiking', 'Hip Hop', 'Hockey', 'Hunting'],
  I: ['Ice Hockey', 'Ice Skating'],
  J: ['Jazz', 'Jiu Jitsu', 'Jive', 'Judo'],
  K: [
    'Karate',
    'Kayaking',
    'Kendo',
    'Kettleball',
    'Kickball',
    'Kickboxing',
    'Kickboxing',
    'Kite Surfing',
    'Krav Maga',
    'Kung Fu',
    'Kung Fu(Shaolin)'
  ],

  L: ['Lacrosse'],
  M: [
    'Marathon Training',
    'Marathon',
    'Martial Arts',
    'Meditation',
    'Merengue',
    'MMA',
    'Mountain Biking',
    'Muay Thai'
  ],

  N: ['Netball', 'Ninjutsu'],
  O: [
    'Other Dance',
    'Other Endurance',
    'Other Fitness',
    'Other Outdoors',
    'Other Sports',
    'Other Wellness'
  ],

  P: [
    'Paddleboarding',
    'Paintball',
    'Paso Doble',
    'Pilates',
    'Pingpong',
    'Polo',
    'Prenatal Fitness'
  ],

  Q: ['Quick Step'],
  R: ['Racquetball', 'Rafting', 'Rowing', 'Rugby', 'Rumba', 'Running'],
  S: [
    'Sailing',
    'Salsa',
    'Samba',
    'Sambo',
    'Skating',
    'Skiing',
    'Skydiving',
    'Soccer',
    'Softball',
    'Squash',
    'Sumo',
    'Surfing',
    'Suspension Training',
    'Swimming',
    'Swimming'
  ],

  T: [
    'Table Tennis',
    'Taekwondo',
    'Tai Chi',
    'Tango',
    'Tap Dancing',
    'Tennis',
    'Trail Running',
    'Trail Running',
    'Trangleball',
    'Triathlon Training',
    'Triathlon'
  ],

  V: ['Volleyball'],
  W: [
    'Walking',
    'Waltz',
    'Water Polo',
    'Water Sports',
    'Weight Lifting',
    'West Coast Swing',
    'Wiffleball',
    'Wing Chun',
    'Winter Sports',
    'Wrestling',
    'Wushu'
  ],

  Y: ['Yoga'],
  Z: ['Zumba']
};
export const getNewCategoriesWithoutHeader = () => {
  let i = 0;
  let data = [];
  Object.values(newCats).forEach(c => {
    data = [...data, ...c];
  });
  const famousCats = [
    { key: i++, label: 'Yoga' },
    { key: i++, label: 'Basketball' },
    { key: i++, label: 'Outdoor Fitness' },
    { key: i++, label: 'Volleyball' },
    { key: i++, label: 'Soccer' },
    { key: i++, label: 'Running' },
    { key: i++, label: 'Spin Classes' },
    { key: i++, label: 'Tennis' },
    { key: i++, label: 'Cross fit' },
    { key: i++, label: 'Cycling' },
    { key: i++, label: 'Dancing' },
    { key: i++, label: 'Obstacle Courses' },
    { key: i++, label: 'Climbing' },
    { key: i++, label: 'Ultimate Frisby' },
    { key: i++, label: 'Swimming' },
    { key: i++, label: 'Football' },
    { key: i++, label: 'MMA' },
    { key: i++, label: 'Hockey' }
  ];
  let finalData = [];
  data.forEach(c => {
    finalData.push({ key: i++, label: c });
  });
  famousCats.forEach(fC => {
    let currLable = fC.label;
    const index = finalData.findIndex(
      d => d.label.toUpperCase() === currLable.toUpperCase()
    );
    if (index !== -1) {
      finalData.splice(index, 1);
    }
  });
  return [...famousCats, ...finalData];
};
