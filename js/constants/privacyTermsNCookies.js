export const PrivacyText = `Protecting your privacy is a serious matter and doing so is very
important to us. Please read this Privacy Policy statement (the
"Policy") to learn more about our Privacy Policy. This Policy describes
the information The Fitly App, Inc. (“The Fitly App”, “our”, “we”)
collect from you and what may happen to that information, and only
applies to such information. This Policy applies to all sites under the
thefitlyapp.com domain as well as our platforms, tools, and services
(together, the "Platform").\n\n
1. Information Collection\n
• 1.1. We collect the following information about you and your use of
our Platform in order to create a better, more personalized experience
for you:\n
o 1.1.1. a nickname selected by you or assigned by us, your email
address, your name (if provided) a password selected by you, your zip
code; and\n
o 1.1.2. for each The Fitly App Group or The Fitly App Event in which
you participate, you may choose to create and store a short description
or statement which will be viewed by anyone who is accessing that The
Fitly App Group or The Fitly App Everywhere, and your message board
postings.\n
o 1.1.3. for purposes of this Policy, "Personally Identifiable
Information" means information that could be used to identify your
personally (such as your email address or IP address), which has not
been previously or subsequently disclosed by you on the public areas of
our website or in messages you send to your The Fitly App Group Mailing
Lists or The Fitly App Everywhere emails.\n
• 1.2. We automatically track certain basic information about our
members (such as internet protocol (IP) addresses, browser type,
internet service provider (ISP), referring/exit pages, click patterns,
etc.). We use this information to do internal research on our members'
usage patterns, demographics, interests and general behavior to better
understand and serve you and our community.\n
• 1.3. We currently contract with online partners to help manage and
optimize our business and communications. For example, we use the
services of partners to help us deliver and measure the effectiveness of
our advertising and how visitors use our Platform. To do this, we and
our partners use third-party tracking technologies such as web beacons,
pixels and cookies on this Platform and on other websites and online
services. A "cookie" is a piece of data stored on your computer that is
tied to information about you. The type of information collected
includes the URL you came from and go to, your browser information, and
IP address, and helps us learn how to improve our service. We use these
technologies for authentication, tracking user sessions, preferences,
and movements around the Platform, anonymous and aggregated marketing
analytics, performance analytics, ad retargeting, and tracking aggregate
trends on the Platform. No information shared with our partners through
cookies and related technologies is directly linked to your Personally
Identifiable Information.\n
• 1.4. For our members' convenience, we also use "cookies" to allow you
to enter your password less frequently during a session and to provide
for an easier registration process. If you configure your browser or
otherwise choose to reject the cookies, you may still use our site.
However, to best experience our website and Platform and most easily use
our Platform you must have cookies enabled. Our use of cookies is
consistent with the rights and restrictions set forth in Section 2.{
  '\n'
}
• 1.5. We may collect information such as postings you make on the
public areas of our website, messages you send to the The Fitly App
Group Mailing List or The Fitly App Everywhere emails, messages you send
to us, and correspondence we receive from other members or third parties
about your activities or postings on our website. Our use of this
information is consistent with the rights and restrictions set forth in
Section 2.\n\n
2. Use of Information\n
• 2.1. We use the information we collect about you (including your
Personally Identifiable Information) to create a better, more
personalized experience for you based on your individual usage habits,
improve our marketing and promotional efforts, analyze site usage,
improve our content and product offerings, and customize our site's
content, layout and Services. These uses improve our site and allow us
to better customize it to meet your needs. We also use the information
we collect about you to resolve disputes, troubleshoot problems, and
enforce our Terms of Service Agreement.\n
• 2.2. We may compile the information we collect about you and use
it, in an aggregate form only, in the negotiation and establishment of
service agreements with public and/or private enterprises under which
such enterprises will serve as The Fitly App partners or as venues for
meetings between our members ("The Fitly Apps").\n
• 2.3. We may use for promotional, sales or any use that we consider
appropriate your correspondence with us or photographs submitted for
publication on our website, be it via email, postings on our website, or
feedback via the member polls. Our use of such materials is consistent
with the restrictions on disclosure of Personally Identifiable
Information set forth in Section 3.\n\n
3. Disclosure of Your Information\n
• 3.1. Opt-in requirement. WITHOUT YOUR AFFIRMATIVE CONSENT (ON A
CASE-BY-CASE BASIS), WE DO NOT SELL, RENT OR OTHERWISE SHARE YOUR
PERSONALLY IDENTIFIABLE INFORMATION WITH OTHER THIRD PARTIES, UNLESS
OTHERWISE REQUIRED AS DESCRIBED BELOW UNDER "REQUIRED DISCLOSURES". TO
THE EXTENT WE SHARE INFORMATION WITH OUR PARTNERS AND ADVERTISERS, WE
SHARE ONLY AGGREGATED OR OTHERWISE NON-PERSONALLY IDENTIFIABLE
INFORMATION THAT IS NOT LINKED TO YOUR PERSONALLY IDENTIFIABLE
INFORMATION. Aggregated information that we may share with our marketing
partners includes, but is not limited to, information showing the
relative popularity of one The Fitly App venue over another, or the
popularity of certain The Fitly App topics.\n
• 3.2. You should understand that information you provide through the
registration process or post to the public areas of our website, or
through the use of our Platform (including your name (if provided) and
location information) may be accessible by and made public through
syndication programs and by search engines, metasearch tools, crawlers,
metacrawlers and other similar programs.\n
• 3.3. Required disclosures. Though we make every effort to preserve
member privacy, we may need to disclose your Personally Identifiable
Information when required by law or if we have a good-faith belief that
such action is necessary to (a) comply with a current judicial
proceeding, a court order or legal process served on our website, (b)
enforce this Policy or the Terms of Service Agreement, (c) respond to
claims that your Personal Information violates the rights of third
parties; or (d) protect the rights, property or personal safety of The
Fitly App, its members and the public. You authorize us to disclose any
information about you to law enforcement or other government officials
as we, in our sole discretion, believe necessary or appropriate, in
connection with an investigation of fraud, intellectual property
infringements, or other activity that is illegal or may expose us or you
to legal liability.\n\n
4. Communications from The Fitly App and Members of the The Fitly App
Community\n
• 4.1. Communication from The Fitly App and Members of the The Fitly App
Community are governed by Sections 7.1 and 7.2 of our Terms of Service.
You may manage your subscriptions to all The Fitly App Communications in
the Communication Preferences tab of the Your Account page.\n
4B. Retargeting\n
• 4.2 We partner with third parties to manage our advertising on other
sites and services. Our third party partners may use technologies such
as cookies and other third-party tracking technologies to gather
information about your activities on our Platform to market and
advertise our services to you on third party sites and services. For
example, third parties that we work with may use the fact that you
visited our Platform to target ads for The Fitly App services to you on
non-The Fitly App sites and services. Third parties that use cookies and
other tracking technologies to deliver targeted advertisements on third
party sites and services may offer you a way to prevent such targeted
advertisements by opting-out at the websites of industry groups such as
the Network Advertising Initiative
(http://www.networkadvertising.org/choices/) and/or the Digital
Advertising Alliance (http://www.aboutads.info/choices/) or if located
in the European Union, the European Interactive Digital Advertising
Alliance (http://www.youronlinechoices.eu/). You may also be able to
control advertising cookies provided by publishers, for example Google's
Ad Preference Manager (https://www.google.com/settings/ads/onweb/).
Please note that even if you choose to opt-out of receiving targeted
advertising, you may still receive advertising about our Platform — it
just will not be tailored to your interests or activities.\n\n
5. Reviewing, Updating, Deleting and Deactivating Personal Information{
  '\n'
}
• 5.1. After registration for our Platform and for specific topic
groups, The Fitly App Groups or The Fitly App Everywheres, we provide a
way to update your Personally Identifiable Information. Upon your
request, we will deactivate your account and remove your Personally
Identifiable Information from our active databases. To make this
request, email privacy@thefitlyapp.com. Upon our receipt of your
request, we will deactivate your account and remove your Personally
Identifiable Information as soon as reasonably possible in accordance
with our deactivation policy and applicable law. Nonetheless, we will
retain in our files information you may have requested us to remove if,
in our discretion, retention of the information is necessary to resolve
disputes, troubleshoot problems or to enforce the Terms of Service
Agreement. Furthermore, your information is never completely removed
from our databases due to technical and legal constraints (for example,
we will not remove your information from our back up storage).\n\n
6. Notification of Changes\n
• 6.1. If we decide to change this Policy, we will post those changes on
http://www.thefitlyapp.com/privacy or post a notice of the changes to
this Policy on the homepage (https://www.thefitlyapp.com/) and other
places we deem appropriate, so you are always aware of what information
we collect, how we use it, and under what circumstances, if any, we
disclose it. We will use information in accordance with the Privacy
Policy statement under which the information was collected.\n
• 6.2. If we make any material changes in our privacy practices, we will
post a prominent notice on our website notifying you and our other
members of the change. In some cases where we post a notice we will also
email you and other members who have opted to receive communications
from us, notifying them of the changes in our privacy practices.
However, if you have deleted/deactivated your account, then you will not
be contacted, nor will your previously collected personal information be
used in this new manner.\n
• 6.3. If the change to this Policy would change how your Personally
Identifiable Information is treated, then the change will not apply to
you without your affirmative consent. However, if after a period of
thirty (30) days you have not consented to the change in the Policy,
your account will be automatically suspended until such time as you may
choose to consent to the Policy change. Until such consent, your
personal information will be treated under the Policy terms in force
when you began your membership.\n
• 6.4. Any other change to this Policy (i.e., if it does not change how
we treat your Personally Identifiable Information) will become are
effective after we provide you with at least thirty (30) days notice of
the changes and provide notice of the changes as described above. You
must notify us within this 30 day period if you do not agree to the
changes to the Policy and wish to deactivate your account as provided
under Section 5.\n\n
7. Dispute Resolution\n
• 7.1. Any dispute, claim or controversy arising out of or relating to
this Policy or previous Privacy Policy statements shall be resolved
through negotiation, mediation and arbitration as provided under our
Terms of Service Agreement.`;
export const Terms = ` The Fitly App End User License Agreement\n\n
This End User License Agreement (“Agreement”) is between you and The
Fitly App and governs use of this app made available through the Apple
App Store. By installing The Fitly App, you agree to be bound by this
Agreement and understand that there is no tolerance for objectionable
content. If you do not agree with the terms and conditions of this
Agreement, you are not entitled to use The Fitly App.\n\n
In order to ensure The Fitly App provides the best experience possible
for everyone, we strongly enforce a no tolerance policy for
objectionable content. If you see inappropriate content, please use the
“Report” feature found under each post or event.\n\n
1. Parties\n
This Agreement is between you and The Fitly App only, and not Apple,
Inc. (“Apple”). Notwithstanding the foregoing, you acknowledge that
Apple and its subsidiaries are third party beneficiaries of this
Agreement and Apple has the right to enforce this Agreement against you.
The Fitly App, not Apple, is solely responsible for The Fitly App and
its content.\n\n
2. Privacy\n
The Fitly App may collect and use information about your usage of The
Fitly App, including certain types of information from and about your
device. The Fitly App may use this information, as long as it is in a
form that does not personally identify you, to measure the use and
performance of The Fitly App.\n\n
3. Limited License\n
The Fitly App grants you a limited, non-exclusive, non-transferable,
revocable license to use The Fitly App for your personal, non-commercial
purposes. You may only use The Fitly App on Apple devices that you own
or control and as permitted by the App Store Terms of Service.\n\n
4. Age Restrictions\n
By using The Fitly App, you represent and warrant that (a) you are 17
years of age or older and you agree to be bound by this Agreement; (b)
if you are under 17 years of age, you have obtained verifiable consent
from a parent or legal guardian; and (c) your use of The Fitly App does
not violate any applicable law or regulation. Your access to The Fitly
App may be terminated without warning if The Fitly App believes, in its
sole discretion, that you are under the age of 17 years and have not
obtained verifiable consent from a parent or legal guardian. If you are
a parent or legal guardian and you provide your consent to your child’s
use of The Fitly App, you agree to be bound by this Agreement in respect
to your child’s use of The Fitly App.\n\n
5. Objectionable Content Policy\n
Content may not be submitted to The Fitly App, who will moderate all
content and ultimately decide whether or not to post a submission to the
extent such content includes, is in conjunction with, or alongside any,
Objectionable Content. Objectionable Content includes, but is not
limited to: (i) sexually explicit materials; (ii) obscene, defamatory,
libelous, slanderous, violent and/or unlawful content or profanity;
(iii) content that infringes upon the rights of any third party,
including copyright, trademark, privacy, publicity or other personal or
proprietary right, or that is deceptive or fraudulent; (iv) content that
promotes the use or sale of illegal or regulated substances, tobacco
products, ammunition and/or firearms; and (v) gambling, including
without limitation, any online casino, sports books, bingo or poker.{
  '\n\n'
}
6. Warranty\n
The Fitly App disclaims all warranties about The Fitly App to the
fullest extent permitted by law. To the extent any warranty exists under
law that cannot be disclaimed, The Fitly App, not Apple, shall be solely
responsible for such warranty.\n\n
7. Maintenance and Support\n
The Fitly App does provide minimal maintenance or support for it but not
to the extent that any maintenance or support is required by applicable
law, The Fitly App, not Apple, shall be obligated to furnish any such
maintenance or support.\n\n
8. Product Claims\n
The Fitly App, not Apple, is responsible for addressing any claims by
you relating to The Fitly App or use of it, including, but not limited
to: (i) any product liability claim; (ii) any claim that The Fitly App
fails to conform to any applicable legal or regulatory requirement; and
(iii) any claim arising under consumer protection or similar
legislation. Nothing in this Agreement shall be deemed an admission that
you may have such claims.\n\n
9. Third Party Intellectual Property Claims\n
The Fitly App shall not be obligated to indemnify or defend you with
respect to any third party claim arising out or relating to The Fitly
App. To the extent The Fitly App is required to provide indemnification
by applicable law, The Fitly App, not Apple, shall be solely responsible
for the investigation, defense, settlement and discharge of any claim
that The Fitly App or your use of it infringes any third party
intellectual property right.`;
