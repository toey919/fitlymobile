import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { commonStyle, FitlyBlue } from '../../styles/styles.js';
import { resetTo } from '../../actions/navigation.js';
import LogoutBtn from '../../common/LogoutBtn.js';
import { push } from '../../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Firebase from 'firebase';
import FAIcons from 'react-native-vector-icons/FontAwesome';
import { saveUpdateToDB } from '../../library/firebaseHelpers.js';
import HeaderLocal from '../../header/HeaderLocal';

import NotificationTabs from './NotificationTabs.js';

// Views
import ProfileEntry from '../../common/ProfileEntry';
import PostView from '../../common/post/PostView';
import ImageView from '../../common/ImageView';
import EventScene from '../../common/activity/EventScene';

import { StackNavigator, NavigationActions } from 'react-navigation';
import { ConnectedNotifications } from './Notification';
import { crossFade } from '../../common/crossFadeTransitionConfig.js';

const notificationRoutes = {
  Notification: {
    screen: ConnectedNotifications,
    navigationOptions: {
      header: () => {
        return (
          <View
            style={{
              height: 80,
              backgroundColor: FitlyBlue,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text
              style={{
                fontFamily: 'HelveticaNeue',
                fontWeight: '700',
                letterSpacing: -1,
                fontSize: 22,
                color: 'white',
                marginBottom: -10
              }}
            >
              Notifications
            </Text>
          </View>
        );
      }
    }
  },
  PostView: {
    screen: PostView,
    navigationOptions: {
      header: null
    }
  },
  ProfileEntry: {
    screen: ProfileEntry,
    navigationOptions: {
      header: null
    }
  },
  ImageView: {
    screen: ImageView,
    navigationOptions: {
      header: null
    }
  },
  EventScene: {
    screen: EventScene,
    navigationOptions: {
      header: null
    }
  }
};

const NotificationStackNavigator = StackNavigator(notificationRoutes, {
  initialRouteName: 'Notification',
  transitionConfig: crossFade
});

class StackNavigationWrapper extends React.Component {
  render() {
    const { navigation, screenProps } = this.props;
    const { rootNavigation, newUser } = screenProps;

    const tabNavigatorScreenProps = {
      tabNavigation: navigation,
      rootNavigation,
      newUser
    };
    return <NotificationStackNavigator screenProps={tabNavigatorScreenProps} />;
  }
}

export default StackNavigationWrapper;
