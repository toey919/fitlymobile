import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  Modal,
  ScrollView,
  Dimensions
} from 'react-native';
import {
  loginStyles,
  loginStylesInverse,
  FitlyBlue,
  tabHeight
} from '../../styles/styles.js';
import { NavigationActions } from 'react-navigation';
const { width, height } = Dimensions.get('window');
import { pop, push, resetTo } from '../../actions/navigation.js';
import { setWorkoutType } from '../../actions/connect.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
const dots = new Array(4).fill(false);
import { StackNavigator } from 'react-navigation';

class Connect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      currentWalkthroughPage: 0
    };
  }

  _getBtnStyle(workoutType) {
    return this.props.workoutType === workoutType
      ? loginStylesInverse.FBbtn
      : [loginStyles.FBbtn, { borderWidth: 1, borderColor: FitlyBlue }];
  }

  _getBtnTextStyle(workoutType) {
    return this.props.workoutType === workoutType
      ? loginStylesInverse.btnText
      : loginStyles.btnText;
  }

  _onPress(type) {
    this.props.action.setWorkoutType(type);
    if (
      !this.props.user.private.height &&
      !this.props.user.private.skippedStats
    ) {
      this.props.screenProps.rootNavigation.navigate('SetupStatsView', {
        nextSteps: (() => {
          this.props.screenProps.rootNavigation.navigate('ActivityLevel', {
            ...this.props
          });
        }).bind(this)
      });
    } else {
      this.props.screenProps.rootNavigation.navigate('ActivityLevel', {
        ...this.props
      });
    }
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View
          style={{
            height: 80,
            backgroundColor: FitlyBlue,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontFamily: 'HelveticaNeue',
              fontWeight: '700',
              letterSpacing: -1,
              fontSize: 22,
              color: 'white',
              marginBottom: -10
            }}
          >
            Find A Partner
          </Text>
        </View>
        <Text style={[loginStylesInverse.textMid, { margin: 60 }]}>
          Select Workout Type
        </Text>
        <TouchableHighlight
          style={this._getBtnStyle('cardio')}
          onPress={this._onPress.bind(this, 'cardio')}
        >
          <Text style={this._getBtnTextStyle('cardio')}>Cardio</Text>
        </TouchableHighlight>
        <Text style={loginStylesInverse.textMid}>or</Text>
        <TouchableHighlight
          style={this._getBtnStyle('strength_training')}
          onPress={this._onPress.bind(this, 'strength_training')}
        >
          <Text style={this._getBtnTextStyle('strength_training')}>
            Strength Training
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    user: state.user.user,
    workoutType: state.connect.workoutType,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators({ setWorkoutType }, dispatch),
    exnavigation: bindActionCreators({ pop, push, resetTo }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Connect);
