import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import {
  loginStyles,
  loginStylesInverse,
  FitlyBlue,
  tabHeight
} from '../../styles/styles.js';
import { pop, push, resetTo } from '../../actions/navigation.js';
import { setWorkoutType } from '../../actions/connect.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ConnectedConnectView from './Connect';
import { StackNavigator } from 'react-navigation';
import { crossFade } from '../../common/crossFadeTransitionConfig.js';
const connectRoutes = {
  Connect: { screen: ConnectedConnectView }
};

const ConnectStackNavigation = StackNavigator(connectRoutes, {
  headerMode: 'none',
  initialRouteName: 'Connect',
  transitionConfig: crossFade
});

class ConnectNavigationWrapper extends React.Component {
  render() {
    const { navigation, screenProps } = this.props;
    const { rootNavigation, newUser } = screenProps;
    const navigationPropsToPass = {
      tabNavigation: navigation,
      rootNavigation,
      newUser
    };

    return <ConnectStackNavigation screenProps={navigationPropsToPass} />;
  }
}

export default ConnectNavigationWrapper;
