import React, { Component } from 'react';
import {
  Alert,
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  TouchableWithoutFeedback,
  Modal,
  StatusBar,
  Platform,
  Animated,
  RefreshControl
} from 'react-native';
import {
  profileStyle,
  FitlyBlue,
  loginStyles,
  headerStyle
} from '../../styles/styles.js';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { storeUserProfile } from '../../actions/user.js';
import { save, clear } from '../../actions/drafts.js';
import { push, pop } from '../../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FeedTabs from '../../common/FeedTabs.js';
import Prompt from 'react-native-prompt';
import {
  selectPictureCropper,
  selectPictureCropperCam
  // selectPicture
} from '../../library/pictureHelper.js';
import ActionSheet from 'react-native-actionsheet';
const CANCEL_INDEX = 2;
const DESTRUCTIVE_INDEX = 4;
const options = ['Gallery...', 'Camera...', 'Cancel'];
const title = 'Upload from...';
import Badge from 'react-native-smart-badge';
import {
  uploadPhoto,
  turnOnfeedService,
  turnOffeedService
} from '../../library/firebaseHelpers.js';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AutoExpandingTextInput from '../../common/AutoExpandingTextInput.js';
import HeaderProfile from '../../header/HeaderProfile';
import NewEventIcon from '../../common/NewEventIcon';
import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-menu';
import { randomString } from '../../library/firebaseHelpers.js';

import FAIcons from 'react-native-vector-icons/FontAwesome';

import { StackNavigator } from 'react-navigation';
import ComposePost from '../../common/post/ComposePost.js';
import CreateActivityScene from '../../common/activity/CreateActivityScene.js';
import SessionView from '../../common/workoutSession/SessionView.js';
import UserListView from '../../common/UserListView.js';
import SessionListView from '../../common/workoutSession/SessionListView.js';
import ProfileEntry from '../../common/ProfileEntry';
import PostView from '../../common/post/PostView';
import ImageView from '../../common/ImageView';
import ProfilePicsView from '../../common/ProfilePicsView';
import EventScene from '../../common/activity/EventScene';
import SelectDateScene from '../../common/activity/SelectDateScene';
import SelectLocationScene from '../../common/activity/SelectLocationScene';
import EventOptionsMenu from '../../common/activity/EventOptionsMenu';
const dots = new Array(3).fill(false);
const { width, height } = Dimensions.get('window');
const bar = Platform.OS === 'android' ? StatusBar.currentHeight : 0;

class Profile extends Component {
  ActionSheet;
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      othersFeed: [],
      feed: [],
      summaryEditMode: false,
      summaryInput: '',
      optionsOpen: false,
      currentWalkthroughPage: 0,
      tutorialDone: false,
      modalVisible: false,
      secondTutorial: false,
      thirdTutorial: false,
      gestureOpacity: new Animated.Value(0),
      swipeGesture: new Animated.Value(width / 2),
      color: new Animated.Value(0),
      animFinish: false,
      scrollHeight: null,
      scrolling: false
    };
  }
  // register listener to update the othersFeed, and follower count
  componentWillMount() {
    this._turnOnProfileWatcher();
    turnOnfeedService(
      this.props.uID,
      { self: false },
      feed => this.setState({ feed: feed.reverse() }),
      newFeed => this.setState({ feed: [newFeed].concat(this.state.feed) })
    );
  }
  componentWillUnmount() {
    this._turnOffProfileWatcher();
    turnOffeedService(this.props.uID, { self: true });
  }
  componentDidUpdate() {
    if (
      this.props.screenProps.currentActiveIndex !== 2 &&
      this.state.thirdTutorial &&
      !this.state.animFinish
    ) {
      this.setState({ thirdTutorial: false, animFinish: true });
    }
    if (
      !this.state.secondTutorial &&
      (!this.state.thirdTutorial && !this.state.animFinish) &&
      this.props.screenProps.startProfTutAnim
    ) {
      this.setState({ secondTutorial: true }, () => this.anim());
    }
    const { public: profile } = this.props.user;
    if (
      profile.followingCount >= 4 &&
      (!profile.profileProgress || !profile.profileProgress.follow4Accounts)
    ) {
      this.props.FitlyFirebase.database()
        .ref(`users/${this.props.uID}/public/profileProgress`)
        .update({ follow4Accounts: true });
      this.props.FitlyFirebase.database()
        .ref(`users/${this.props.uID}`)
        .on('value', snap => this.props.action.storeUserProfile(snap.val()));
    }
    if (
      profile.followerCount >= 10 &&
      (!profile.profileProgress || !profile.profileProgress.get10Followers)
    ) {
      this.props.FitlyFirebase.database()
        .ref(`users/${this.props.uID}/public/profileProgress`)
        .update({ get10Followers: true });
      this.props.FitlyFirebase.database()
        .ref(`users/${this.props.uID}`)
        .on('value', snap => this.props.action.storeUserProfile(snap.val()));
    }
  }
  _turnOnProfileWatcher() {
    const handleProfileChange = snapshot => {
      // const {private: privateData} = this.props.user;
      const data = snapshot.val();
      // TODO: get push notification for updates in follower and following
      this.props.action.storeUserProfile({
        private: data.private,
        public: data.public
      });
    };
    this.props.FitlyFirebase.database()
      .ref(`users/${this.props.uID}`)
      .on('value', handleProfileChange.bind(this));
  }

  _turnOffProfileWatcher() {
    this.props.FitlyFirebase.database()
      .ref(`users/${this.props.uID}/public/`)
      .off('value');
  }
  _renderAcitionSheet() {
    return (
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        title={title}
        options={options}
        cancelButtonIndex={CANCEL_INDEX}
        destructiveButtonIndex={DESTRUCTIVE_INDEX}
        onPress={i => {
          if (i == 0) {
            this._updateProfileGalleryPic();
          } else if (i == 1) {
            this._updateProfileCameraPic();
          }
        }}
      />
    );
  }
  _renderCenteredText(text, styles) {
    return <Text style={[profileStyle.centeredText, styles]}>{text}</Text>;
  }

  _updateSummary(value) {
    this.props.FitlyFirebase.database()
      .ref(`users/${this.props.uID}/public/summary`)
      .set(value);
    this.setState({ summaryEditMode: false });
  }

  _renderSummary(profile) {
    return (
      <View
        style={{
          alignItems: 'center',
          flexDirection: 'row',
          width: width - 32,
          justifyContent: 'center'
        }}
      >
        <Text
          style={[
            profileStyle.summaryText,
            !(!profile.picture || profile.picture.includes('default-user'))
              ? {
                  backgroundColor: 'transparent',
                  color: '#fff',
                  marginRight: 10
                }
              : {}
          ]}
          numberOfLines={1}
        >
          {profile.summary
            ? profile.summary
            : `I haven't filled this in yet...`}
        </Text>
        <Text
          style={[
            profileStyle.summaryTextEdit,
            { backgroundColor: 'transparent' }
          ]}
          onPress={() =>
            this.props.screenProps.rootNavigation.navigate('SettingsMenu', {
              editSummary: true
            })
          }
        >
          <Icon
            name="md-create"
            color={
              !profile.picture || profile.picture.includes('default-user')
                ? 'gray'
                : '#fff'
            }
            size={15}
            style={{ backgroundColor: 'transparent' }}
          />
        </Text>
      </View>
    );
  }

  _openOptions() {
    this.setState({
      optionsOpen: !this.state.optionsOpen
    });
  }

  _composePost() {
    const draftRef = randomString();
    this.props.draftsAction.save(draftRef, {
      category: 'Workout Plan',
      title: '',
      content: '',
      tags: [],
      photos: [],
      photoRefs: null
    });

    this.props.navigation.navigate('ComposePost', {
      draftRef
    });
  }

  _renderModal() {
    return (
      <Modal
        animationType={'fade'}
        transparent
        visible={this.state.optionsOpen}
        onRequestClose={() => this._openOptions()}
      >
        <TouchableWithoutFeedback onPress={this._openOptions.bind(this)}>
          <View
            style={{
              flexGrow: 1,
              width,
              height: height - bar,
              alignSelf: 'stretch',
              position: 'absolute',
              zIndex: 90,
              backgroundColor: 'rgba(0,0,0,0.5)'
            }}
          >
            {this.state.showCreatePostToolTip ? (
              <View
                style={{
                  width: 100,
                  height: 50,
                  backgroundColor: '#fff',
                  position: 'absolute',
                  bottom: 70,
                  right: 135,
                  padding: 10
                }}
              >
                <Text style={{ fontSize: 12, textAlign: 'center' }}>
                  Create new post here
                </Text>
              </View>
            ) : null}
            {this.state.showCreateEventToolTip ? (
              <View
                style={{
                  width: 100,
                  height: 50,
                  backgroundColor: '#fff',
                  position: 'absolute',
                  bottom: 185,
                  right: 20,
                  padding: 10
                }}
              >
                <Text style={{ fontSize: 12, textAlign: 'center' }}>
                  Create new event here
                </Text>
              </View>
            ) : null}
            <TouchableOpacity
              style={[profileStyle.blueBtn, { right: 25, bottom: 120 }]}
              onPress={() => {
                this._openOptions();
                this.props.screenProps.rootNavigation.navigate(
                  'CreateActivityScene'
                );
              }}
              onPressOut={() => {
                this.setState({ showCreateEventToolTip: false });
              }}
              onLongPress={() => {
                this.setState({ showCreateEventToolTip: true });
              }}
            >
              <NewEventIcon iconSize={30} color={'white'} />
            </TouchableOpacity>

            <TouchableOpacity
              style={[profileStyle.blueBtn, { right: 72, bottom: 65 }]}
              onPress={() => {
                this._composePost();
                this._openOptions();
              }}
              onPressOut={() => {
                this.setState({ showCreatePostToolTip: false });
              }}
              onLongPress={() => {
                this.setState({ showCreatePostToolTip: true });
              }}
            >
              <Text style={{ color: 'white', fontSize: 20 }}>
                <Icon name="ios-create-outline" size={30} color="white" />
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }

  _renderProfilePicsView = () => {
    this.props.navigation.navigate('ProfilePicsView', {
      uID: this.props.uID,
      navigation: this.props.navigation
    });
  };

  _updateProfileGalleryPic = () => {
    selectPictureCropper(true)
      .then(picture => this._storePhoto(picture.uri))
      //.then(picture => console.log(picture))
      .catch(err => console.log(err));
  };
  _updateProfileCameraPic = () => {
    selectPictureCropperCam(true)
      .then(picture => this._storePhoto(picture.uri))
      //.then(picture => console.log(picture))
      .catch(err => console.log(err));
  };

  _storePhoto = uri => {
    this.setState({ loading: true });
    uploadPhoto(`users/${this.props.uID}/profilePic/`, uri, {
      profile: false
    })
      .then(link => {
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}/public/picture`)
          .set(link);
        return link;
      })
      .then(link => {
        this.props.FitlyFirebase.database()
          .ref(`users/${this.props.uID}/public/profilePictures`)
          .push()
          .set(link);
        if (
          !this.props.user.public.profileProgress ||
          !this.props.user.public.profileProgress.uploadPhoto
        ) {
          this.props.FitlyFirebase.database()
            .ref(`users/${this.props.uID}/public/profileProgress`)
            .update({ uploadPhoto: true });
          this.props.FitlyFirebase.database()
            .ref(`users/${this.props.uID}`)
            .on('value', snap => this.props.action.storeUserProfile(snap.val()))
            .catch(err => console.log(err));
        }
        this.setState({ loading: false });
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log('upload profile pic', error);
      });
  };

  // this function should be a reusable component
  anim() {
    Animated.parallel([
      Animated.sequence([
        Animated.timing(this.state.gestureOpacity, {
          toValue: 1,
          duration: 200
        }),
        Animated.timing(this.state.swipeGesture, {
          toValue: 0,
          duration: 600
        }),
        Animated.timing(this.state.gestureOpacity, {
          toValue: 0,
          duration: 200
        }),
        Animated.timing(this.state.swipeGesture, {
          toValue: width / 2,
          duration: 500
        })
      ]),
      Animated.sequence([
        Animated.timing(this.state.color, {
          toValue: 500,
          duration: 700
        }),
        Animated.timing(this.state.color, {
          toValue: 0,
          duration: 700
        })
      ])
    ]).start(() => this.anim());
  }
  _renderHeader(profile, translateYName, picTranslateY, opacity) {
    return (
      <View
        style={{
          height: 80,
          paddingHorizontal: 16,
          width,
          backgroundColor: FitlyBlue,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}
      >
        <Animated.View
          style={{
            height: 50,
            width: 50,
            top: -150,
            position: 'absolute',
            left: 10,
            transform: [{ translateY: picTranslateY }]
          }}
        >
          <Animated.Image
            source={{ uri: profile.picture }}
            style={{
              borderColor: '#fff',
              borderWidth: 1.5,
              height: 50,
              width: 50,
              borderRadius: 25
            }}
            defaultSource={require('../../../img/default-user-image.png')}
          />
        </Animated.View>
        <Animated.Text style={[headerStyle.logoText, { opacity }]}>
          Fitly
        </Animated.Text>
        <Animated.View
          style={[
            {
              paddingVertical: 8,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
              flex: 2,
              top: 203,
              transform: [{ translateY: translateYName }]
            }
          ]}
        >
          <Text
            style={[
              profileStyle.nameText,
              {
                backgroundColor: 'transparent',
                color: '#fff'
              }
            ]}
          >
            {`${profile.first_name} ${profile.last_name}`}
          </Text>
        </Animated.View>
        <TouchableOpacity
          style={[
            {
              alignItems: 'center',
              justifyContent: 'center',
              position: 'absolute',
              right: 60,
              top: 30
            }
          ]}
          onPress={() => {
            this.props.screenProps.rootNavigation.navigate('MessagingScene');
          }}
        >
          <AwesomeIcon
            name="envelope-o"
            size={30}
            color="white"
            style={{ backgroundColor: 'transparent' }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            {
              alignItems: 'center',
              justifyContent: 'center',
              position: 'absolute',
              right: 20,
              top: 30
            }
          ]}
          onPress={() => {
            this.props.screenProps.rootNavigation.navigate('SettingsMenu', {
              editSummary: false
            });
          }}
        >
          <AwesomeIcon
            name="bars"
            size={30}
            color="white"
            style={{ backgroundColor: 'transparent' }}
          />
        </TouchableOpacity>
      </View>
    );
  }
  _scrollY = new Animated.Value(0);
  render() {
    const color = this.state.color.interpolate({
      inputRange: [0, 500],
      outputRange: ['rgba(29,47,123,1)', 'rgba(29,47,123,0)']
    });
    const translateYName = this._scrollY.interpolate({
      inputRange: [0, 174],
      outputRange: [0, -196],
      extrapolate: 'clamp'
    });
    const opacity = this._scrollY.interpolate({
      inputRange: [0, 135],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    });
    const scale = this._scrollY.interpolate({
      inputRange: [0, 70],
      outputRange: [1, 0.5],
      extrapolate: 'clamp'
    });
    const picTranslateY = this._scrollY.interpolate({
      inputRange: [0, 135],
      outputRange: [0, 170],
      extrapolate: 'clamp'
    });
    const translateY = this._scrollY.interpolate({
      inputRange: [0, 70],
      outputRange: [0, 70],
      extrapolate: 'clamp'
    });
    const { public: profile } = this.props.user;
    return (
      <View style={{ flex: 1 }}>
        {this._renderAcitionSheet()}
        <View style={{ flex: 1, backgroundColor: '#fff', width }}>
          {this._renderHeader(profile, translateYName, picTranslateY, opacity)}
          <TouchableOpacity
            style={[
              profileStyle.blueBtn,
              { bottom: 15, width: 55, height: 55 }
            ]}
            onPress={this._openOptions.bind(this)}
          >
            <View>
              <Icon color="#fff" size={40} name="md-add" />
            </View>
          </TouchableOpacity>
          {this._renderModal()}
          <Animated.ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.fakeLoading}
                title={'Your feeds refresh automatically'}
                titleColor={'#fff'}
                tintColor={'#fff'}
                onRefresh={() => {
                  this.setState({ fakeLoading: true }, () => {
                    setTimeout(
                      () => this.setState({ fakeLoading: false }),
                      2000
                    );
                  });
                }}
              />
            }
            onScrollBeginDrag={() => this.setState({ scrolling: true })}
            onScrollEndDrag={() => this.setState({ scrolling: false })}
            style={{
              backgroundColor: Platform.OS === 'ios' ? FitlyBlue : '#fff'
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this._scrollY } } }],
              { useNativeDriver: true }
            )}
            stickyHeaderIndices={[1]}
            scrollEventThrottle={16}
            contentContainerStyle={[
              profileStyle.container,
              { height: this.state.scrollHeight }
            ]}
            ref={'animatedScroll'}
            showsVerticalScrollIndicator={false}
          >
            <View
              style={{
                height: 228.5,
                alignItems: 'center',
                overflow: 'hidden',
                width
              }}
            >
              {
                <Image
                  source={
                    !profile.picture || profile.picture.includes('default-user')
                      ? null
                      : { uri: profile.picture }
                  }
                  style={{
                    position: 'absolute',
                    height: 228.5,
                    width: width
                  }}
                  blurRadius={15}
                />
              }
              <Animated.View
                style={{
                  transform: [{ scale }, { translateY }]
                }}
              >
                <TouchableOpacity onPress={this._renderProfilePicsView}>
                  <Image
                    source={
                      profile.picture
                        ? { uri: profile.picture }
                        : require('../../../img/default-user-image.png')
                    }
                    style={[
                      profileStyle[`${profile.account}Img`],
                      { overlayColor: 'transparent' }
                    ]}
                    defaultSource={require('../../../img/default-user-image.png')}
                  />
                  <TouchableOpacity
                    style={[
                      profileStyle.updateProfileBtn,
                      {
                        backgroundColor: '#fff',
                        borderWidth: 2,
                        borderColor:
                          !profile.picture ||
                          profile.picture.includes('default-user')
                            ? FitlyBlue
                            : '#fff'
                      }
                    ]}
                    onPress={() => this.ActionSheet.show()}
                  >
                    <Icon color={FitlyBlue} size={20} name="md-add" />
                  </TouchableOpacity>
                  {this.state.loading ? (
                    <ActivityIndicator
                      animating={this.state.loading}
                      size="small"
                      style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        marginTop: 20
                      }}
                      color={FitlyBlue}
                    />
                  ) : null}
                </TouchableOpacity>
              </Animated.View>
              <Text
                style={[
                  profileStyle.nameText,
                  {
                    backgroundColor: 'transparent',
                    color: !(
                      !profile.picture ||
                      profile.picture.includes('default-user')
                    )
                      ? '#fff'
                      : '#000'
                  }
                ]}
              >
                {`${profile.first_name} ${profile.last_name}`}
              </Text>
              <View style={profileStyle.locationContainer}>
                <Icon
                  name="ios-pin-outline"
                  size={30}
                  color={
                    !(
                      !profile.picture ||
                      profile.picture.includes('default-user')
                    )
                      ? '#fff'
                      : 'gray'
                  }
                  style={{ backgroundColor: 'transparent' }}
                />
                <Text
                  style={[
                    profileStyle.dashboardText,
                    {
                      marginLeft: 5,
                      zIndex: 1,
                      backgroundColor: 'transparent',
                      color: !(
                        !profile.picture ||
                        profile.picture.includes('default-user')
                      )
                        ? '#fff'
                        : 'gray'
                    }
                  ]}
                >
                  {profile.userCurrentLocation.place}
                </Text>
              </View>
              {this._renderSummary(profile)}
            </View>
            <View
              style={[
                profileStyle.dashboard,
                { height: 70, backgroundColor: '#fff' }
              ]}
            >
              <TouchableOpacity
                style={profileStyle.dashboardItem}
                onPress={() => {
                  this.props.navigation.navigate('SessionListView', {
                    uID: this.props.uID
                  });
                }}
              >
                <View>
                  {this._renderCenteredText(
                    profile.sessionCount,
                    profileStyle.dashboardTextColor
                  )}
                  {this._renderCenteredText(
                    'SESSIONS',
                    profileStyle.dashboardText
                  )}
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={profileStyle.dashboardItem}
                onPress={() => {
                  this.props.navigation.navigate('UserListView', {
                    userType: 'Followers',
                    uID: this.props.uID,
                    dbRef: 'followers',
                    navigation: this.props.navigation,
                    isHeaderRequired: true
                  });
                }}
              >
                <View>
                  {this._renderCenteredText(
                    profile.followerCount,
                    profileStyle.dashboardTextColor
                  )}
                  {this._renderCenteredText(
                    'FOLLOWERS',
                    profileStyle.dashboardText
                  )}
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={profileStyle.dashboardItem}
                onPress={() =>
                  this.props.navigation.navigate('UserListView', {
                    userType: 'Following',
                    uID: this.props.uID,
                    dbRef: 'followings',
                    navigation: this.props.navigation,
                    isHeaderRequired: true
                  })
                }
              >
                <View>
                  {this._renderCenteredText(
                    profile.followingCount,
                    profileStyle.dashboardTextColor
                  )}
                  {this._renderCenteredText(
                    'FOLLOWING',
                    profileStyle.dashboardText
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View style={profileStyle.container}>
              <FeedTabs
                navigation={this.props.navigation}
                screenProps={this.props.screenProps}
                feeds={this.state.feed}
                profile
                resetScroll={() =>
                  this.refs.animatedScroll._component.scrollTo({
                    x: 0,
                    y: 0,
                    animated: false
                  })
                }
                blockTouchables={this.state.scrolling}
                blockScrollHeight={() => {
                  let contentHeight =
                    Math.ceil(
                      this.state.feed.filter(
                        feed => !!feed.photos.length && feed.isActive
                      ).length / 3
                    ) *
                    (width / 3 - 2);
                  contentHeight = contentHeight + 80 + 229 + 70 + 51;
                  console.log(contentHeight, 'here');
                  this.setState({ scrollHeight: contentHeight });
                }}
                releaseScrollHeight={() => {
                  this.setState({ scrollHeight: null });
                }}
                disableTut2={() => {
                  if (this.state.secondTutorial)
                    this.setState({ thirdTutorial: true });
                  this.setState({ secondTutorial: false });
                }}
              />
            </View>
          </Animated.ScrollView>
        </View>
        {this.state.secondTutorial && (
          <Animated.View
            style={{
              width,
              height: 52,
              borderColor: color,
              borderWidth: 4,
              position: 'absolute',
              top: 378.5
            }}
          />
        )}
        {(this.state.secondTutorial || this.state.thirdTutorial) && (
          <View
            style={{
              height,
              width,
              position: 'absolute',
              top: 0,
              left: 0,
              backgroundColor: 'rgba(0,0,0,0.4)'
            }}
            pointerEvents={'none'}
          >
            <Animated.View
              style={{
                height: 70,
                width: 70,
                zIndex: 9999,
                top: !this.state.thirdTutorial ? 448.5 : 280,
                transform: [{ translateX: this.state.swipeGesture }],
                opacity: this.state.gestureOpacity
              }}
            >
              <MaterialIcon name="touch-app" size={70} color="#fff" />
            </Animated.View>
            <View
              style={{
                top: 65,
                position: 'absolute',
                alignSelf: 'center',
                width: 200,
                left: width / 2 - 100,
                backgroundColor: '#fff',
                borderWidth: 0.5,
                borderColor: '#ccc',
                padding: 16,
                shadowColor: '#000',
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 1.5,
                elevation: 3,
                height: 80,
                justifyContent: 'center'
              }}
            >
              <Text
                style={{
                  textAlign: 'center',
                  backgroundColor: 'transparent',
                  lineHeight: 21
                }}
              >
                {this.state.thirdTutorial
                  ? 'Swiping above subtabs navigates to the next tab.'
                  : 'Swiping below subtabs navigates left or right of the subtab.'}
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    loading: state.app.loading,
    user: state.user.user,
    uID: state.auth.uID,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators(
      {
        storeUserProfile
      },
      dispatch
    ),
    exnavigation: bindActionCreators({ push, pop }, dispatch),
    draftsAction: bindActionCreators({ save, clear }, dispatch)
  };
};

const ConnectedProfile = connect(mapStateToProps, mapDispatchToProps)(Profile);

export default ConnectedProfile;
