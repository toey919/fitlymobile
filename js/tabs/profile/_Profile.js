import React, { Component } from 'react';
import { StatusBar, Platform, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import FAIcons from 'react-native-vector-icons/FontAwesome';
import { StackNavigator } from 'react-navigation';
import ComposePost from '../../common/post/ComposePost.js';
import CreateActivityScene from '../../common/activity/CreateActivityScene.js';
import SessionView from '../../common/workoutSession/SessionView.js';
import UserListView from '../../common/UserListView.js';
import SessionListView from '../../common/workoutSession/SessionListView.js';
import ProfileEntry from '../../common/ProfileEntry';
import PostView from '../../common/post/PostView';
import ImageView from '../../common/ImageView';
import ProfilePicsView from '../../common/ProfilePicsView';
import EventScene from '../../common/activity/EventScene';
import SelectDateScene from '../../common/activity/SelectDateScene';
import SelectLocationScene from '../../common/activity/SelectLocationScene';
import EventOptionsMenu from '../../common/activity/EventOptionsMenu';
import Profile from './Profile';
import { crossFade } from '../../common/crossFadeTransitionConfig.js';
const bar = Platform.OS === 'android' ? StatusBar.currentHeight : 0;
const profileRoute = {
  Profile: { screen: Profile },
  ComposePost: { screen: ComposePost },
  SessionListView: { screen: SessionListView },
  CreateActivityScene: { screen: CreateActivityScene },
  SessionView: { screen: SessionView },
  UserListView: { screen: UserListView },
  ProfileEntry: { screen: ProfileEntry },
  PostView: { screen: PostView },
  ImageView: { screen: ImageView },
  ProfilePicsView: { screen: ProfilePicsView },
  EventScene: { screen: EventScene },
  SelectDateScene: { screen: SelectDateScene },
  SelectLocationScene: { screen: SelectLocationScene },
  EventOptionsMenu: { screen: EventOptionsMenu }
};

const ProfileStackNavigation = StackNavigator(profileRoute, {
  headerMode: 'none',
  initialRouteName: 'Profile',
  transitionConfig: crossFade
});

class ProfileNavigationWrapper extends React.Component {
  newUser = this.props.screenProps.newUser;
  render() {
    const { navigation, screenProps } = this.props;
    const {
      rootNavigation,
      startProfTutAnim,
      currentActiveIndex
    } = screenProps;
    const navigationPropsToPass = {
      tabNavigation: navigation,
      rootNavigation,
      startProfTutAnim,
      currentActiveIndex
    };

    return <ProfileStackNavigation screenProps={navigationPropsToPass} />;
  }
}

export default ProfileNavigationWrapper;
