import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  Animated,
  Dimensions,
  Platform
} from 'react-native';
import { commonStyle } from '../../styles/styles.js';
import { push, pop } from '../../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserSearchResults from './UserSearchResults.js';
import HeaderInView from '../../header/HeaderInView.js';
import UserSearchResultEntry from './UserSearchResultEntry';

import SearchBar from 'react-native-search-box';
import Query from '../../library/Query';
import { saveTags } from '../../library/firebaseHelpers';
import SearchResults from './SearchResults';
import { postCategories, eventCategories } from '../../constants/categories';
import FAIcons from 'react-native-vector-icons/FontAwesome';
// Navigation Screens
import ProfileEntry from '../../common/ProfileEntry';
import PostView from '../../common/post/PostView';
import ImageView from '../../common/ImageView';
import TaggedView from '../../common/TaggedView';
import EventScene from '../../common/activity/EventScene';

const isAndroid = Platform.OS === 'android';

import { StackNavigator, NavigationActions } from 'react-navigation';
import commingSoon from '../../common/commingSoon';
import { ConnectedSearch } from './Search';
import ComposePost from '../../common/post/ComposePost';
import { crossFade } from '../../common/crossFadeTransitionConfig.js';
const screenWidth = Dimensions.get('window').width;

const defaultEventSettings = {
  distance: 30,
  fromTime: null,
  toTime: null,
  category: eventCategories
};

const defaultPostSettings = {
  fromTime: null,
  toTime: null,
  category: postCategories
};

const searchRoutes = {
  Search: { screen: ConnectedSearch },
  PostView: { screen: PostView },
  ComposePost: { screen: ComposePost },
  ProfileEntry: { screen: ProfileEntry },
  ImageView: { screen: ImageView },
  TaggedView: { screen: TaggedView },
  UserSearchResults: { screen: UserSearchResults },
  EventScene: { screen: EventScene }
};

const SearchStackNavigator = StackNavigator(searchRoutes, {
  headerMode: 'none',
  initialRouteName: 'Search',
  transitionConfig: crossFade
});

class SearchNavigationWrapper extends React.Component {
  // static navigationOptions = {
  //   tabBarIcon: () => <FAIcons name="search" size={24} color="white" />
  // };

  render() {
    const { navigation, screenProps } = this.props;
    const { rootNavigation } = screenProps;
    const navigationPropsToPass = {
      tabNavigation: navigation,
      rootNavigation
    };

    return <SearchStackNavigator screenProps={navigationPropsToPass} />;
  }
}

export default SearchNavigationWrapper;
