import React, { Component } from 'react';
import {
  View,
  Text,
  Platform,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ListView,
  Image,
  RefreshControl,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';
import { get, shuffle, isObject } from 'lodash';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ImageCache, CachedImage } from 'react-native-img-cache';
import { getWeekdayMonthDay } from '../../library/convertTime';
import { NavigationActions } from 'react-navigation';
import { FitlyBlue } from '../../styles/styles';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const picSize = screenWidth / 3;
export default class LocalPosts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latestPosts: get(this, 'props.latestPosts', {}),
      content: this.props.content,
      loading: this.props.loading,
      imageLoading: false,
      newUsers: get(this, 'props.newUsers', [])
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      JSON.stringify(nextProps.latestPosts) !==
      JSON.stringify(this.props.latestPosts)
    )
      return true;
    if (nextProps.newUsers.length !== this.props.newUsers.length) return true;
    return false;
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      content: nextProps.content,
      loading: nextProps.loading,
      latestPosts: nextProps.latestPosts || {},
      newUsers: get(nextProps, 'newUsers', [])
    });
  }
  _renderPosts() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'flex-start'
        }}
      >
        {this.props.exploring !== 'Posts'
          ? this.props.content.map((content, i) => this._renderTag(content, i))
          : [
              ...shuffle(Object.keys(this.state.latestPosts)),
              ...shuffle(this.state.newUsers)
            ]
              .reverse()
              .map(post => this._renderPost(post))}
      </View>
    );
  }
  // _renderNewUsers() {
  //   return (
  //     <View
  //       style={{
  //         flex: 1,
  //         flexDirection: 'row',
  //         flexWrap: 'wrap',
  //         justifyContent: 'flex-start'
  //       }}
  //     >
  //       {shuffle(this.state.newUsers) // here
  //         .map(user => this._renderUserNew(user))}
  //     </View>
  //   );
  // }
  _renderTag(content, key) {
    if (!content) return null;
    const contentID = content.contentID;
    const contentType = content.contentType;
    let photo, authorName;
    if (contentType === 'post') {
      photo = Object.keys(get(content, 'photos', {})).length
        ? content.photos[Object.keys(content.photos)[0]].link
        : null;
      authorName = content.authorName;
    } else {
      photo = content.backgroundImage;
    }
    return (
      <View
        style={{
          height: picSize,
          width: picSize,
          marginBottom: 2,
          marginLeft: 1,
          marginRight: 1
        }}
        key={key}
      >
        <TouchableOpacity
          onPress={
            contentType === 'post'
              ? this._goToPost.bind(this, contentID)
              : this._goToEvent.bind(this, content)
          }
        >
          <Image
            source={
              photo
                ? { uri: photo }
                : require('../../../img/default-photo-image.png')
            }
            style={{ height: picSize, width: picSize }}
          />
          <View
            style={{
              position: 'absolute',
              width: picSize,
              bottom: 0,
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: 40,
              flexDirection: 'column',
              paddingLeft: 10,
              paddingBottom: 5,
              alignItems: 'flex-start',
              justifyContent: 'flex-end'
            }}
          >
            <Text style={{ color: '#fff', fontSize: 10 }}>
              {authorName || `Event - ${getWeekdayMonthDay(content.startDate)}`}
            </Text>
            <Text style={{ color: '#fff', fontSize: 10 }}>
              {content.title.length > 18
                ? `${content.title.slice(0, 18)}...`
                : content.title || null}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  _renderPostWithListView = data => this._renderPost(data.contentID);
  _renderPost(contentID) {
    if (isObject(contentID)) {
      return this._renderUserNew(contentID);
    }
    const post = get(this, `props.latestPosts.${contentID}`, {});
    const photo = get(post, `photos.${Object.keys(post.photos)[0]}.link`, null);
    return (
      <View
        style={{
          height: picSize,
          width: picSize
        }}
        key={contentID}
      >
        <TouchableOpacity onPress={this._goToPost.bind(this, contentID)}>
          <CachedImage
            source={photo ? { uri: photo } : null}
            style={{
              height: picSize,
              width: picSize,
              justifyContent: 'center'
            }}
          />
          {(post.category === 'Workout Plan' ||
            post.category === 'Meal Plan') && (
            <View
              style={{
                height: 1.5,
                backgroundColor:
                  post.category === 'Workout Plan' ? '#FFD700' : '#ddd',
                width: picSize / 2,
                left: picSize / 4,
                position: 'absolute',
                bottom: 2
              }}
            />
          )}
        </TouchableOpacity>
      </View>
    );
  }

  _renderUserNew(user) {
    if (user.picture.includes('default')) return undefined;
    return (
      <View
        style={{
          height: picSize,
          width: picSize
        }}
        key={user.uID}
      >
        <TouchableOpacity
          onPress={() => {
            if (this.props.uID === user.uID) {
              this.props.navigation.navigate('Profile');
            }
            this.props.goToProfile(user.uID);
          }}
        >
          <CachedImage
            source={{ uri: user.picture }}
            style={{
              height: picSize,
              width: picSize,
              justifyContent: 'center'
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }
  _goToPost(contentID) {
    this.props.navigation.navigate('PostView', { postID: contentID });
  }

  _goToEvent(content) {
    const isAdmin =
      (content.organizers && !!content.organizers[this.props.uID]) || false;
    this.props.navigation.navigate('EventScene', {
      eventID: content.contentID,
      isAdmin
    });
  }
  count = 0;
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this.props.getPosts}
            />
          }
          overScrollMode={'always'}
          bouncesZoom
          onScroll={async ({ nativeEvent }) => {
            let paddingToBottom = 10;
            paddingToBottom += nativeEvent.layoutMeasurement.height;
            if (
              nativeEvent.contentOffset.y >
                nativeEvent.contentSize.height - paddingToBottom &&
              this.count === 0
            ) {
              this.props.onEndReached();
              this.count = 1;
            }
          }}
          scrollEventThrottle={16}
          onScrollBeginDrag={({ nativeEvent }) => {
            if (
              nativeEvent.contentSize.height >
                Dimensions.get('window').height - 180 &&
              !this.state.showSpinnerAtBottom
            ) {
              this.setState({ showSpinnerAtBottom: true });
            }
            this.count = 0;
          }}
          style={{ backgroundColor: 'white' }}
          showsVerticalScrollIndicator={false}
        >
          {this._renderPosts()}
          {this.state.loading &&
            Platform.OS === 'ios' &&
            this.state.showSpinnerAtBottom && (
              <ActivityIndicator
                size={'large'}
                color={FitlyBlue}
                animating={true}
                style={{ backgroundColor: 'transparent' }}
              />
            )}
          {!this.state.loading &&
            !get(this, 'props.latestPosts', {}) &&
            !get(this, 'props.newUsers', []).length && (
              <Text
                style={{
                  textAlign: 'center',
                  alignSelf: 'center',
                  fontSize: 20,
                  fontWeight: '200',
                  marginTop: Dimensions.get('window').height / 2 - 180,
                  color: '#aaa'
                }}
              >
                Nothing here!{'\n'}Check back later to find interesting stuff to
                explore.
              </Text>
            )}
        </ScrollView>
      </View>
    );
  }
}
