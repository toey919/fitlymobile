import React, { Component } from 'react';
import {
  Text,
  Dimensions,
  View,
  ScrollView,
  Platform,
  Animated
} from 'react-native';
import {
  TabViewAnimated,
  TabBarTop,
  TabViewPagerScroll,
  TabViewPagerAndroid
} from 'react-native-tab-view';
const screenWidth = Dimensions.get('window').width;
import { profileStyle } from '../../styles/styles';

export default class FeedTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: '1', title: 'Posts' },
        { key: '2', title: 'Find A Trainer' }
      ]
    };
  }

  _handleChangeTab = index => {
    this.setState({ index });
  };

  _renderHeader = props => {
    const indicatorWidth = 65;
    const marginleft = screenWidth / 4 - indicatorWidth / 2;
    return (
      <TabBarTop
        {...props}
        style={{ backgroundColor: 'white' }}
        labelStyle={{ fontSize: 14, color: 'grey' }}
        indicatorStyle={{
          backgroundColor: '#326fd1',
          alignSelf: 'center',
          marginLeft: marginleft,
          width: indicatorWidth
        }}
      />
    );
  };

  _renderPager = props => {
    return Platform.OS === 'ios' ? (
      <TabViewPagerScroll {...props} />
    ) : (
      <TabViewPagerAndroid {...props} />
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <TabViewAnimated
          style={{ flex: 1, width: screenWidth }}
          navigationState={this.state}
          renderScene={({ route }) => this.props.renderScene(route)}
          renderHeader={this._renderHeader}
          onRequestChangeTab={this._handleChangeTab}
          renderPager={this._renderPager}
          lazy
        />
      </View>
    );
  }
}
