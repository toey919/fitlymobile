import React, { Component } from 'react';
import {
  View,
  ListView,
  ActivityIndicator,
  Dimensions,
  Text,
  RefreshControl
} from 'react-native';
import ActivityCalendarPage from './ActivityCalendarPage.js';
import shallowCompare from 'react-addons-shallow-compare';
import { connect } from 'react-redux';
import _ from 'lodash';
import { bindActionCreators } from 'redux';
import {
  setCurrentPage,
  setCalendarState
} from '../../actions/eventCalendar.js';
import { FitlyBlue } from '../../styles/styles.js';

class ActivityCalendar extends Component {
  constructor(props) {
    super(props);
    this._handleChangeTab = this._handleChangeTab.bind(this);
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      page: 1,
      loading: false,
      initialLoading: true,
      todayTommText: 'Today',
      todayHeight: undefined,
      tomorrowHeight: undefined,
      dataSource: this.ds.cloneWithRows(_.range(0, 10))
    };
  }

  componentDidMount() {
    this._handleCurrentDayChange();
    setTimeout(() => {
      this.setState({ initialLoading: false });
    }, 2000);
  }

  _handleCurrentDayChange() {
    const calendarState = this.props;
    const timeUntilTmr = new Date().setHours(23, 59, 59, 999) - new Date();
    setTimeout(() => {
      let index =
        calendarState.index > 0 ? calendarState.index - 1 : calendarState.index;
      let routes =
        calendarState.index > 0
          ? calendarState.routes.slice(0, calendarState.routes.length - 1)
          : calendarState.routes;
      this.props.action.setCalendarState({ index, routes });
      this._handleCurrentDayChange();
    }, timeUntilTmr);
  }

  _handleChangeTab = index => {
    this.props.action.setCurrentPage(index);
  };

  _renderScene = ({ route }) => {
    return (
      <ActivityCalendarPage key={Math.random()} index={parseInt(route.key)} />
    );
  };

  _onEndReached = () => {
    if (this.state.loading) return;
    this.setState({
      dataSource: this.ds.cloneWithRows(_.range(0, (this.state.page + 1) * 10)),
      page: this.state.page + 1,
      loading: true
    });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 2000);
  };

  _renderRow = rowData => {
    return (
      <ActivityCalendarPage
        index={rowData}
        setContentHeight={height => {
          if (rowData === 0) this.setState({ todayHeight: height });
          else this.setState({ tomorrowHeight: height });
        }}
        key={rowData}
        screenProps={this.props.screenProps}
      />
    );
  };

  _renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <View
        style={{
          alignSelf: 'center',
          backgroundColor: 'rgba(0,0,0,0.4)',
          paddingVertical: 7,
          paddingHorizontal: 15,
          borderRadius: 10,
          alignItems: 'stretch',
          margin: 10
        }}
      >
        <Text style={{ color: '#fff' }}>Loading Events...</Text>
      </View>
    );
  };

  render() {
    const { page, initialLoading } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <ListView
          dataSource={this.state.dataSource}
          refreshControl={
            <RefreshControl
              colors={['#f00', '#0f0', '#00f']}
              refreshing={this.state.loading}
              onRefresh={() => {
                this.setState({
                  page: 0,
                  loading: true
                });
                setTimeout(() => {
                  this.setState({ loading: false });
                }, 2000);
              }}
            />
          }
          onScroll={({ nativeEvent }) => {
            const todayHeight = this.state.todayHeight
              ? this.state.todayHeight
              : 63;
            const tomorrowHeight = this.state.tomorrowHeight
              ? this.state.tomorrowHeight
              : this.state.todayHeight ? 63 + todayHeight : 126;
            if (
              nativeEvent.contentOffset.y > todayHeight &&
              nativeEvent.contentOffset.y < tomorrowHeight
            )
              this.setState({ todayTommText: 'Tomorrow' });
            else if (nativeEvent.contentOffset.y > tomorrowHeight)
              this.setState({ todayTommText: undefined });
            else this.setState({ todayTommText: 'Today' });
          }}
          scrollEventThrottle={16}
          renderRow={this._renderRow}
          renderFooter={this._renderFooter}
          onEndReached={this._onEndReached}
          onEndReachedThreshold={10}
          showsVerticalScrollIndicator={false}
        />
        {this.state.todayTommText && !initialLoading && !this.state.loading ? (
          <View
            style={{
              width: this.state.todayTommText == 'Today' ? 80 : 100,
              paddingHorizontal: 10,
              height: 30,
              borderRadius: 15,
              borderColor: '#ccc',
              borderWidth: 0.75,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#fff',
              elevation: 3,
              shadowColor: '#000',
              shadowOffset: { width: 2, height: 2 },
              shadowOpacity: 0.1,
              shadowRadius: 1.5,
              left:
                Dimensions.get('window').width / 2 -
                (this.state.todayTommText == 'Today' ? 40 : 50),
              top: 5,
              position: 'absolute'
            }}
          >
            <Text style={{ color: 'red', fontWeight: 'bold' }}>
              {this.state.todayTommText}
            </Text>
          </View>
        ) : (
          false
        )}
      </View>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    calendarState: state.eventCalendar
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    action: bindActionCreators({ setCurrentPage, setCalendarState }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ActivityCalendar);
