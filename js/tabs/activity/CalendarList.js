import React from 'react';
import { ListView, UIManager, findNodeHandle } from 'react-native';
import ActivitySearchResultEntry from './ActivitySearchResultEntry';
export default class CalendarList extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.eventData !== this.props.eventData;
  }
  componentDidMount() {
    setTimeout(() => {
      if (this.refs['eventCalSearchResults'])
        UIManager.measure(
          findNodeHandle(this.refs['eventCalSearchResults']),
          (x, y, w, h, pX, pY) => {
            if (this.props.index === 1 || this.props.index === 0) {
              this.props.setContentHeight(h);
            }
          }
        );
    }, 0);
  }
  render() {
    return (
      <ListView
        onEndReachedThreshold={10}
        ref="eventCalSearchResults"
        dataSource={this.props.eventData}
        renderRow={rowData => (
          <ActivitySearchResultEntry
            key={rowData}
            data={rowData}
            screenProps={this.props.screenProps}
          />
        )}
        onEndReached={this.props.onEndReached}
      />
    );
  }
}
