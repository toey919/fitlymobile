import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { activityTabStyle, FitlyBlue } from '../../styles/styles.js';
import { push } from '../../actions/navigation.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/Ionicons';
import RenderUserBadges from '../../common/RenderUserBadges.js';
import { getDateStringByFormat } from '../../library/convertTime.js';

class ActivitySearchResultEntry extends Component {
  constructor(props) {
    super(props);
    this.event = this.props.data._source;
    this.eventID = this.props.data._id;
    this._pushToEvent = this._pushToEvent.bind(this);
  }

  _pushToEvent() {
    const isAdmin =
      (this.event.organizers && !!this.event.organizers[this.props.uID]) ||
      false;
    this.props.screenProps.rootNavigation.navigate('EventScene', {
      eventID: this.eventID,
      isAdmin
    });
  }

  render() {
    const event = this.event;
    return (
      <TouchableOpacity
        style={[activityTabStyle.eventEntry, { minHeight: 90 }]}
        onPress={this._pushToEvent}
      >
        <View
          style={{
            width: 100,
            marginVertical: 10,
            marginRight: 20,
            borderRightWidth: 2,
            borderRightColor: FitlyBlue,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text
            style={[
              activityTabStyle.eventEntryText,
              { textAlign: 'center', fontWeight: '300' }
            ]}
          >
            <Text
              style={[
                activityTabStyle.eventEntryText,
                {
                  color: '#000',
                  fontWeight: '300',
                  letterSpacing: -1,
                  textAlign: 'center'
                }
              ]}
            >
              STARTS{'\n'}
            </Text>
            {getDateStringByFormat(
              new Date(event.startDate),
              this.props.searchMode ? 'MMM Do h:mm a' : 'h:mm a'
            )}
          </Text>
          <Text style={[activityTabStyle.eventEntryText, {}]}>
            {event.memberCount + ' going'}
          </Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text
            style={[
              activityTabStyle.eventEntryText,
              { color: 'black', fontWeight: 'bold', letterSpacing: -1 }
            ]}
          >
            {event.title.toUpperCase()}
          </Text>
          <Text
            style={[activityTabStyle.eventEntryText, { fontWeight: 'bold' }]}
          >
            {event.location.address
              .split(',')
              .slice(0, 2)
              .join(',')
              .trim()}
          </Text>
        </View>
        {(!event.isActive || event.startDate < new Date()) && (
          <View
            style={{
              borderTopColor: event.startDate < new Date() ? 'green' : 'red',
              borderBottomColor: event.startDate < new Date() ? 'green' : 'red',
              backgroundColor: 'transparent',
              borderBottomWidth: 1,
              borderTopWidth: 1,
              position: 'absolute',
              top: 20,
              right: -60,
              width: 200,
              alignItems: 'center',
              transform: [{ rotate: '30deg' }]
            }}
          >
            <Text
              style={{
                backgroundColor: 'transparent',
                color: event.startDate < new Date() ? 'green' : 'red'
              }}
            >
              {event.startDate < new Date() ? 'ONGOING' : `CANCELLED`}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    uID: state.auth.uID,
    loading: state.app.loading,
    FitlyFirebase: state.app.FitlyFirebase
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    exnavigation: bindActionCreators({ push }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  ActivitySearchResultEntry
);
