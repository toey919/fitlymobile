import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { FitlyBlue, loginStyles } from '../styles/styles';
const { width, height } = Dimensions.get('window');
export default class Welcome extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'space-between',
          paddingVertical: 150,
          paddingTop: 130,
          width: 276
        }}
      >
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
            textAlign: 'center'
          }}
        >
          Welcome{'\n'}to
        </Text>
        <Text
          style={{
            color: FitlyBlue,
            fontFamily: 'HelveticaNeue',
            fontWeight: '700',
            fontSize: 50,
            textAlign: 'center',
            letterSpacing: -2
          }}
        >
          Fitly
        </Text>
        <Text
          style={{
            textAlign: 'center'
          }}
        >
          The best way to meet new people!
        </Text>
      </View>
    );
  }
}
