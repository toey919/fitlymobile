import React from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity } from 'react-native';
import { FitlyBlue, loginStyles } from '../styles/styles';
const { width, height } = Dimensions.get('window');
export default class Events extends React.Component {
  render() {
    return (
      <View
        style={{
          height: 476.5,
          width: 276,
          borderRadius: 10,
          backgroundColor: '#eee',
          alignItems: 'center',
          padding: 16,
          paddingVertical: 100,
          justifyContent: 'space-between'
        }}
      >
        <Text
          style={{
            color: '#000',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 20
          }}
        >
          Events
        </Text>
        <Text
          style={{
            color: '#000',
            textAlign: 'center'
          }}
        >
          Here you can Find or Host events.
        </Text>
        <TouchableOpacity
          onPress={this.props.onDonePress}
          style={{
            backgroundColor: '#fff',
            width: 80,
            height: 40,
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'flex-end',
            borderColor: FitlyBlue
          }}
        >
          <Text
            style={{
              fontWeight: 'bold',
              color: FitlyBlue,
              backgroundColor: 'transparent'
            }}
          >
            Done
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
