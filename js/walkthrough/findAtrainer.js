import React from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity } from 'react-native';
import { FitlyBlue, loginStyles } from '../styles/styles';
const { width, height } = Dimensions.get('window');
export default class FindATrainer extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: 276,
          alignItems: 'center',
          padding: 16,
          justifyContent: 'center'
        }}
      >
        <Text
          style={{
            color: '#000',
            textAlign: 'center'
          }}
        >
          Find top trainers in your area who can help you meet your fitness
          goals!
        </Text>
      </View>
    );
  }
}
