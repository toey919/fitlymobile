import React from 'react';
import { View, Text, Dimensions, Image } from 'react-native';
import { FitlyBlue, loginStyles } from '../styles/styles';
const { width, height } = Dimensions.get('window');
export default class Messages extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: 276,
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingVertical: 100,
          padding: 16
        }}
      >
        <Text
          style={{
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: width * 0.06
          }}
        >
          Congratulations!
        </Text>
        <Text
          style={{
            textAlign: 'center',
            marginTop: 20,
            lineHeight: 30,
            fontSize: width * 0.04
          }}
        >
          You are a part of the Bay Area launch! As the app grows you'll notice
          more features become available. Check back regularly and provide
          feedback during the Beta to help the team make your experience even
          better!
        </Text>
      </View>
    );
  }
}
