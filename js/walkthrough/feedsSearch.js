import React from 'react';
import { View, Text, Dimensions, Image } from 'react-native';
import { FitlyBlue, loginStyles } from '../styles/styles';
const { width, height } = Dimensions.get('window');
export default class Welcome extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: 276,
          alignItems: 'center',
          padding: 16
        }}
      >
        <Text
          style={{
            color: '#000',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 22
          }}
        >
          Discover
        </Text>
        <Text
          style={{
            color: '#000',
            textAlign: 'center',
            marginTop: 150
          }}
        >
          On this tab you can find users photos, workout plans, and meal
          submissions.
        </Text>
      </View>
    );
  }
}
