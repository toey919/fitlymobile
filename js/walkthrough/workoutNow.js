import React from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity } from 'react-native';
import { FitlyBlue, loginStyles } from '../styles/styles';
const { width, height } = Dimensions.get('window');
export default class Welcome extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: 276,
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: 16,
          paddingVertical: 100
        }}
      >
        <Text
          style={{
            color: '#000',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 20
          }}
        >
          Workout Partners
        </Text>
        <Text
          style={{
            color: '#000',
            textAlign: 'center'
          }}
        >
          This tab allows users to find training partners. We ask that you enter
          the queue 30 minutes before your workout to allow our team time to
          find you the best match!
        </Text>
        <TouchableOpacity
          onPress={this.props.onDonePress}
          style={{
            backgroundColor: '#fff',
            width: 80,
            height: 40,
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'flex-end',
            borderColor: FitlyBlue
          }}
        >
          <Text
            style={{
              fontWeight: 'bold',
              color: FitlyBlue,
              backgroundColor: 'transparent'
            }}
          >
            Done
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
