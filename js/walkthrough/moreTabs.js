import React from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity } from 'react-native';
import { FitlyBlue, loginStyles } from '../styles/styles';
const { width, height } = Dimensions.get('window');
export default class Welcome extends React.Component {
  render() {
    return (
      <View
        style={{
          height: 476.5,
          width: 276,
          alignItems: 'center',
          padding: 16
        }}
      >
        <Text
          style={{
            color: '#000',
            backgroundColor: 'transparent',
            textAlign: 'center',
            marginTop: 200,
            lineHeight: 24,
            fontSize: width * 0.04
          }}
        >
          Explore all 5 tabs to learn how to navigate the app. Swipe left or
          right to move between the tabs.
        </Text>
      </View>
    );
  }
}
